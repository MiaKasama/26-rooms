﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SimpleCollectibleScript : MonoBehaviour {

	public enum CollectibleTypes {NoType, Type1, Type2, Type3, Type4, Type5}; // you can replace this with your own labels for the types of collectibles in your game!

	public CollectibleTypes CollectibleType; // this gameObject's type

	public bool rotate; // do you want it to rotate?

	public float rotationSpeed;

	public AudioClip collectSound;

	public GameObject collectEffect;

	// 追記部分 コインを入手したかどうか
	//public static bool AcquireCoin = false;
	public static bool AcquireCoinA = false;
	public static bool AcquireCoinB = false;
	public static bool AcquireCoinC = false;
	public static bool AcquireCoinD = false;
	public static bool AcquireCoinE = false;
	public static bool AcquireCoinF = false;
	public static bool AcquireCoinG = false;
	public static bool AcquireCoinH = false;
	public static bool AcquireCoinI = false;
	public static bool AcquireCoinJ = false;
	public static bool AcquireCoinK = false;
	public static bool AcquireCoinL = false;
	public static bool AcquireCoinM = false;
	public static bool AcquireCoinN = false;
	public static bool AcquireCoinO = false;
	public static bool AcquireCoinP = false;
	public static bool AcquireCoinQ = false;
	public static bool AcquireCoinR = false;
	public static bool AcquireCoinS = false;
	public static bool AcquireCoinT = false;
	public static bool AcquireCoinU = false;
	public static bool AcquireCoinV = false;
	public static bool AcquireCoinW = false;
	public static bool AcquireCoinX = false;
	public static bool AcquireCoinY = false;
	public static bool AcquireCoinZ = false;

	// Use this for initialization
	void Start () {

		// 追記部分 AcquireCoinの初期化
		if (RetryGame.getInitializeStartTime() == true)
        {
			AcquireCoinA = false;
			AcquireCoinB = false;
			AcquireCoinC = false;
			AcquireCoinD = false;
			AcquireCoinE = false;
			AcquireCoinF = false;
			AcquireCoinG = false;
			AcquireCoinH = false;
			AcquireCoinI = false;
			AcquireCoinJ = false;
			AcquireCoinK = false;
			AcquireCoinL = false;
			AcquireCoinM = false;
			AcquireCoinN = false;
			AcquireCoinO = false;
			AcquireCoinP = false;
			AcquireCoinQ = false;
			AcquireCoinR = false;
			AcquireCoinS = false;
			AcquireCoinT = false;
			AcquireCoinU = false;
			AcquireCoinV = false;
			AcquireCoinW = false;
			AcquireCoinX = false;
			AcquireCoinY = false;
			AcquireCoinZ = false;
		}
		//AcquireCoin = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (rotate)
			transform.Rotate (Vector3.up * rotationSpeed * Time.deltaTime, Space.World);

		// 追記部分
		//AcquireCoin = false;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") {
			Collect ();
		}
	}

	public void Collect()
	{
		if(collectSound)
			AudioSource.PlayClipAtPoint(collectSound, transform.position);
		if(collectEffect)
			Instantiate(collectEffect, transform.position, Quaternion.identity);

		//Below is space to add in your code for what happens based on the collectible type

		if (CollectibleType == CollectibleTypes.NoType) {

			//Add in code here;

			Debug.Log ("Do NoType Command");
		}
		if (CollectibleType == CollectibleTypes.Type1) {

			//Add in code here;

			Debug.Log ("Do NoType Command");
		}
		if (CollectibleType == CollectibleTypes.Type2) {

			//Add in code here;

			Debug.Log ("Do NoType Command");
		}
		if (CollectibleType == CollectibleTypes.Type3) {

			//Add in code here;

			Debug.Log ("Do NoType Command");
		}
		if (CollectibleType == CollectibleTypes.Type4) {

			//Add in code here;

			Debug.Log ("Do NoType Command");
		}
		if (CollectibleType == CollectibleTypes.Type5) {

			//Add in code here;

			Debug.Log ("Do NoType Command");
		}

		Destroy (gameObject);

		// 追記部分 コイン入手
		//AcquireCoin = true;

		if (NowRoom.getinRoomA() == true)
			AcquireCoinA = true;

		if (NowRoom.getinRoomB() == true)
			AcquireCoinB = true;

		if (NowRoom.getinRoomC() == true)
			AcquireCoinC = true;

		if (NowRoom.getinRoomD() == true)
			AcquireCoinD = true;

		if (NowRoom.getinRoomE() == true)
			AcquireCoinE = true;

		if (NowRoom.getinRoomF() == true)
			AcquireCoinF = true;

		if (NowRoom.getinRoomG() == true)
			AcquireCoinG = true;

		if (NowRoom.getinRoomH() == true)
			AcquireCoinH = true;

		if (NowRoom.getinRoomI() == true)
			AcquireCoinI = true;

		if (NowRoom.getinRoomJ() == true)
			AcquireCoinJ = true;

		if (NowRoom.getinRoomK() == true)
			AcquireCoinK = true;

		if (NowRoom.getinRoomL() == true)
			AcquireCoinL = true;

		if (NowRoom.getinRoomM() == true)
			AcquireCoinM = true;

		if (NowRoom.getinRoomN() == true)
			AcquireCoinN = true;

		if (NowRoom.getinRoomO() == true)
			AcquireCoinO = true;

		if (NowRoom.getinRoomP() == true)
			AcquireCoinP = true;

		if (NowRoom.getinRoomQ() == true)
			AcquireCoinQ = true;

		if (NowRoom.getinRoomR() == true)
			AcquireCoinR = true;

		if (NowRoom.getinRoomS() == true)
			AcquireCoinS = true;

		if (NowRoom.getinRoomT() == true)
			AcquireCoinT = true;

		if (NowRoom.getinRoomU() == true)
			AcquireCoinU = true;

		if (NowRoom.getinRoomV() == true)
			AcquireCoinV = true;

		if (NowRoom.getinRoomW() == true)
			AcquireCoinW = true;

		if (NowRoom.getinRoomX() == true)
			AcquireCoinX = true;

		if (NowRoom.getinRoomY() == true)
			AcquireCoinY = true;

		if (NowRoom.getinRoomZ() == true)
			AcquireCoinZ = true;
	}

	// 追記部分 コイン入手の状態を返す
	//public static bool getAquireCoin()
	//{
		//return AcquireCoin;
	//}

	public static bool getAquireCoinA()
	{
		return AcquireCoinA;
	}

	public static bool getAquireCoinB()
	{
		return AcquireCoinB;
	}

	public static bool getAquireCoinC()
	{
		return AcquireCoinC;
	}

	public static bool getAquireCoinD()
	{
		return AcquireCoinD;
	}

	public static bool getAquireCoinE()
	{
		return AcquireCoinE;
	}

	public static bool getAquireCoinF()
	{
		return AcquireCoinF;
	}

	public static bool getAquireCoinG()
	{
		return AcquireCoinG;
	}

	public static bool getAquireCoinH()
	{
		return AcquireCoinH;
	}

	public static bool getAquireCoinI()
	{
		return AcquireCoinI;
	}

	public static bool getAquireCoinJ()
	{
		return AcquireCoinJ;
	}

	public static bool getAquireCoinK()
	{
		return AcquireCoinK;
	}

	public static bool getAquireCoinL()
	{
		return AcquireCoinL;
	}

	public static bool getAquireCoinM()
	{
		return AcquireCoinM;
	}

	public static bool getAquireCoinN()
	{
		return AcquireCoinN;
	}

	public static bool getAquireCoinO()
	{
		return AcquireCoinO;
	}

	public static bool getAquireCoinP()
	{
		return AcquireCoinP;
	}

	public static bool getAquireCoinQ()
	{
		return AcquireCoinQ;
	}

	public static bool getAquireCoinR()
	{
		return AcquireCoinR;
	}

	public static bool getAquireCoinS()
	{
		return AcquireCoinS;
	}

	public static bool getAquireCoinT()
	{
		return AcquireCoinT;
	}

	public static bool getAquireCoinU()
	{
		return AcquireCoinU;
	}

	public static bool getAquireCoinV()
	{
		return AcquireCoinV;
	}

	public static bool getAquireCoinW()
	{
		return AcquireCoinW;
	}

	public static bool getAquireCoinX()
	{
		return AcquireCoinX;
	}

	public static bool getAquireCoinY()
	{
		return AcquireCoinY;
	}

	public static bool getAquireCoinZ()
	{
		return AcquireCoinZ;
	}
}
