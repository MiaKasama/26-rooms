using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChangeCount : MonoBehaviour
{
    public static int SceneChanger = -1;

    // Start is called before the first frame update
    void Start()
    {
        //何度MainStageに戻ってきたかカウントする
        SceneChanger++;
    }

    // Update is called once per frame
    void Update()
    {
        //ゲームオーバーかクリアでリセット
        if (GameOverInformation.getGameOverSet() == true || NowRoom.getClearCheck() == true)
            SceneChanger = -1;
    }

    public static int getSceneChanger()
    {
        return SceneChanger;
    }
}
