using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MiniToMain : MonoBehaviour
{
    //このシーンのインデックス
    int CurrentMiniGame;

    //ミニゲームからメインに帰ってきた時に、鍵番号が変わらないように
    public static bool KeepKNA = false;

    //ステージクリア時の処理
    public static int[] StageClearArray = new int[26];
    bool SuccessCoroutine = true;

    public static bool SuccessTextBool = false;

    //クリア失敗時の処理
    public static bool StageFailure = false;
    bool FailureCoroutine = true;
    bool TryOrGive = false;

    public static bool FailureTextBool = false;

    //一枚もコインを手に入れていない時専用の処理
    public static bool NoCoinClear = false;

    [SerializeField]
    [Tooltip("仮の処理をしますか")]
    private bool Tentative = true;

    //ゲーム開始後いつでもメインステージに戻れるミニゲーム用
    bool SecondWaiter = false;

    //仮のクリア処理を行う用
    int TentativeClear;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        CurrentMiniGame = SceneManager.GetActiveScene().buildIndex - 2;

        KeepKNA = false;

        SuccessTextBool = false;
        FailureTextBool = false;

        //リスタート後に初めてミニゲームをやる時にクリア情報をリセットする
        //一枚もコインを入手していない時に別のミニゲームを開始した時には処理をスキップ
        if (SceneChangeCount.getSceneChanger() == 0)
        {
            for (i = 0; i < StageClearArray.Length; i++)
                StageClearArray[i] = 0;
        }
        
        StageFailure = false;

        StartCoroutine(Start_frames());

        //仮処理で以下のコルーチンの実行
        if (Tentative == true)
            StartCoroutine(Start_Tentative());
    }

    // Update is called once per frame
    void Update()
    {
        //ゲームオーバー及びクリア時は鍵番号を変え、ミニゲームのクリア情報を消す
        if (GameOverInformation.getGameOverSet() == true || NowRoom.getClearCheck() == true)
        {
            KeepKNA = false;
            StageClearArray[CurrentMiniGame] = 0;
            Debug.Log("Game Set");
        }

        //ミニゲームA
        if(CurrentMiniGame == 0)
        {
            if (AppleInputFieldManager.getSuccessApple() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (AppleInputFieldManager.getFailureApple() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームB
        if (CurrentMiniGame == 1)
        {
            if (BlockCheck.getSuccessBlock() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }
            else if (SecondWaiter == true)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    StageClearArray[CurrentMiniGame] = 0;

                    KeepKNA = true;
                    SceneManager.LoadScene(1);
                }
            }
        }

        //ミニゲームC
        if (CurrentMiniGame == 2)
        {
            if (CameraTaking.getSuccessCamera() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (CameraTaking.getFailureCamera() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームD
        if (CurrentMiniGame == 3)
        {
            if (DefferenceClicked.getSuccessDefference() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (DefferenceClicked.getFailureDefference() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームE
        if (CurrentMiniGame == 4)
        {
            if (ExplosionAnim.getSuccessExplosion() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (ExplosionAnim.getFailureExplosion() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームF
        if (CurrentMiniGame == 5)
        {
            if (FlagController.getSuccessFlag() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (FlagController.getFailureFlag() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームG
        if (CurrentMiniGame == 6)
        {
            if (GoldDestroy.getSuccessGold() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }
            else if (SecondWaiter == true)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    StageClearArray[CurrentMiniGame] = 0;

                    KeepKNA = true;
                    SceneManager.LoadScene(1);
                }
            }
        }

        //ミニゲームH
        if (CurrentMiniGame == 7)
        {
            if (HalfAnswerOpen.getSuccessHalf() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (HalfAnswerOpen.getFailureHalf() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームI
        if (CurrentMiniGame == 8)
        {
            if (IceCharacterMove.getSuccessIce() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }
            else if (SecondWaiter == true)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    StageClearArray[CurrentMiniGame] = 0;

                    KeepKNA = true;
                    SceneManager.LoadScene(1);
                }
            }
        }

        //ミニゲームJ
        if (CurrentMiniGame == 9)
        {
            if (JokerClicked.getSuccessJoker() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (JokerClicked.getFailureJoker() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームK
        if (CurrentMiniGame == 10)
        {
            if (KnifeSuccess.getSuccessKnife() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (KnifeFailure.getFailureKnife() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームL
        if (CurrentMiniGame == 11)
        {
            if (LengthSlasherMove.getSuccessLength() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (LengthSlasherMove.getFailureLength() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームM
        if (CurrentMiniGame == 12)
        {
            if (MathInputField.getSuccessMath() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (MathInputField.getFailureMath() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームN
        if (CurrentMiniGame == 13)
        {
            if (NothingCameraMove.getSuccessNothing() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }
            else if (SecondWaiter == true)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    StageClearArray[CurrentMiniGame] = 0;

                    KeepKNA = true;
                    SceneManager.LoadScene(1);
                }
            }
        }

        //ミニゲームO
        if (CurrentMiniGame == 14)
        {
            if (OnlyClicked.getSuccessOnly() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (OnlyClicked.getFailureOnly() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームP
        if (CurrentMiniGame == 15)
        {
            if (PianoClicked.getSuccessPiano() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (PianoClicked.getFailurePiano() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームQ
        if (CurrentMiniGame == 16)
        {
            if (QuizButtonPush.getSuccessQuiz() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (QuizButtonPush.getFailureQuiz() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームR
        if (CurrentMiniGame == 17)
        {
            if (RPSTheirHand.getSuccessRPS() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (RPSTheirHand.getFailureRPS() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームS
        if (CurrentMiniGame == 18)
        {
            if (ShieldMove.getSuccessShield() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (ShieldFailureZone.getFailureShield() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームT
        if (CurrentMiniGame == 19)
        {
            if (TypingInput.getSuccessTyping() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (TypingInput.getFailureTyping() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームU
        if (CurrentMiniGame == 20)
        {
            if (UmbrellaJudgement.getSuccessUmbrella() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (UmbrellaJudgement.getFailureUmbrella() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームV
        if (CurrentMiniGame == 21)
        {
            if (VegetableBasketMove.getSuccessVegetable() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (VegetableBasketMove.getFailureVegetable() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームW
        if (CurrentMiniGame == 22)
        {
            if (WatchNow.getSuccessWatch() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (WatchNow.getFailureWatch() == true && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームX
        if (CurrentMiniGame == 23)
        {
            if (XWrongClick.getSuccessXXX() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }
            else if (SecondWaiter == true)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                    SceneManager.LoadScene(1);
            }

            if (XClickOver.getFailureXXX() && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームY
        if (CurrentMiniGame == 24)
        {
            if (YieldProgram.getSuccessYield() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }

            if (YieldProgram.getFailureYield() && FailureCoroutine == true)
            {
                FailureCoroutine = false;

                FailureTextBool = true;
                StartCoroutine(FailureMove());
            }
        }

        //ミニゲームZ
        if (CurrentMiniGame == 25)
        {
            if (ZigZagMove.getSuccessZigZag() == true && SuccessCoroutine == true)
            {
                SuccessCoroutine = false;

                SuccessTextBool = true;
                StartCoroutine(SuccessMove());
            }
            else if (SecondWaiter == true)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    StageClearArray[CurrentMiniGame] = 0;

                    KeepKNA = true;
                    SceneManager.LoadScene(1);
                }
            }
        }

        if (TryOrGive == true)
        {
            // もし入力したキーがEnterキーならば、MainStageに切り替える
            if (Input.GetKeyDown(KeyCode.Return))
            {
                KeepKNA = true;
                SceneManager.LoadScene(1);
            }

            // もし入力したキーがSpaceキーならば、ミニゲームを繰り返す
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Reload");
                SceneManager.LoadScene(CurrentMiniGame + 2);
            }
        }
    }

    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        SecondWaiter = true;
    }

    //クリア後3秒後にMainStageに戻る
    private IEnumerator SuccessMove()
    {
        yield return new WaitForSeconds(3.0f);

        StageClearArray[CurrentMiniGame] = CurrentMiniGame + 1;

        KeepKNA = true;
        SceneManager.LoadScene(1);
    }

    //クリア失敗時にキー入力可能にする
    private IEnumerator FailureMove()
    {
        yield return new WaitForSeconds(3.0f);

        StageClearArray[CurrentMiniGame] = 0;

        //ミニゲームXのみ特殊な処理を行う
        if (CurrentMiniGame != 23)
            TryOrGive = true;
        else
        {
            yield return new WaitForSeconds(3.0f);

            KeepKNA = true;
            SceneManager.LoadScene(1);
        }
    }

    //仮の処理として、3秒後に50%の確率でクリアしてMainStageに戻す
    private IEnumerator Start_Tentative()
    {
        TentativeClear = Random.Range(0, 2);

        yield return new WaitForSeconds(3.0f);

        KeepKNA = true;

        if (TentativeClear == 0)
            StageClearArray[CurrentMiniGame] = CurrentMiniGame + 1;
        else
            StageClearArray[CurrentMiniGame] = 0;

        SceneManager.LoadScene(1);
    }

    public static bool getKeepKNA()
    {
        return KeepKNA;
    }

    public static int[] getStageClearArray()
    {
        return StageClearArray;
    }

    public static bool getSuccessTextBool()
    {
        return SuccessTextBool;
    }

    public static bool getFailureTextBool()
    {
        return FailureTextBool;
    }
}
