using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearDestroy : MonoBehaviour
{
    [SerializeField]
    [Tooltip("部屋のコードを入力してください")]
    private int RoomCode;

    //部屋のコードが1〜26の時だけtrueにする
    bool RoomAlpha;

    //一度破壊したら、リスタートまで復活しないようにする
    public static int[] KeepDestroyArray = new int[27];

    public static bool FirstDestroyArray = true;

    //FirstDestroyArrayをfalseにする為の処理用変数
    public static int FirstDestroyCount = 0;

    public static bool RetryKeepArray = false;

    //その部屋のステージをクリアしたか
    int[] StageClearArray = new int[26];

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //空のオブジェクトに入れた27番目の部屋だけ処理から外す
        if (RoomCode >= 1 && RoomCode <= 26)
            RoomAlpha = true;
        else
            RoomAlpha = false;

        //ゲーム開始時に配列に値を入れる
        //RoomCodeのデフォルト値と異なる値を入れておく
        if(FirstDestroyArray == true)
        {
            FirstDestroyCount++;

            if(FirstDestroyCount == KeepDestroyArray.Length)
                FirstDestroyArray = false;

            KeepDestroyArray[RoomCode - 1] = -1;
        }

        if(RoomAlpha == true)
            StageClearArray[RoomCode - 1] = MiniToMain.getStageClearArray()[RoomCode - 1];

        //ミニゲームクリアでオブジェクトを破壊する
        if (RoomAlpha == true)
        {
            if (StageClearArray[RoomCode - 1] == RoomCode && (RoomCode == OnTriggerMiniGame.getNowRoomCode() || KeepDestroyArray[RoomCode - 1] == RoomCode) && RetryKeepArray == false)
            {
                //Debug.Log("Destroy");
                Destroy(gameObject);
                KeepDestroyArray[RoomCode - 1] = RoomCode;
            }
        }

        //リトライ時にオブジェクトを復活（破壊しない）
        if(RetryKeepArray == true)
        {
            FirstDestroyCount++;

            if (FirstDestroyCount == KeepDestroyArray.Length)
                RetryKeepArray = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(GameOverInformation.getGameOverSet() == true || NowRoom.getClearCheck() == true)
        {
            if(RoomCode == 27)
            {
                for(i = 0; i < KeepDestroyArray.Length; i++)
                    KeepDestroyArray[i] = -1;

                RetryKeepArray = true;

                FirstDestroyCount = 0;
            }
        } 
    }

    public static int[] getKeepDestroyArray()
    {
        return KeepDestroyArray;
    }
}
