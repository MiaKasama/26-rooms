using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepLocation : MonoBehaviour
{
    [SerializeField]
    [Tooltip("初期のx座標を入力してください")]
    private float x0 = 0;

    [SerializeField]
    [Tooltip("初期のy座標を入力してください")]
    private float y0 = 0;

    [SerializeField]
    [Tooltip("初期のz座標を入力してください")]
    private float z0 = -5;

    //キャラクターのtransform
    Transform myTransform;

    //保存する座標
    public static Vector3 KeepPosition;
    public static Quaternion KeepRotation;

    //初回用
    public static bool FirstPositionBool = true;

    // Start is called before the first frame update
    void Start()
    {
        //初回はスタート地点固定
        if (FirstPositionBool == true)
        {
            FirstPositionBool = false;
            KeepPosition = new Vector3(x0, y0, z0);
            KeepRotation = Quaternion.Euler(0, 0, 0);
        }

        myTransform = GameObject.Find("PlayerArmature").transform;
        myTransform.position = KeepPosition;
        myTransform.rotation = KeepRotation;
    }

    // Update is called once per frame
    void Update()
    {
        myTransform = GameObject.Find("PlayerArmature").transform;

        //ミニゲームスタート時のプレイヤーの座標を保存
        if (OnTriggerMiniGame.getMiniGameStartingBool() == true)
        {
            KeepPosition = myTransform.position;
            KeepRotation = myTransform.rotation;
        }

        //ゲームオーバー及びクリアの時はスタートに戻れるよう処理
        if (GameOverInformation.getGameOverSet() == true || NowRoom.getClearCheck() == true)
        {
            KeepPosition = new Vector3(x0, y0, z0);
            KeepRotation = Quaternion.Euler(0, 0, 0);
        }
    }
}
