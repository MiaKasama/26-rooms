using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnTriggerMiniGame : MonoBehaviour
{
    //部屋によってどのミニゲームが開始するかが変わる
    public static int NowRoomCode = 0;

    [SerializeField]
    [Tooltip("部屋のコードを入力してください")]
    private int RoomCode;

    public static bool EnterTextBool = false;

    //ミニゲームスタートした瞬間にtrueとなるboolean
    public static bool MiniGameStartingBool = false;

    // Start is called before the first frame update
    void Start()
    {
        MiniGameStartingBool = false;

        //コルーチンの実行
        //StartCoroutine(Start_frames());

        EnterTextBool = false;
    }

    // Update is called once per frame
    void Update()
    {
        NowRoomCode = 0;

        if (NowRoom.getinRoomA() == true)
            NowRoomCode = 1;

        if (NowRoom.getinRoomB() == true)
            NowRoomCode = 2;

        if (NowRoom.getinRoomC() == true)
            NowRoomCode = 3;

        if (NowRoom.getinRoomD() == true)
            NowRoomCode = 4;

        if (NowRoom.getinRoomE() == true)
            NowRoomCode = 5;

        if (NowRoom.getinRoomF() == true)
            NowRoomCode = 6;

        if (NowRoom.getinRoomG() == true)
            NowRoomCode = 7;

        if (NowRoom.getinRoomH() == true)
            NowRoomCode = 8;

        if (NowRoom.getinRoomI() == true)
            NowRoomCode = 9;

        if (NowRoom.getinRoomJ() == true)
            NowRoomCode = 10;

        if (NowRoom.getinRoomK() == true)
            NowRoomCode = 11;

        if (NowRoom.getinRoomL() == true)
            NowRoomCode = 12;

        if (NowRoom.getinRoomM() == true)
            NowRoomCode = 13;

        if (NowRoom.getinRoomN() == true)
            NowRoomCode = 14;

        if (NowRoom.getinRoomO() == true)
            NowRoomCode = 15;

        if (NowRoom.getinRoomP() == true)
            NowRoomCode = 16;

        if (NowRoom.getinRoomQ() == true)
            NowRoomCode = 17;

        if (NowRoom.getinRoomR() == true)
            NowRoomCode = 18;

        if (NowRoom.getinRoomS() == true)
            NowRoomCode = 19;

        if (NowRoom.getinRoomT() == true)
            NowRoomCode = 20;

        if (NowRoom.getinRoomU() == true)
            NowRoomCode = 21;

        if (NowRoom.getinRoomV() == true)
            NowRoomCode = 22;

        if (NowRoom.getinRoomW() == true)
            NowRoomCode = 23;

        if (NowRoom.getinRoomX() == true)
            NowRoomCode = 24;

        if (NowRoom.getinRoomY() == true)
            NowRoomCode = 25;

        if (NowRoom.getinRoomZ() == true)
            NowRoomCode = 26;

        //範囲内に入っていたら、部屋に合わせたミニゲームを開始
        if (EnterTextBool == true && NowRoomCode == RoomCode)
        {
            // Enterキーでミニゲーム開始
            if (Input.GetKeyDown(KeyCode.Return))
            {
                MiniGameStartingBool = true;

                //コルーチンの実行
                StartCoroutine(Start_Mini());
            }
        }
    }

    //範囲内に入ったらテキストを表示する。
    void OnTriggerEnter(Collider other)
    {
        EnterTextBool = true;
    }

    //範囲外に出たらテキストを非表示にする。
    void OnTriggerExit(Collider other)
    {
        EnterTextBool = false;
    }

    public static bool getEnterText()
    {
        return EnterTextBool;
    }

    public static bool getMiniGameStartingBool()
    {
        return MiniGameStartingBool;
    }

    public static int getNowRoomCode()
    {
        return NowRoomCode;
    }

    private IEnumerator Start_Mini()
    {
        yield return null;

        // ミニゲームに切り替える
        SceneManager.LoadScene(RoomCode + 1);
    }

    /*
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(0.1f);
    }
    */
}
