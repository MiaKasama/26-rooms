using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // UnityEngine.SceneManagemntの機能を使用

public class ToMainStage : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        // もし入力したキーがEnterキーならば、中の処理を実行する
        if (Input.GetKeyDown(KeyCode.Return))
        {
            // MainStageに切り替える
            SceneManager.LoadScene(1);
        }
    }
}
