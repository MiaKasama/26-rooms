using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TitleTextFlash : MonoBehaviour
{
    //表示するテキストの内容
    private TextMeshProUGUI TitleFlashText;

    //点滅スパン
    float span = 1.0f;

    //経過時間
    float CurrentTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        TitleFlashText = GetComponent<TextMeshProUGUI>();
        TitleFlashText.text = "Enterキーを押してください";
    }

    // Update is called once per frame
    void Update()
    {
        CurrentTime += Time.deltaTime;

        if (CurrentTime > span)
            TitleFlashText.text = "";

        if(CurrentTime > 2 * span)
        {
            TitleFlashText.text = "Enterキーを押してください";
            CurrentTime = 0.0f;
        }
    }
}
