using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleIngotSpawn : MonoBehaviour
{
    [SerializeField]
    [Tooltip("生成するGameObject")]
    private GameObject createPrefab;

    [SerializeField]
    [Tooltip("生成するGameObjectの色")]
    private Material[] ColorSet = new Material[26];

    int ColorNo = 0;

    //スポーンする座標と向き
    float x, y, z;
    int angleX, angleY, angleZ;

    //スポーンスパン
    float span = 0.2f;

    //経過時間
    float CurrentTime = 0.0f;

    //ジャラジャラ音
    AudioSource CoinSound;

    //ジャラジャラスパン
    int CoinSpan = 3;

    // Start is called before the first frame update
    void Start()
    {
        x = 0.0f;
        y = 5.0f;
        z = 0.0f;

        angleX = 0;
        angleY = 0;
        angleZ = 0;

        CoinSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        CurrentTime += Time.deltaTime;

        if (CurrentTime > span)
        {
            //指定した範囲内でランダムに位置と向きを決定
            x = Random.Range(-5.0f, 5.0f);
            z = Random.Range(-4.0f, -6.0f);

            angleX = Random.Range(0, 360);
            angleY = Random.Range(0, 360);
            angleZ = Random.Range(0, 360);

            //色をランダムに設定
            ColorNo = Random.Range(0, 26);

            //定義した位置にスポーン
            GameObject CoinRainbow = Instantiate(createPrefab, new Vector3(x, y, z), Quaternion.Euler(angleX, angleY, angleZ));

            CoinSpan++;

            if(CoinSpan > 7)
            {
                CoinSound.Play();
                CoinSpan = 0;
            }

            //マテリアルを設定
            CoinRainbow.GetComponent<MeshRenderer>().material = ColorSet[ColorNo];

            //マテリアルを破壊
            Destroy(CoinRainbow, 2.0f);

            CurrentTime = 0;
        }
    }
}
