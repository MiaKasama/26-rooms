using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WatchNow : MonoBehaviour
{
    //ボタンのオブジェクト
    GameObject Button;

    //現在の時間
    float NowTimeWatch = 0.0f;
    int NowNature = 0;

    //テキスト
    private TextMeshPro NowTimeText;
    string StringText;

    //カウント音
    private AudioSource CountSound;

    //カウント
    float TimeCount = 0.1f;

    //ボタンを押したか
    bool PuchedButton = false;

    //文字の色
    Color FailureColor = new Color(1.0f, 0.0f, 0.0f, 1.0f); //失敗時の赤色
    Color SuccessColor = new Color(1.0f, 1.0f, 0.0f, 1.0f); //成功時は黄色

    //目標タイム
    int PurposeTime;

    //許容誤差
    float ErrorTime;

    //上限と下限
    public static float LowTimeWatch;
    public static float HighTimeWatch;

    //成功か否か
    public static bool SuccessWatch = false;
    public static bool FailureWatch = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessWatch = false;
        FailureWatch = false;

        //ランダムで目標タイムと誤差を決定
        PurposeTime = Random.Range(8, 15);
        ErrorTime = Random.Range(0.1f, 0.5f);

        LowTimeWatch = PurposeTime - ErrorTime;
        HighTimeWatch = PurposeTime + ErrorTime;

        //四捨五入して丸める
        LowTimeWatch = LowTimeWatch * 10;
        LowTimeWatch = Mathf.Round(LowTimeWatch);
        LowTimeWatch = LowTimeWatch / 10;

        HighTimeWatch = HighTimeWatch * 10;
        HighTimeWatch = Mathf.Round(HighTimeWatch);
        HighTimeWatch = HighTimeWatch / 10;

        NowTimeWatch = 0.0f;

        Button = GameObject.Find("StopButton");
        Button.SetActive(false);

        NowTimeText = GetComponent<TextMeshPro>();
        CountSound = GetComponent<AudioSource>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //3秒後に計測開始
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        //ボタン表示
        Button.SetActive(true);

        CountSound.Play();

        while (true)
        {
            yield return new WaitForSeconds(0.1f);

            NowTimeWatch += TimeCount;
            NowNature++;

            //最大99.9秒
            if (NowTimeWatch > 100.0f)
            {
                NowTimeWatch = 99.9f;
                NowNature = 999;
            }

            //四捨五入して丸める
            NowTimeWatch = NowTimeWatch * 10;
            NowTimeWatch = Mathf.Round(NowTimeWatch);
            NowTimeWatch = NowTimeWatch / 10;

            //経過時間を文字列に変換するが、小数点以下が0の時のみ表示の方法を変更
            if (NowNature % 10 == 0)
                StringText = NowTimeWatch.ToString() + ".0";
            else
                StringText = NowTimeWatch.ToString();

            //3秒以下と、ボタンを押した後のみ表示
            if (NowTimeWatch <= 3.0f)
            {
                NowTimeText.text = StringText;

                //1秒おきに音を出す
                if (NowNature % 10 == 0)
                    CountSound.Play();
            }
            else
                NowTimeText.text = "";

            //ボタンを押されたらループを抜ける
            if (PuchedButton == true)
            {
                TimeCount = 0;
                NowTimeText.text = StringText;

                //範囲内なら成功、そうでなければ失敗
                if (LowTimeWatch <= NowTimeWatch && NowTimeWatch <= HighTimeWatch)
                {
                    SuccessWatch = true;
                    NowTimeText.color = SuccessColor;
                }
                else
                {
                    FailureWatch = true;
                    NowTimeText.color = FailureColor;
                }

                break;
            }
        }
    }

    //ボタンを押した場合
    public void OnStopClick()
    {
        PuchedButton = true;
    }

    public static float getLowTimeWatch()
    {
        return LowTimeWatch;
    }

    public static float getHighTimeWatch()
    {
        return HighTimeWatch;
    }

    public static bool getSuccessWatch()
    {
        return SuccessWatch;
    }

    public static bool getFailureWatch()
    {
        return FailureWatch;
    }
}
