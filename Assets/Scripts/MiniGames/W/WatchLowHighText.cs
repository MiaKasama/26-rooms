using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WatchLowHighText : MonoBehaviour
{
    private TextMeshPro RangeTimeText;

    [SerializeField]
    [Tooltip("Low�ł����H")]
    private bool Low = true;

    // Start is called before the first frame update
    void Start()
    {
        RangeTimeText = GetComponent<TextMeshPro>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator Start_frames()
    {
        yield return null;

        if (Low == true)
            RangeTimeText.text = WatchNow.getLowTimeWatch().ToString();
        else
            RangeTimeText.text = WatchNow.getHighTimeWatch().ToString();
    }
}
