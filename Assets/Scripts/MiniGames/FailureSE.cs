using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailureSE : MonoBehaviour
{
    //不正解音
    private AudioSource FailureSound;

    //不正解音は一度だけ
    bool SEOnce = true;

    // Start is called before the first frame update
    void Start()
    {
        SEOnce = true;

        FailureSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (MiniToMain.getFailureTextBool() == true && SEOnce == true)
        {
            SEOnce = false;
            FailureSound.Play();
        }
    }
}
