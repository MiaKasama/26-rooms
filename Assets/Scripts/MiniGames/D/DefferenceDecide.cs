using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefferenceDecide : MonoBehaviour
{
    //正解がどれかを決定する
    public static int ColumnDefference;
    public static int RowDefference;

    //どの文字を表示するかを決定する
    public static int StringDefference;

    // Start is called before the first frame update
    void Start()
    {
        //7×4の中でどれか一つをランダムで決定
        ColumnDefference = Random.Range(0, 7);
        RowDefference = Random.Range(0, 4);

        //文字は五通り
        StringDefference = Random.Range(0, 5);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static int getColumnDefference()
    {
        return ColumnDefference;
    }

    public static int getRowDefference()
    {
        return RowDefference;
    }

    public static int getStringDefference()
    {
        return StringDefference;
    }
}
