using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DefferenceLetter : MonoBehaviour
{
    [SerializeField]
    [Tooltip("何列目にありますか？（0〜6）")]
    private int Column;

    [SerializeField]
    [Tooltip("何行目にありますか？（0〜3）")]
    private int Row;

    TextMeshPro Letter;

    //文字の色
    Color BeforeColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);

    //クリック後
    Color AfterColorR = new Color(1.0f, 0.0f, 0.0f, 1.0f); //クリックした文字
    Color AfterColorB = new Color(0.0f, 0.0f, 1.0f, 1.0f); //正解の文字
    Color AfterColorM = new Color(1.0f, 0.0f, 1.0f, 1.0f); //上二つが一致した場合

    // Start is called before the first frame update
    void Start()
    {
        Letter = GetComponent<TextMeshPro>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //クリック後、クリックした文字と正解の文字の色を変更
        if (DefferenceClicked.getClickDefference() == true)
        {
            if (Column == DefferenceDecide.getColumnDefference() && Row == DefferenceDecide.getRowDefference())
                //正解の色は青く
                Letter.color = AfterColorB;

            if (Column == DefferenceClicked.getClickColumnDefference() && Row == DefferenceClicked.getClickRowDefference())
            {
                //クリックした文字は赤く
                Letter.color = AfterColorR;

                if (Column == DefferenceDecide.getColumnDefference() && Row == DefferenceDecide.getRowDefference())
                    //但し、正解ならマゼンタで
                    Letter.color = AfterColorM;
            }
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        //一つ目は1、仲間外れはl
        if(DefferenceDecide.getStringDefference() == 0)
        {
            if (Column == DefferenceDecide.getColumnDefference() && Row == DefferenceDecide.getRowDefference())
                Letter.text = "l";
            else
                Letter.text = "1";
        }
        //二つ目は二、仲間外れはニ
        if (DefferenceDecide.getStringDefference() == 1)
        {
            if (Column == DefferenceDecide.getColumnDefference() && Row == DefferenceDecide.getRowDefference())
                Letter.text = "ニ";
            else
                Letter.text = "二";
        }
        //三つ目は力、仲間外れはカ
        if (DefferenceDecide.getStringDefference() == 2)
        {
            if (Column == DefferenceDecide.getColumnDefference() && Row == DefferenceDecide.getRowDefference())
                Letter.text = "カ";
            else
                Letter.text = "力";
        }
        //四つ目はル、仲間外れはﾉﾚ
        if (DefferenceDecide.getStringDefference() == 3)
        {
            if (Column == DefferenceDecide.getColumnDefference() && Row == DefferenceDecide.getRowDefference())
                Letter.text = "ﾉﾚ";
            else
                Letter.text = "ル";
        }
        //五つ目は外、仲間外れはﾀﾄ
        if (DefferenceDecide.getStringDefference() == 4)
        {
            if (Column == DefferenceDecide.getColumnDefference() && Row == DefferenceDecide.getRowDefference())
                Letter.text = "ﾀﾄ";
            else
                Letter.text = "外";
        }

        Letter.color = BeforeColor;
    }
}
