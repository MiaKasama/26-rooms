using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefferenceClicked : MonoBehaviour
{
    GameObject clickedGameObject;

    //クリックは一回だけ適用
    bool CanClick = false;
    public static bool ClickDefference = false;

    //クリックしたオブジェクトの行と列
    public static int ClickColumnDefference;
    public static int ClickRowDefference;

    //どの範囲をクリックしたかを見極める用
    [SerializeField]
    [Tooltip("原点から一列目まで")]
    private int ColumnFirst;

    [SerializeField]
    [Tooltip("原点から一行目まで")]
    private int RowFirst;

    [SerializeField]
    [Tooltip("各列の距離")]
    private int ColumnDistance;

    [SerializeField]
    [Tooltip("各行の距離")]
    private int RowDistance;

    //正解かどうか
    public static bool SuccessDefference = false;
    public static bool FailureDefference = false;

    //while文用変数
    int nColumn = 0;
    int nRow = 0;
    float x, y;

    // Start is called before the first frame update
    void Start()
    {
        SuccessDefference = false;
        FailureDefference = false;

        ClickDefference = false;

        ClickColumnDefference = 0;
        ClickRowDefference = 0;

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && CanClick == true)
        {
            Vector3 mousePosition = Input.mousePosition;
            clickedGameObject = null;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(ray, out hit))
            {
                clickedGameObject = hit.collider.gameObject;
            }

            //オブジェクトを取得できた時にだけクリックを有効にする
            if (clickedGameObject != null)
            {
                CanClick = false;
                ClickDefference = true;

                //Debug.Log(clickedGameObject);

                x = mousePosition.x;
                y = mousePosition.y;

                nColumn = 0;
                nRow = 0;

                //クリックしたオブジェクトが何行目・何列目かを取得
                while(true)
                {
                    if(x < ColumnFirst)
                    {
                        ClickColumnDefference = nColumn;

                        break;
                    }
                    else
                    {
                        nColumn++;
                        x = x - ColumnDistance;
                    }
                }

                while (true)
                {
                    if (y < RowFirst)
                    {
                        //逆転してしまっているので直す
                        nRow = 3 - nRow;
                        ClickRowDefference = nRow;

                        break;
                    }
                    else
                    {
                        nRow++;
                        y = y - RowDistance;
                    }
                }

                if (ClickColumnDefference == DefferenceDecide.getColumnDefference() && ClickRowDefference == DefferenceDecide.getRowDefference())
                    SuccessDefference = true;
                else
                    FailureDefference = true;
            }

            //Debug.Log("LeftClick:" + mousePosition);
        }
    }

    //3秒後に一度だけクリック可能に
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        CanClick = true;
    }

    public static int getClickColumnDefference()
    {
        return ClickColumnDefference;
    }

    public static int getClickRowDefference()
    {
        return ClickRowDefference;
    }

    public static bool getClickDefference()
    {
        return ClickDefference;
    }

    public static bool getSuccessDefference()
    {
        return SuccessDefference;
    }

    public static bool getFailureDefference()
    {
        return FailureDefference;
    }
}
