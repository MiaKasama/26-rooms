using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QuizQuestions : MonoBehaviour
{
    bool CanQuiz = false;

    private TextMeshPro QuizText;

    //クイズ数
    int QuizMany = 21;

    //どのクイズを出すか
    int QuizNum = 1;
    int[] QuizNumArray = new int[10];

    //何問目か
    int HowQuiz = 0;

    //正解をどこに置くか
    public static int CorrectPosQuiz = -1;
    int ErrorPos;

    int FirstCorrect;

    //三択の内容
    public static string AnswerQuiz0, AnswerQuiz1, AnswerQuiz2;

    string FirstQuiz0, FirstQuiz1, FirstQuiz2;

    //for文用変数
    int i;

    //ループさせるか
    bool Loop = false;

    // Start is called before the first frame update
    void Start()
    {
        CorrectPosQuiz = -1;

        AnswerQuiz0 = "";
        AnswerQuiz1 = "";
        AnswerQuiz2 = "";

        QuizText = GetComponent<TextMeshPro>();
        QuizText.text = "";

        StartCoroutine(Start_frames());

        for (i = 0; i < QuizNumArray.Length; i++)
            QuizNumArray[i] = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(CanQuiz == true)
        {
            CanQuiz = false;
            HowQuiz++;

            //正解の位置と、クイズの出題内容を決定
            //10問目は特殊な処理をする
            if(HowQuiz < 10)
            {
                CorrectPosQuiz = Random.Range(0, 3);

                while (true)
                {
                    ErrorPos = Random.Range(0, 3);

                    if (CorrectPosQuiz != ErrorPos)
                        break;
                }

                //同じ問題は出題しないように
                while (true)
                {
                    Loop = false;

                    QuizNum = Random.Range(1, QuizMany);

                    for (i = 0; i < (HowQuiz - 1); i++)
                    {
                        if (QuizNumArray[i] == QuizNum)
                            Loop = true;
                    }

                    if (Loop == false)
                        break;
                }

                QuizNumArray[HowQuiz - 1] = QuizNum;
            }
            else
            {
                CorrectPosQuiz = FirstCorrect;

                QuizNum = 0;
            }

            //以下、クイズの内容
            if (QuizNum == 20)
            {
                QuizText.text = "パンはパンでも食べられないパンは？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "フライパン";
                        else if (i == 1)
                            AnswerQuiz1 = "フライパン";
                        else
                            AnswerQuiz2 = "フライパン";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "パンケーキ";
                        else if (i == 1)
                            AnswerQuiz1 = "パンケーキ";
                        else
                            AnswerQuiz2 = "パンケーキ";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "アンパン";
                        else if (i == 1)
                            AnswerQuiz1 = "食パン";
                        else
                            AnswerQuiz2 = "カレーパン";
                    }
                }
            }
            else if (QuizNum == 19)
            {
                QuizText.text = "有名な寒いダジャレと言えば、布団が…";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "吹っ飛んだ";
                        else if (i == 1)
                            AnswerQuiz1 = "吹っ飛んだ";
                        else
                            AnswerQuiz2 = "吹っ飛んだ";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "太んない";
                        else if (i == 1)
                            AnswerQuiz1 = "太んない";
                        else
                            AnswerQuiz2 = "太んない";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "汗臭い";
                        else if (i == 1)
                            AnswerQuiz1 = "カピカピだ";
                        else
                            AnswerQuiz2 = "カビ生えた";
                    }
                }
            }
            else if (QuizNum == 18)
            {
                QuizText.text = "1986年に発売されたゲームとして正しいのは？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "ゼルダの伝説";
                        else if (i == 1)
                            AnswerQuiz1 = "ドラゴンクエスト";
                        else
                            AnswerQuiz2 = "メトロイド";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "ファイナルファンタジー";
                        else if (i == 1)
                            AnswerQuiz1 = "スーパーマリオブラザーズ";
                        else
                            AnswerQuiz2 = "ストリートファイター";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "悪魔城スマブラ";
                        else if (i == 1)
                            AnswerQuiz1 = "新・光神話パルテナの鏡";
                        else
                            AnswerQuiz2 = "スプラトゥーン";
                    }
                }
            }
            else if (QuizNum == 17)
            {
                QuizText.text = "「漸く」の読み方として正しいのは？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "ようやく";
                        else if (i == 1)
                            AnswerQuiz1 = "ようやく";
                        else
                            AnswerQuiz2 = "ようやく";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "しばらく";
                        else if (i == 1)
                            AnswerQuiz1 = "いさぎよく";
                        else
                            AnswerQuiz2 = "すばやく";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "ぐっどらっく";
                        else if (i == 1)
                            AnswerQuiz1 = "いきじごく";
                        else
                            AnswerQuiz2 = "とうきょうだいがく";
                    }
                }
            }
            else if (QuizNum == 16)
            {
                QuizText.text = "中国地方において、北東にある都道府県の名前は？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "鳥取県";
                        else if (i == 1)
                            AnswerQuiz1 = "鳥取県";
                        else
                            AnswerQuiz2 = "鳥取県";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "島根県";
                        else if (i == 1)
                            AnswerQuiz1 = "島根県";
                        else
                            AnswerQuiz2 = "島根県";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "島取県";
                        else if (i == 1)
                            AnswerQuiz1 = "鳥根県";
                        else
                            AnswerQuiz2 = "北海道";
                    }
                }
            }
            else if (QuizNum == 15)
            {
                QuizText.text = "10000に最も近い素数は？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "10007";
                        else if (i == 1)
                            AnswerQuiz1 = "10007";
                        else
                            AnswerQuiz2 = "10007";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "10009";
                        else if (i == 1)
                            AnswerQuiz1 = "9973";
                        else
                            AnswerQuiz2 = "9967";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "2";
                        else if (i == 1)
                            AnswerQuiz1 = "101";
                        else
                            AnswerQuiz2 = "100003";
                    }
                }
            }
            else if (QuizNum == 14)
            {
                QuizText.text = "地球で一番使われている言語は？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "中国語";
                        else if (i == 1)
                            AnswerQuiz1 = "中国語";
                        else
                            AnswerQuiz2 = "中国語";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "英語";
                        else if (i == 1)
                            AnswerQuiz1 = "英語";
                        else
                            AnswerQuiz2 = "英語";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "平家物語";
                        else if (i == 1)
                            AnswerQuiz1 = "源氏物語";
                        else
                            AnswerQuiz2 = "竹取物語";
                    }
                }
            }
            else if (QuizNum == 13)
            {
                QuizText.text = "一般的に「HP」とは何の略？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "ヒットポイント";
                        else if (i == 1)
                            AnswerQuiz1 = "ホームページ";
                        else
                            AnswerQuiz2 = "ヒューレットパッカード";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "ハッピーピンク";
                        else if (i == 1)
                            AnswerQuiz1 = "ハローペイ";
                        else
                            AnswerQuiz2 = "ハイプラス";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "ホッとするパパ";
                        else if (i == 1)
                            AnswerQuiz1 = "ヘンなパンツ";
                        else
                            AnswerQuiz2 = "ハカイのパワー";
                    }
                }
            }
            else if (QuizNum == 12)
            {
                QuizText.text = "干支の「未」はどの動物を表す？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "ヒツジ";
                        else if (i == 1)
                            AnswerQuiz1 = "ヒツジ";
                        else
                            AnswerQuiz2 = "ヒツジ";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "ヘビ";
                        else if (i == 1)
                            AnswerQuiz1 = "ヘビ";
                        else
                            AnswerQuiz2 = "ヘビ";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "トキ";
                        else if (i == 1)
                            AnswerQuiz1 = "ヤンバルクイナ";
                        else
                            AnswerQuiz2 = "コウノトリ";
                    }
                }
            }
            else if (QuizNum == 11)
            {
                QuizText.text = "円周率を3.14として、半径5cmの円の面積は何平方メートル？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "78.5";
                        else if (i == 1)
                            AnswerQuiz1 = "78.5";
                        else
                            AnswerQuiz2 = "78.5";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "77.5";
                        else if (i == 1)
                            AnswerQuiz1 = "79.5";
                        else
                            AnswerQuiz2 = "87.5";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "0.775";
                        else if (i == 1)
                            AnswerQuiz1 = "7950";
                        else
                            AnswerQuiz2 = "87.5398";
                    }
                }
            }
            else if (QuizNum == 10)
            {
                QuizText.text = "世界で一番寒い場所は？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "南極";
                        else if (i == 1)
                            AnswerQuiz1 = "南極";
                        else
                            AnswerQuiz2 = "南極";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "北極";
                        else if (i == 1)
                            AnswerQuiz1 = "北極";
                        else
                            AnswerQuiz2 = "北極";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "あなたの心の闇";
                        else if (i == 1)
                            AnswerQuiz1 = "彼の地の真髄";
                        else
                            AnswerQuiz2 = "平行世界の日本";
                    }
                }
            }
            else if (QuizNum == 9)
            {
                QuizText.text = "焼肉食べたよ、何枚食べた？（ヒント:九九）";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "18枚";
                        else if (i == 1)
                            AnswerQuiz1 = "18枚";
                        else
                            AnswerQuiz2 = "18枚";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "29枚";
                        else if (i == 1)
                            AnswerQuiz1 = "11枚";
                        else
                            AnswerQuiz2 = "8枚";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "10人前";
                        else if (i == 1)
                            AnswerQuiz1 = "牛一頭分";
                        else
                            AnswerQuiz2 = "特上カルビ大好き";
                    }
                }
            }
            else if (QuizNum == 8)
            {
                QuizText.text = "次の内、大富豪にあるメジャールールとして正しいのは？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "8切り";
                        else if (i == 1)
                            AnswerQuiz1 = "イレブンバック";
                        else
                            AnswerQuiz2 = "革命";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "よろめき";
                        else if (i == 1)
                            AnswerQuiz1 = "セイムツー";
                        else
                            AnswerQuiz2 = "切り札請求";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "負けたらケツバット";
                        else if (i == 1)
                            AnswerQuiz1 = "負けたらタイキック";
                        else
                            AnswerQuiz2 = "負けたらビンタ";
                    }
                }
            }
            else if (QuizNum == 7)
            {
                QuizText.text = "ブラジル発祥のスポーツとして正しいのは？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "フェンシング";
                        else if (i == 1)
                            AnswerQuiz1 = "フェンシング";
                        else
                            AnswerQuiz2 = "フェンシング";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "サッカー";
                        else if (i == 1)
                            AnswerQuiz1 = "サッカー";
                        else
                            AnswerQuiz2 = "サッカー";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "阿波踊り";
                        else if (i == 1)
                            AnswerQuiz1 = "ソーラン節";
                        else
                            AnswerQuiz2 = "東京音頭";
                    }
                }
            }
            else if (QuizNum == 6)
            {
                QuizText.text = "猫の語源と言われている物は？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "寝る子";
                        else if (i == 1)
                            AnswerQuiz1 = "寝る子";
                        else
                            AnswerQuiz2 = "寝る子";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "根っこ";
                        else if (i == 1)
                            AnswerQuiz1 = "根っこ";
                        else
                            AnswerQuiz2 = "根っこ";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "何となく";
                        else if (i == 1)
                            AnswerQuiz1 = "そもそも説が無い";
                        else
                            AnswerQuiz2 = "真実は猫のみぞ知る";
                    }
                }
            }
            else if (QuizNum == 5)
            {
                QuizText.text = "空気中に最も含まれている元素は？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "窒素";
                        else if (i == 1)
                            AnswerQuiz1 = "窒素";
                        else
                            AnswerQuiz2 = "窒素";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "酸素";
                        else if (i == 1)
                            AnswerQuiz1 = "酸素";
                        else
                            AnswerQuiz2 = "酸素";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "ニホニウム";
                        else if (i == 1)
                            AnswerQuiz1 = "臭素";
                        else
                            AnswerQuiz2 = "金";
                    }
                }
            }
            else if (QuizNum == 4)
            {
                QuizText.text = "ダブル、トリプル、さてその次は？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "クアドラプル";
                        else if (i == 1)
                            AnswerQuiz1 = "クアドラプル";
                        else
                            AnswerQuiz2 = "クアドラプル";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "フォースブル";
                        else if (i == 1)
                            AnswerQuiz1 = "スクエアブル";
                        else
                            AnswerQuiz2 = "エイプリブル";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "ダブルダブル";
                        else if (i == 1)
                            AnswerQuiz1 = "ツギトリプル";
                        else
                            AnswerQuiz2 = "ヨバンメプル";
                    }
                }
            }
            else if (QuizNum == 3)
            {
                QuizText.text = "光の三原色は赤、青、そして何色？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "緑";
                        else if (i == 1)
                            AnswerQuiz1 = "緑";
                        else
                            AnswerQuiz2 = "緑";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "黄色";
                        else if (i == 1)
                            AnswerQuiz1 = "黄色";
                        else
                            AnswerQuiz2 = "黄色";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "瓶覗き";
                        else if (i == 1)
                            AnswerQuiz1 = "ウィスタリア";
                        else
                            AnswerQuiz2 = "梅鼠";
                    }
                }
            }
            else if (QuizNum == 2)
            {
                QuizText.text = "「猫に小判」と同じ意味を持つことわざは？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "豚に真珠";
                        else if (i == 1)
                            AnswerQuiz1 = "馬の耳に念仏";
                        else
                            AnswerQuiz2 = "兎に祭文";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "犬も歩けば棒に当たる";
                        else if (i == 1)
                            AnswerQuiz1 = "猿も木から落ちる";
                        else
                            AnswerQuiz2 = "立つ鳥跡を濁さず";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "カピバラにエメラルド";
                        else if (i == 1)
                            AnswerQuiz1 = "アルマジロにルビー";
                        else
                            AnswerQuiz2 = "コアラにサファイア";
                    }
                }
            }
            else if (QuizNum == 1)
            {
                QuizText.text = "1 + 1は？";

                for (i = 0; i < 3; i++)
                {
                    if (i == CorrectPosQuiz)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "2";
                        else if (i == 1)
                            AnswerQuiz1 = "2";
                        else
                            AnswerQuiz2 = "2";
                    }
                    else if (i == ErrorPos)
                    {
                        if (i == 0)
                            AnswerQuiz0 = "田んぼの田";
                        else if (i == 1)
                            AnswerQuiz1 = "田中の田";
                        else
                            AnswerQuiz2 = "Windowsのロゴ";
                    }
                    else
                    {
                        if (i == 0)
                            AnswerQuiz0 = "いえーい！";
                        else if (i == 1)
                            AnswerQuiz1 = "やっふー！";
                        else
                            AnswerQuiz2 = "ひゃほい！";
                    }
                }
            }
            else if (QuizNum == 0)
            {
                QuizText.text = "一問目の問題の答えは？";

                AnswerQuiz0 = FirstQuiz0;
                AnswerQuiz1 = FirstQuiz1;
                AnswerQuiz2 = FirstQuiz2;
            }

            //一問目の解答を格納
            if (HowQuiz == 1)
            {
                FirstQuiz0 = AnswerQuiz0;
                FirstQuiz1 = AnswerQuiz1;
                FirstQuiz2 = AnswerQuiz2;

                FirstCorrect = CorrectPosQuiz;
            }
        }

        //正解したら次の問題を出す
        if (HowQuiz < QuizButtonPush.getNextHowQuiz() && QuizButtonPush.getNextHowQuiz() <= QuizNumArray.Length && 0 < HowQuiz)
            CanQuiz = true;
    }

    //3秒後にクイズ出題
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        CanQuiz = true;
    }

    public static int getCorrectPosQuiz()
    {
        return CorrectPosQuiz;
    }

    public static string getAnswerQuiz0()
    {
        return AnswerQuiz0;
    }

    public static string getAnswerQuiz1()
    {
        return AnswerQuiz1;
    }

    public static string getAnswerQuiz2()
    {
        return AnswerQuiz2;
    }
}
