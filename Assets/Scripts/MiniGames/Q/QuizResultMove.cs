using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizResultMove : MonoBehaviour
{
    //到達するy座標
    float YLim = -0.5f;

    float y;

    // Start is called before the first frame update
    void Start()
    {
        y = this.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        //プレートを移動する
        if ((QuizButtonPush.getSuccessQuiz() == true || QuizButtonPush.getFailureQuiz() == true) && y < YLim)
        {
            this.transform.position += 0.1f * transform.up;
            y = this.transform.position.y;
        }
    }
}
