using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuizButtonText : MonoBehaviour
{
    [SerializeField]
    [Tooltip("ボタンのコード")]
    private int ButtonCode = 0;

    private TextMeshProUGUI ButtonText;

    // Start is called before the first frame update
    void Start()
    {
        ButtonText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ButtonCode == 0)
            ButtonText.text = QuizQuestions.getAnswerQuiz0();
        else if (ButtonCode == 1)
            ButtonText.text = QuizQuestions.getAnswerQuiz1();
        else if (ButtonCode == 2)
            ButtonText.text = QuizQuestions.getAnswerQuiz2();
    }
}
