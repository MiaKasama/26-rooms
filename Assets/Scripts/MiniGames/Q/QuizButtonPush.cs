using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizButtonPush : MonoBehaviour
{
    //ボタンの表示
    GameObject QuizButton;

    //解答の情報
    int YourAnswerQuiz = -1;

    //何問目か
    int HowQuiz = 0;
    public static int NextHowQuiz = 1;

    //正解音
    AudioSource QuizSE;

    //成功か否か
    public static bool SuccessQuiz = false;
    public static bool FailureQuiz = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessQuiz = false;
        FailureQuiz = false;

        NextHowQuiz = 1;

        QuizButton = GameObject.Find("Buttons");
        QuizButton.SetActive(false);

        QuizSE = GetComponent<AudioSource>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if(HowQuiz == NextHowQuiz)
        {
            if(YourAnswerQuiz == QuizQuestions.getCorrectPosQuiz())
            {
                if(HowQuiz < 10)
                {
                    QuizSE.Play();
                    NextHowQuiz++;
                }
                else
                {
                    SuccessQuiz = true;
                    QuizButton.SetActive(false);
                }
            }
            else
            {
                FailureQuiz = true;
                QuizButton.SetActive(false);
            }
        }
    }

    //3秒後にボタン表示
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        //ボタン表示
        QuizButton.SetActive(true);
    }

    // 左ボタンが押された場合
    public void OnLeftClick()
    {
        YourAnswerQuiz = 0;
        HowQuiz++;
    }

    // 中ボタンが押された場合
    public void OnCenterClick()
    {
        YourAnswerQuiz = 1;
        HowQuiz++;
    }

    // 右ボタンが押された場合
    public void OnRightClick()
    {
        YourAnswerQuiz = 2;
        HowQuiz++;
    }

    public static int getNextHowQuiz()
    {
        return NextHowQuiz;
    }

    public static bool getSuccessQuiz()
    {
        return SuccessQuiz;
    }

    public static bool getFailureQuiz()
    {
        return FailureQuiz;
    }
}
