using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QuizHowCorrect : MonoBehaviour
{
    private TextMeshPro CorrectText;

    //����
    int ResultNum;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CorrectText = GetComponent<TextMeshPro>();

        if (QuizButtonPush.getSuccessQuiz() == true)
            ResultNum = 10;
        else
            ResultNum = QuizButtonPush.getNextHowQuiz() - 1;

        CorrectText.text = ResultNum.ToString();
    }
}
