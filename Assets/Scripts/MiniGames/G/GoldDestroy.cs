using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldDestroy : MonoBehaviour
{
    //オブジェクト取得
    private Rigidbody rb;

    //合格判定範囲
    int RangeGold = 10;

    //合格判定
    public static bool SuccessGold = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessGold = false;

        rb = this.transform.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //どれか一つのオブジェクトでも一定の高さ以上で停止したら合格
        //スポーン地点では判定を起こさないようにする
        if (RangeGold <= this.transform.position.y && 18 >= this.transform.position.y && rb.velocity.magnitude <= 0.05)
            SuccessGold = true;

        if (this.transform.position.y <= -20)
            Destroy(gameObject);
    }

    public static bool getSuccessGold()
    {
        return SuccessGold;
    }
}
