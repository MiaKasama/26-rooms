using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldSpawnSlide : MonoBehaviour
{
    //スポーンを始めるかどうか
    bool StartStand = false;

    [SerializeField]
    [Tooltip("台の移動速度")]
    float speed = 0.1f;

    [SerializeField]
    [Tooltip("移動できる左右の限界")]
    float side = 10.0f;

    [SerializeField]
    [Tooltip("生成するGameObject1")]
    private GameObject createPrefab1;

    [SerializeField]
    [Tooltip("生成するGameObject2")]
    private GameObject createPrefab2;

    //スポーンする向き
    int angleX, angleY, angleZ;

    //乱数
    int n;

    //落下音
    private AudioSource FallSound;

    // Start is called before the first frame update
    void Start()
    {
        FallSound = GetComponent<AudioSource>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //3秒後にスタート
        if (StartStand == true && GoldDestroy.getSuccessGold() == false)
        {
            //座標を取得
            Vector3 pos = this.transform.position;

            // 左に移動
            if (-side <= pos.x)
                speed = -1 * speed;

            // 右に移動
            if (pos.x <= side)
                speed = -1 * speed;

            this.transform.Translate(speed, 0.0f, 0.0f);

            //スペースキーでスポーン
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //ランダムに向きとオブジェクトを決定
                angleX = Random.Range(0, 360);
                angleY = Random.Range(0, 360);
                angleZ = Random.Range(0, 360);
                n = Random.Range(0, 16);

                if (n != 0)
                {
                    GameObject Gold = Instantiate(createPrefab1, pos, Quaternion.Euler(angleX, angleY, angleZ));
                    FallSound.Play();
                }
                else
                {
                    GameObject Silver = Instantiate(createPrefab2, pos, Quaternion.Euler(angleX, angleY, angleZ));
                    FallSound.Play();
                }
            }
        }
    }

    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartStand = true;
    }
}
