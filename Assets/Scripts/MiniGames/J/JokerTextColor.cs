using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class JokerTextColor : MonoBehaviour
{
    TextMeshPro Joker;

    //文字の色
    Color BlackColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
    Color WhiteColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);

    bool ColorCheck = true;

    int Second = 0;

    //点滅時の音
    private AudioSource FlashSound;

    // Start is called before the first frame update
    void Start()
    {
        Joker = GetComponent<TextMeshPro>();

        FlashSound = GetComponent<AudioSource>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if(ColorCheck == true && Second <= 6)
        {
            ColorCheck = false;

            StartCoroutine(ColorCoroutine());
        }

        if (Second > 6)
            Joker.color = BlackColor;
    }

    //0.5秒毎に色を変える
    private IEnumerator ColorCoroutine()
    {
        yield return new WaitForSeconds(0.5f);

        Joker.color = BlackColor;

        yield return new WaitForSeconds(0.5f);

        Joker.color = WhiteColor;

        Second++;
        ColorCheck = true;
    }

    //3秒後に点滅音
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        while(true)
        {
            if (Second > 5)
                break;
            else
            {
                FlashSound.Play();

                yield return new WaitForSeconds(0.5f);
            }
        }
    }
}
