using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JokerStartCard : MonoBehaviour
{
    //カードのオブジェクト
    GameObject joker;
    GameObject ace;
    GameObject king;
    GameObject queen;
    GameObject jack;

    //カードのX座標
    float CardX;

    [SerializeField]
    [Tooltip("カードのy座標")]
    float CardY = 2.0f;

    [SerializeField]
    [Tooltip("カードのz座標")]
    float CardZ = -3.0f;

    //カードの番号を格納
    int[] NumArrayJoker = new int[5];

    //for文用変数
    int i, j;

    // Start is called before the first frame update
    void Start()
    {
        joker = GameObject.Find("Joker");
        ace = GameObject.Find("Ace");
        king = GameObject.Find("King");
        queen = GameObject.Find("Queen");
        jack = GameObject.Find("Jack");

        i = 0;

        //-2〜2でランダムに数値を得る
        while (i < NumArrayJoker.Length)
        {
            NumArrayJoker[i] = Random.Range(-2, 3);

            if (i != 0)
            {
                //数値がダブったらやり直し
                for (j = 0; j < i; j++)
                {
                    if (NumArrayJoker[i] == NumArrayJoker[j])
                        i--;
                }
            }

            //カードの座標を決定
            CardX = 3 * NumArrayJoker[i];

            if(i == 0)
            {
                joker.transform.position = new Vector3(CardX, CardY, CardZ);
            }

            if (i == 1)
            {
                ace.transform.position = new Vector3(CardX, CardY, CardZ);
            }

            if (i == 2)
            {
                king.transform.position = new Vector3(CardX, CardY, CardZ);
            }

            if (i == 3)
            {
                queen.transform.position = new Vector3(CardX, CardY, CardZ);
            }

            if (i == 4)
            {
                jack.transform.position = new Vector3(CardX, CardY, CardZ);
            }

            i++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
