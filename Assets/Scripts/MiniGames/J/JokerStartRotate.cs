using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JokerStartRotate : MonoBehaviour
{
    bool StartRotate = false;

    //回転速度
    float RotateSpeed = 6.0f;

    //回転回数
    int Count;
    float CountMax;

    //シャッフルを開始するために
    public static bool ShuffleStarterJoker = false;

    // Start is called before the first frame update
    void Start()
    {
        ShuffleStarterJoker = false;

        CountMax = 180 / RotateSpeed;

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (StartRotate == true)
        {
            this.transform.Rotate(0.0f, RotateSpeed, 0.0f);

            Count++;
        }

        if (Count >= CountMax)
        {
            StartRotate = false;
            ShuffleStarterJoker = true;
        }
    }

    //6秒後に回転
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(6.0f);

        StartRotate = true;
    }

    public static bool getShuffleStarterJoker()
    {
        return ShuffleStarterJoker;
    }
}
