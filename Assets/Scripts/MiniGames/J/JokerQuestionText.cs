using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class JokerQuestionText : MonoBehaviour
{
    private TextMeshProUGUI WhichJoker;

    // Start is called before the first frame update
    void Start()
    {
        WhichJoker = GetComponent<TextMeshProUGUI>();
        WhichJoker.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if(JokerCardChange.getQuestionJoker() == true)
            WhichJoker.text = "ジョーカーはどぉれだっ";

    }
}
