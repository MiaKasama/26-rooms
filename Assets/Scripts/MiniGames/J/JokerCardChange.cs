using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JokerCardChange : MonoBehaviour
{
    //シャッフルを行うか
    bool CanShuffle = true;

    //入れ替えるカード
    GameObject Card1;
    GameObject Card2;

    int Card1Code = -3;
    int Card2Code = 3;

    //カードの座標
    float Card1posX, Card2posX;

    [SerializeField]
    [Tooltip("入れ替えにかける時間の半分(フレーム)")]
    float ShuffleFrame = 30.0f;

    [SerializeField]
    [Tooltip("入れ替え中のy方向の移動量")]
    float ShuffleMoveY = 3.0f;

    //カード移動速度
    float SpeedX, SpeedY;

    //経過時間
    float CurrentTime;

    //何回シャッフルするか
    int ShuffleCount;
    int Count = 0;

    //シャッフル音
    private AudioSource ShuffleSound;

    //問題開始
    public static bool QuestionJoker = false;

    // Start is called before the first frame update
    void Start()
    {
        ShuffleSound = GetComponent<AudioSource>();

        QuestionJoker = false;
        
        SpeedY = ShuffleMoveY / ShuffleFrame;

        ShuffleCount = Random.Range(4, 15);
    }

    // Update is called once per frame
    void Update()
    {
        //カードを裏返してからシャッフルをスタート
        if(CanShuffle == true && Count < ShuffleCount && JokerStartRotate.getShuffleStarterJoker() == true)
        {
            CanShuffle = false;

            //入れ替えるカードの決定
            Card1Code = Random.Range(-2, 3);

            if(Card1Code == -2)
                Card1 = GameObject.Find("Joker");
            else if(Card1Code == -1)
                Card1 = GameObject.Find("Ace");
            else if (Card1Code == 0)
                Card1 = GameObject.Find("King");
            else if (Card1Code == 1)
                Card1 = GameObject.Find("Queen");
            else if (Card1Code == 2)
                Card1 = GameObject.Find("Jack");

            //選ぶ二枚のカードが被らないように
            while (true)
            {
                Card2Code = Random.Range(-2, 3);

                if (Card1Code != Card2Code)
                    break;
            }

            if (Card2Code == -2)
                Card2 = GameObject.Find("Joker");
            else if (Card2Code == -1)
                Card2 = GameObject.Find("Ace");
            else if (Card2Code == 0)
                Card2 = GameObject.Find("King");
            else if (Card2Code == 1)
                Card2 = GameObject.Find("Queen");
            else if (Card2Code == 2)
                Card2 = GameObject.Find("Jack");

            //座標の取得
            Card1posX = Card1.transform.position.x;
            Card2posX = Card2.transform.position.x;

            SpeedX = (Card2posX - Card1posX) / (2 *ShuffleFrame);
        }

        //入れ替え動作
        if(CanShuffle == false)
        {
            //シャッフル始まりのみ音出し
            if (CurrentTime == 0 && Count != ShuffleCount)
                ShuffleSound.Play();

            CurrentTime++;

            if (ShuffleFrame >= CurrentTime)
            {
                Card1.transform.Translate(SpeedX, SpeedY, 0.0f);
                Card2.transform.Translate(-SpeedX, -SpeedY, 0.0f);
            }
            else if (ShuffleFrame * 2 >= CurrentTime)
            {
                Card1.transform.Translate(SpeedX, -SpeedY, 0.0f);
                Card2.transform.Translate(-SpeedX, SpeedY, 0.0f);
            }
            else
            {
                CanShuffle = true;
                CurrentTime = 0;

                Count++;
            }
        }

        //シャッフルが終わって0.5秒後に問題開始
        if(Count == ShuffleCount)
        {
            StartCoroutine(QuestionCoroutine());
            //Count++;
        }
    }

    private IEnumerator QuestionCoroutine()
    {
        yield return new WaitForSeconds(0.5f);

        QuestionJoker = true;
    }

    public static bool getQuestionJoker()
    {
        return QuestionJoker;
    }
}
