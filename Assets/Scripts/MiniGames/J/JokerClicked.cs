using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JokerClicked : MonoBehaviour
{
    GameObject clickedGameObject;

    bool ClickChance = true;

    //回転速度
    float RotateSpeed = 6.0f;

    //回転回数
    int Count = 0;
    float CountMax;

    //正解か否か
    public static bool SuccessJoker = false;
    public static bool FailureJoker = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessJoker = false;
        FailureJoker = false;

        CountMax = 180 / RotateSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && JokerCardChange.getQuestionJoker() == true && ClickChance == true)
        {

            clickedGameObject = null;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(ray, out hit))
            {
                clickedGameObject = hit.collider.gameObject;
            }

            //クリックは一回だけ
            if(clickedGameObject != null)
            {
                ClickChance = false;

                StartCoroutine(RotateCoroutine());
            }
        }
    }

    //クリックしたカードを回転
    private IEnumerator RotateCoroutine()
    {
        while (true)
        {
            yield return null;

            clickedGameObject.transform.Rotate(0.0f, RotateSpeed, 0.0f);

            Count++;

            if (Count >= CountMax)
                break;
        }

        if (clickedGameObject.name == "Joker")
            SuccessJoker = true;
        else
            FailureJoker = true;
    }

    public static bool getSuccessJoker()
    {
        return SuccessJoker;
    }

    public static bool getFailureJoker()
    {
        return FailureJoker;
    }
}
