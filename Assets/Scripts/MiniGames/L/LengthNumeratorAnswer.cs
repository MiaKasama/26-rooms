using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LengthNumeratorAnswer : MonoBehaviour
{
    private TextMeshPro NumeratorText;

    // Start is called before the first frame update
    void Start()
    {
        NumeratorText = GetComponent<TextMeshPro>();

        NumeratorText.text = "?";
    }

    // Update is called once per frame
    void Update()
    {
        if (LengthSlasherMove.getCheckDenomiratorLength() >= -1)
            NumeratorText.SetText(LengthSlasherMove.getCheckDenomiratorLength().ToString());
    }
}
