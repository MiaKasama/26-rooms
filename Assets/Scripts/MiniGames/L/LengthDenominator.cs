using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LengthDenominator : MonoBehaviour
{
    public static int DenoLength;

    private TextMeshPro DenominatorText;

    // Start is called before the first frame update
    void Start()
    {
        //3〜6でランダムに分母の値を得る
        DenoLength = Random.Range(3, 7);

        DenominatorText = GetComponent<TextMeshPro>();

        DenominatorText.SetText(DenoLength.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static int getDenoLength()
    {
        return DenoLength;
    }
}
