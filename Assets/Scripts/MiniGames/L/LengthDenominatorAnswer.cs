using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LengthDenominatorAnswer : MonoBehaviour
{
    int DenoAnswer;

    private TextMeshPro DenominatorText;

    // Start is called before the first frame update
    void Start()
    {
        DenominatorText = GetComponent<TextMeshPro>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        DenominatorText.SetText(LengthDenominator.getDenoLength().ToString());
    }
}
