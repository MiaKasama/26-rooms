using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LengthNumerator : MonoBehaviour
{
    public static int NumeLength;

    private TextMeshPro NumeratorText;

    // Start is called before the first frame update
    void Start()
    {
        NumeLength = 0;

        NumeratorText = GetComponent<TextMeshPro>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator Start_frames()
    {
        yield return null;

        //1〜分母未満でランダムに分子の値を得る
        NumeLength = Random.Range(1, LengthDenominator.getDenoLength());

        NumeratorText.SetText(NumeLength.ToString());
    }

    public static int getNumeLength()
    {
        return NumeLength;
    }
}
