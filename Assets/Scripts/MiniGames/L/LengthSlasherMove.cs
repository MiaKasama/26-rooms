using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LengthSlasherMove : MonoBehaviour
{
    //スラッシャーを動かし始めるかどうか
    bool StartSlasher = false;

    [SerializeField]
    [Tooltip("スラッシャーの移動速度")]
    float speed = 0.1f;

    [SerializeField]
    [Tooltip("移動できる左右の限界")]
    float side = 14.9f;

    [SerializeField]
    [Tooltip("四捨五入する小数点")]
    int DenimalRound = 3;

    [SerializeField]
    [Tooltip("許容誤差")]
    float OKError = 0.05f;

    //誤差
    float Error;

    //左端からの距離
    float TotalLength;

    //得られた分子の値
    public static float CheckDenomiratorLength = -10.0f;

    //決定音
    private AudioSource DecideSound;

    //正解か否か
    public static bool SuccessLength = false;
    public static bool FailureLength = false;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        SuccessLength = false;
        FailureLength = false;

        DecideSound = GetComponent<AudioSource>();

        CheckDenomiratorLength = -10.0f;

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //3秒後にスタート
        if (StartSlasher == true)
        {
            //スラッシャーの座標を取得
            Vector3 pos = this.transform.position;

            // 左に移動
            if (-side <= pos.x)
            {
                if (Input.GetKey(KeyCode.LeftArrow))
                    this.transform.Translate(-speed, 0.0f, 0.0f);
            }

            // 右に移動
            if (pos.x <= side)
            {
                if (Input.GetKey(KeyCode.RightArrow))
                    this.transform.Translate(speed, 0.0f, 0.0f);
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                StartSlasher = false;
                DecideSound.Play();

                TotalLength = pos.x + 15;

                StartCoroutine(SlasherCoroutine());
            }
        }
    }

    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartSlasher = true;
    }

    private IEnumerator SlasherCoroutine()
    {
        for(i = 0; i < 30; i++)
        {
            yield return null;

            this.transform.Translate(0.0f, 0.05f, 0.0f);
        }

        yield return null;

        //あらかじめ決定された分母から、分子の値を決定
        CheckDenomiratorLength = TotalLength * LengthDenominator.getDenoLength() / 30;

        //以下、四捨五入処理
        CheckDenomiratorLength = CheckDenomiratorLength * Mathf.Pow(10, DenimalRound - 1);
        CheckDenomiratorLength = Mathf.Round(CheckDenomiratorLength);
        CheckDenomiratorLength = CheckDenomiratorLength / (Mathf.Pow(10, DenimalRound - 1));

        //誤差取得
        Error = Mathf.Abs(CheckDenomiratorLength - LengthNumerator.getNumeLength());

        //誤差以内なら成功
        if(Error <= OKError)
            SuccessLength = true;
        else
            FailureLength = true;
    }

    public static float getCheckDenomiratorLength()
    {
        return CheckDenomiratorLength;
    }

    public static bool getSuccessLength()
    {
        return SuccessLength;
    }

    public static bool getFailureLength()
    {
        return FailureLength;
    }
}
