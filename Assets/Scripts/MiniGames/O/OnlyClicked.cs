using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlyClicked : MonoBehaviour
{
    GameObject clickedGameObject;

    //クリックは一回だけ
    bool CanClick = false;

    //正解か否か
    public static bool SuccessOnly = false;
    public static bool FailureOnly = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessOnly = false;
        FailureOnly = false;

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && CanClick == true)
        {
            Vector3 mousePosition = Input.mousePosition;
            clickedGameObject = null;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(ray, out hit))
            {
                clickedGameObject = hit.collider.gameObject;
            }

            //オブジェクトを取得できた時にだけクリックを有効にする
            if (clickedGameObject != null)
            {
                CanClick = false;

                if (clickedGameObject.tag == "OnlyObject")
                    SuccessOnly = true;
                else
                    FailureOnly = true;
            }
        }
    }

    //3秒後に一度だけクリック可能に
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        CanClick = true;
    }

    public static bool getSuccessOnly()
    {
        return SuccessOnly;
    }

    public static bool getFailureOnly()
    {
        return FailureOnly;
    }
}
