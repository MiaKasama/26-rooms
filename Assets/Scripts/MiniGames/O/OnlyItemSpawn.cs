using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlyItemSpawn : MonoBehaviour
{
    //prehabの種類は8種、一つ生成を1種、三つ生成を1種、他を二つ、計16個生成する

    [SerializeField]
    [Tooltip("生成するGameObject")]
    private GameObject[] createPrefab = new GameObject[8];

    //一つしか無いゲームオブジェクト
    int OnlyObject = -1;

    //三つあるゲームオブジェクト
    int ThreeObject = -1;

    //スポーンする座標と向き
    float x, y, z;
    int angleX, angleY, angleZ;

    //for文用変数
    int i, j;

    // Start is called before the first frame update
    void Start()
    {
        //スポーンする個数が2個じゃない二つを決定
        OnlyObject = Random.Range(0, 8);

        while(true)
        {
            ThreeObject = Random.Range(0, 8);

            if (OnlyObject != ThreeObject)
                break;
        }

        //一つしかないオブジェクトのみタグを仲間外れにする
        for(i = 0; i < 8; i++)
        {
            if (i == OnlyObject)
                createPrefab[i].tag = "OnlyObject";
            else
                createPrefab[i].tag = "NotOnlyObject";
        }


        //範囲内のどこかにスポーン
        z = -5.5f;
        angleX = 0;
        angleY = 0;

        //まずは全prehabを一つずつスポーン
        for(i = 0; i < 8; i++)
        {
            x = Random.Range(-3.5f, 3.5f);
            y = Random.Range(-0.5f, 2.5f);
            angleZ = Random.Range(0, 360);
            
            Instantiate(createPrefab[i], new Vector3(x, y, z), Quaternion.Euler(angleX, angleY, angleZ));
        }

        //OnlyObject以外を一つずつスポーン
        for (i = 0; i < 8; i++)
        {
            x = Random.Range(-4.5f, 4.5f);
            y = Random.Range(-1.5f, 3.5f);
            angleZ = Random.Range(0, 360);

            if (i != OnlyObject)
                Instantiate(createPrefab[i], new Vector3(x, y, z), Quaternion.Euler(angleX, angleY, angleZ));
        }

        //ThreeObjectをスポーン
        x = Random.Range(-4.5f, 4.5f);
        y = Random.Range(-1.5f, 3.5f);
        angleZ = Random.Range(0, 360);
        Instantiate(createPrefab[ThreeObject], new Vector3(x, y, z), Quaternion.Euler(angleX, angleY, angleZ));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
