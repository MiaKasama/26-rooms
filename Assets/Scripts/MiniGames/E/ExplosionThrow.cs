using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionThrow : MonoBehaviour
{
    //爆弾を動かし始めるかどうか
    bool StartBomb = false;

    //特定の座標に入る事でtrueになる
    bool BlackThrowArea = true;
    bool WhiteThrowArea = false;

    //相手が投げる事でtrueになる
    bool BlackThrowCatch = true;
    bool WhiteThrowCatch = false;

    [SerializeField]
    [Tooltip("スピードは？")]
    float speed = 15.0f;

    //右方向の移動速度
    float speedRight;

    //上方向の移動速度
    float speedUp;

    [SerializeField]
    [Tooltip("力の角度は？")]
    private int angle;
    float angleRadian;

    //現在のx、y座標
    float x, y;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Start_frames());

        //角度をラジアンに
        angleRadian = angle * Mathf.PI / 180;

        speedRight = speed * Mathf.Cos(angleRadian);
        speedUp = speed * Mathf.Sin(angleRadian);
    }

    // Update is called once per frame
    void Update()
    {
        x = this.transform.position.x;
        y = this.transform.position.y;

        //xが5以上、yが2以下で投げられる
        if (x > 5 && y < 2)
            WhiteThrowArea = true;
        else
            WhiteThrowArea = false;

        if (x < -5 && y < 2)
            BlackThrowArea = true;
        else
            BlackThrowArea = false;

        if (BlackThrowArea == true && BlackThrowCatch == true && StartBomb == true)
        {
            BlackThrowArea = false;
            BlackThrowCatch = false;

            WhiteThrowCatch = true;
            
            GetComponent<Rigidbody>().velocity = Vector3.zero;

            StartCoroutine(BlackCoroutine());
        }

        //エンターを押すと投げられる
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (WhiteThrowArea == true && WhiteThrowCatch == true)
            {
                WhiteThrowArea = false;
                WhiteThrowCatch = false;

                BlackThrowCatch = true;

                GetComponent<Rigidbody>().velocity = Vector3.zero;

                StartCoroutine(WhiteCoroutine());
            }
        }

        //カウントダウン0で破壊
        if(ExplosionCountDown.getSecondExplosion() == 0)
            Destroy(gameObject);
    }

    //3秒後にスタート
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartBomb = true;
    }

    private IEnumerator BlackCoroutine()
    {
        yield return null;

        var forceBlack = new Vector3(speedRight, speedUp, 0.0f);
        GetComponent<Rigidbody>().AddForce(forceBlack, ForceMode.VelocityChange);
    }

    private IEnumerator WhiteCoroutine()
    {
        yield return null;

        var forceWhite = new Vector3(-speedRight, speedUp, 0.0f);
        GetComponent<Rigidbody>().AddForce(forceWhite, ForceMode.VelocityChange);
    }
}
