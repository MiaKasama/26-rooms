using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ExplosionCountDown : MonoBehaviour
{
    //カウントダウンを始めるかどうか
    bool StartCount = false;
    bool CountCheck = true;

    //カウント表示
    TextMeshPro CountText;

    //残り何秒か
    public static int SecondExplosion = 6;

    //カウント音
    private AudioSource CountSound;

    // Start is called before the first frame update
    void Start()
    {
        SecondExplosion = 6;

        CountText = GetComponent<TextMeshPro>();
        CountText.SetText(SecondExplosion.ToString());

        CountSound = GetComponent<AudioSource>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //0秒になるまで以下の関数を呼び起こす
        if(StartCount == true && CountCheck == true && SecondExplosion > 0)
        {
            CountCheck = false;

            StartCoroutine(CountDownSecond());
        }
    }

    //3秒後にスタート
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartCount = true;
    }

    //1秒毎にカウントダウン
    private IEnumerator CountDownSecond()
    {
        CountSound.Play();

        yield return new WaitForSeconds(1.0f);

        SecondExplosion--;
        CountText.SetText(SecondExplosion.ToString());

        CountCheck = true;
    }

    public static int getSecondExplosion()
    {
        return SecondExplosion;
    }
}
