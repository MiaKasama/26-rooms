using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionAnim : MonoBehaviour
{
    [SerializeField]
    [Tooltip("爆発エフェクト")]
    GameObject Effect;

    [SerializeField]
    [Tooltip("白にアタッチしてる？")]
    private bool AttachWhite;

    bool ExplosionBool = true;

    // プレイヤーの座標取得
    float x, y, z;

    //爆発音
    private AudioSource ExplosionSound;

    //成功か否か
    public static bool SuccessExplosion = false;
    public static bool FailureExplosion = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessExplosion = false;
        FailureExplosion = false;

        ExplosionSound = GetComponent<AudioSource>();

        Effect.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //カウント0で爆発演出を起こす
        if (ExplosionCountDown.getSecondExplosion() == 0)
        {
            if (ExplosionBool == true)
            {
                //爆弾の位置で爆発させる
                x = GameObject.Find("Bomb").transform.position.x;
                y = GameObject.Find("Bomb").transform.position.y;
                z = GameObject.Find("Bomb").transform.position.z;

                Effect.transform.position = new Vector3(x, y, z);

                Effect.SetActive(true);
                ExplosionBool = false;

                ExplosionSound.Play();

                //爆弾の位置でどっちを破壊するかを決定する
                if(AttachWhite == true && 0 <= x)
                    Destroy(gameObject);

                if(AttachWhite == false && 0 > x)
                    Destroy(gameObject);
            }

            //爆発は一回だけ
            StartCoroutine(EffectSentence());
        }
    }

    IEnumerator EffectSentence()
    {
        yield return new WaitForSeconds(1.0f);

        Effect.SetActive(false);

        //1秒後に成功か失敗か判定する
        if (AttachWhite == true && 0 > x)
            SuccessExplosion = true;

        if (AttachWhite == false && 0 <= x)
            FailureExplosion = true;
    }

    public static bool getSuccessExplosion()
    {
        return SuccessExplosion;
    }

    public static bool getFailureExplosion()
    {
        return FailureExplosion;
    }
}
