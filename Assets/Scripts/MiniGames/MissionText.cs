using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MissionText : MonoBehaviour
{
    //表示するテキストの内容
    [SerializeField]
    [Tooltip("ミッションの内容")]
    string MissionNameStr = "Mission";

    private TextMeshProUGUI MissionName;

    // Start is called before the first frame update
    void Start()
    {
        MissionName = GetComponent<TextMeshProUGUI>();
        MissionName.text = MissionNameStr;

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //3秒間タイトル表示
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        MissionName.text = "";
    }
}
