using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldFailureZone : MonoBehaviour
{
    //失敗か
    public static bool FailureShield = false;

    // Start is called before the first frame update
    void Start()
    {
        FailureShield = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // このエリアに弾が当たったら失敗
    void OnCollisionEnter(Collision collision)
    {
        FailureShield = true;
    }

    public static bool getFailureShield()
    {
        return FailureShield;
    }
}
