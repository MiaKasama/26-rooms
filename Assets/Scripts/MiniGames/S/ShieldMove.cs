using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldMove : MonoBehaviour
{
    //盾を動かし始めるかどうか
    bool StartShield = false;

    [SerializeField]
    [Tooltip("盾の移動速度")]
    float speed = 0.05f;

    [SerializeField]
    [Tooltip("移動できる上の限界")]
    float UpLim = 2.4f;

    [SerializeField]
    [Tooltip("移動できる下の限界")]
    float DownLim = -0.4f;

    [SerializeField]
    [Tooltip("移動できる左右の限界")]
    float RightLim = 1.4f;

    //現在の座標
    float x, y;

    //ガード数
    int GuardCount = 0;

    //ガード音
    private AudioSource GuardSound;

    //成功か
    public static bool SuccessShield = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessShield = false;

        GuardSound = GetComponent<AudioSource>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (StartShield == true)
        {
            //座標を取得
            x = this.transform.position.x;
            y = this.transform.position.y;

            //上に移動
            if (Input.GetKey(KeyCode.UpArrow) && y <= UpLim)
                this.transform.Translate(0.0f, speed, 0.0f);

            //下に移動
            if (Input.GetKey(KeyCode.DownArrow) && y >= DownLim)
                this.transform.Translate(0.0f, -speed, 0.0f);

            //左に移動
            if (Input.GetKey(KeyCode.LeftArrow) && x >= -RightLim)
                this.transform.Translate(-speed, 0.0f, 0.0f);

            //右に移動
            if (Input.GetKey(KeyCode.RightArrow) && x <= RightLim)
                this.transform.Translate(speed, 0.0f, 0.0f);
        }

        if (SuccessShield == true || ShieldFailureZone.getFailureShield() == true)
            StartShield = false;
    }

    // 3秒後にスタート
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartShield = true;
    }

    // 当たった弾を消滅
    void OnCollisionEnter(Collision collision)
    {
        GuardSound.Play();

        Destroy(collision.gameObject);

        GuardCount++;

        if (GuardCount == 25)
            SuccessShield = true;
    }

    public static bool getSuccessShield()
    {
        return SuccessShield;
    }
}
