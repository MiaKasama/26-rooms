using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBulletSpawn : MonoBehaviour
{
    [SerializeField]
    [Tooltip("生成するGameObject(弾丸)")]
    private GameObject createBullet;

    [SerializeField]
    [Tooltip("生成するGameObject(照準)")]
    private GameObject createTarget;

    [SerializeField]
    [Tooltip("短めのスパン")]
    private float ShortSpan = 0.7f;

    [SerializeField]
    [Tooltip("長めのスパン")]
    private float LongSpan = 2.0f;

    //スポーン可能か
    bool CanSpawn = false;

    //スポーンする座標と向き
    float x, y, z, Tz;
    int angleX, angleY, angleZ;

    //スポーンスパン
    float span = 3.1f;

    //スポーンした数
    int HowSpawn = 0;

    //銃声
    private AudioSource BulletSound;

    // Start is called before the first frame update
    void Start()
    {
        BulletSound = GetComponent<AudioSource>();

        z = 15.0f;
        Tz = -6.6f;

        angleX = 0;
        angleY = 0;
        angleZ = 0;

        StartCoroutine(SpawnCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        if (CanSpawn == true && ShieldFailureZone.getFailureShield() == false)
        {
            //ランダムに位置を決定
            x = Random.Range(-1.4f, 1.4f);
            y = Random.Range(-0.4f, 2.4f);

            //定義した位置にスポーン
            GameObject Bullet = Instantiate(createBullet, new Vector3(x, y, z), Quaternion.Euler(angleX, angleY, angleZ));
            GameObject Target = Instantiate(createTarget, new Vector3(x, y, Tz), Quaternion.Euler(angleX, angleY, angleZ));

            BulletSound.Play();

            CanSpawn = false;
            HowSpawn++;

            //スポーンした数によってスパンを変更する
            if (HowSpawn < 25)
            {
                if (HowSpawn == 1 || HowSpawn == 2)
                    span = LongSpan;
                else if (HowSpawn == 4 || HowSpawn == 6)
                    span = LongSpan;
                else if (HowSpawn == 9 || HowSpawn == 12)
                    span = LongSpan;
                else if (HowSpawn == 16 || HowSpawn == 20)
                    span = LongSpan;
                else
                    span = ShortSpan;

                StartCoroutine(SpawnCoroutine());
            }
        }
    }

    //指定秒後にスポーン
    private IEnumerator SpawnCoroutine()
    {
        yield return new WaitForSeconds(span);

        CanSpawn = true;
    }
}
