using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBulletMove : MonoBehaviour
{
    [SerializeField]
    [Tooltip("弾の移動速度")]
    float speed = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ShieldFailureZone.getFailureShield() == false)
            this.transform.Translate(0.0f, 0.0f, -speed);
    }
}
