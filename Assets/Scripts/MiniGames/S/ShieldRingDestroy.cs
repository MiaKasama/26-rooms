using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldRingDestroy : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(DestroyCoroutine());
    }

    private IEnumerator DestroyCoroutine()
    {
        yield return new WaitForSeconds(1.0f);

        if (ShieldFailureZone.getFailureShield() == false)
            Destroy(gameObject);
    }
}
