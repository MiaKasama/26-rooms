using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuccessSE : MonoBehaviour
{
    //正解音
    private AudioSource SuccessSound;

    //正解音は一度だけ
    bool SEOnce = true;

    // Start is called before the first frame update
    void Start()
    {
        SEOnce = true;

        SuccessSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (MiniToMain.getSuccessTextBool() == true && SEOnce == true)
        {
            SEOnce = false;
            SuccessSound.Play();
        }
    }
}
