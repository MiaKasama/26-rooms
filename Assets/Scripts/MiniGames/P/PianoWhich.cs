using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PianoWhich : MonoBehaviour
{
    private TextMeshPro WhichSound;

    [SerializeField]
    [Tooltip("正しい答え")]
    private bool TrueAnswer = true;

    // Start is called before the first frame update
    void Start()
    {
        WhichSound = GetComponent<TextMeshPro>();
        WhichSound.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (PianoClicked.getJudgementPiano() == false)
            StartCoroutine(Answer());
    }

    private IEnumerator Answer()
    {
        yield return new WaitForSeconds(2.0f);

        if (TrueAnswer == true)
            WhichSound.text = PianoTrueAnswer.getKeyboardPiano();
        else
            WhichSound.text = PianoClicked.getClickKeyboardPiano();

        //英語表記を日本語表記に修正
        if (WhichSound.text == "C")
            WhichSound.text = "ド";

        if (WhichSound.text == "D")
            WhichSound.text = "レ";

        if (WhichSound.text == "E")
            WhichSound.text = "ミ";

        if (WhichSound.text == "F")
            WhichSound.text = "ﾌｧ";

        if (WhichSound.text == "G")
            WhichSound.text = "ソ";

        if (WhichSound.text == "A")
            WhichSound.text = "ラ";

        if (WhichSound.text == "B")
            WhichSound.text = "シ";
    }
}
