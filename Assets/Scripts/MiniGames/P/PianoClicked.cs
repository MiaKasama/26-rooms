using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PianoClicked : MonoBehaviour
{
    GameObject clickedGameObject;
    AudioSource PianoSE;

    bool CanClick = false;
    public static bool JudgementPiano = true;

    //クリックした鍵盤の名前
    public static string ClickKeyboardPiano;

    //正解か否か
    public static bool SuccessPiano = false;
    public static bool FailurePiano = false;

    // Start is called before the first frame update
    void Start()
    {
        JudgementPiano = true;

        ClickKeyboardPiano = "";

        SuccessPiano = false;
        FailurePiano = false;

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0) && CanClick == true)
        {
            Vector3 mousePosition = Input.mousePosition;
            clickedGameObject = null;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(ray, out hit))
            {
                clickedGameObject = hit.collider.gameObject;
            }

            //オブジェクトを取得できた時にだけクリックを有効にする
            if (clickedGameObject != null)
            {
                PianoSE = clickedGameObject.GetComponent<AudioSource>();
                PianoSE.Play();
                
                //音は何回でも奏でられるが、判定は一回だけ
                if(JudgementPiano == true)
                    StartCoroutine(JudgementCoroutine());
                {
                    
                }
            }
        }
    }

    //3秒後にクリック可能に
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        CanClick = true;
    }

    //2秒後に判定
    private IEnumerator JudgementCoroutine()
    {
        JudgementPiano = false;

        yield return new WaitForSeconds(2.0f);

        ClickKeyboardPiano = clickedGameObject.name;
        //Debug.Log(ClickKeyboardPiano);

        if (ClickKeyboardPiano == PianoTrueAnswer.getKeyboardPiano())
            SuccessPiano = true;
        else
            FailurePiano = true;
    }

    public static bool getJudgementPiano()
    {
        return JudgementPiano;
    }

    public static string getClickKeyboardPiano()
    {
        return ClickKeyboardPiano;
    }

    public static bool getSuccessPiano()
    {
        return SuccessPiano;
    }

    public static bool getFailurePiano()
    {
        return FailurePiano;
    }
}
