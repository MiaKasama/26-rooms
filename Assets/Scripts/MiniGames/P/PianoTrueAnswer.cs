using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PianoTrueAnswer : MonoBehaviour
{
    //正解の音を奏でる
    int CorrectPiano;

    [SerializeField]
    [Tooltip("格納する音源")]
    private AudioClip[] audioClips = new AudioClip[7];

    AudioSource PianoSE;

    [SerializeField]
    [Tooltip("鍵盤のGameObject")]
    private GameObject[] createPrefab = new GameObject[7];

    GameObject KeyboardPrehab;
    public static string KeyboardPiano;

    //もう一度聞くボタンの表示
    GameObject RepeatButton;

    bool PianoStart = false;

    // Start is called before the first frame update
    void Start()
    {
        CorrectPiano = Random.Range(0, 7);

        PianoSE = GetComponent<AudioSource>();

        RepeatButton = GameObject.Find("RePiano");
        RepeatButton.SetActive(false);

        KeyboardPrehab = createPrefab[CorrectPiano];
        KeyboardPiano = KeyboardPrehab.name;
        //Debug.Log(KeyboardPiano);

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if(PianoStart == true)
        {
            PianoStart = false;

            PianoSE.PlayOneShot(audioClips[CorrectPiano]);
        }
    }

    //3秒後に音を奏でる
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        //ボタン表示
        RepeatButton.SetActive(true);

        PianoStart = true;
    }

    // ボタンが押された場合
    public void OnPianoClick()
    {
        PianoStart = true;
    }

    public static string getKeyboardPiano()
    {
        return KeyboardPiano;
    }
}
