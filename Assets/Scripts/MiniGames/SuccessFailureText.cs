using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SuccessFailureText : MonoBehaviour
{
    private TextMeshProUGUI SuFaText;

    [SerializeField]
    [Tooltip("Failureでの指示表示をしますか")]
    private bool FailureCommand = false;

    // Start is called before the first frame update
    void Start()
    {
        SuFaText = GetComponent<TextMeshProUGUI>();
        SuFaText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if(FailureCommand == false)
        {
            if (MiniToMain.getSuccessTextBool() == true)
                SuFaText.text = "Success!";

            if (MiniToMain.getFailureTextBool() == true)
                SuFaText.text = "Failure...";
        }
        else
        {
            if (MiniToMain.getFailureTextBool() == true)
                StartCoroutine(FailureCoroutine());
        }
    }

    private IEnumerator FailureCoroutine()
    {
        yield return new WaitForSeconds(3.0f);

        SuFaText.text = "Space: まだ諦めるわけにはいかない…!\nEnter: 今のところは止めておこう…";
    }
}
