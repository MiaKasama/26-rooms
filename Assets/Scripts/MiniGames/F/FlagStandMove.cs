using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagStandMove : MonoBehaviour
{
    [SerializeField]
    [Tooltip("移動速度")]
    float speed = 0.1f;

    [SerializeField]
    [Tooltip("移動できる左の限界")]
    float LeftSide = -9.0f;

    [SerializeField]
    [Tooltip("移動できる右の限界")]
    float RightSide = 11.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x >= RightSide || LeftSide >= this.transform.position.x)
            speed = -1 * speed;

        this.transform.Translate(speed, 0.0f, 0.0f);
    }
}
