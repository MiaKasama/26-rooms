using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagParent : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("FlagGround"))
        {
            // 触れたobjの親を移動床にする
            other.transform.SetParent(transform);
        }
    }
    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("FlagGround"))
        {
            // 触れたobjの親をなくす
            other.transform.SetParent(null);
        }
    }
}
