using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagColliderSwitch : MonoBehaviour
{
    BoxCollider BoxCol;

    float thisY;
    float charaY;

    // Start is called before the first frame update
    void Start()
    {
        BoxCol = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        thisY = this.transform.position.y;

        if (thisY > FlagController.getyFlag())
            BoxCol.enabled = false;
        else
            BoxCol.enabled = true;
    }
}
