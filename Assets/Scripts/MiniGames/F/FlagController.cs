using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagController : MonoBehaviour
{
    //操作可能か
    bool StartControl = false;

    private Rigidbody rb;
    private float distance;

    [SerializeField]
    [Tooltip("移動速度")]
    float speed = 0.1f;

    [SerializeField]
    [Tooltip("ジャンプ力")]
    private int upForce = 300;

    //ジャンプ音
    private AudioSource JumpSound;

    //現在のxとy座標
    public static float xFlag;
    public static float yFlag;

    //オブジェクトの向き
    //右向きか
    bool RightHead = true;

    //成功か否か
    public static bool SuccessFlag = false;
    public static bool FailureFlag = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessFlag = false;
        FailureFlag = false;

        JumpSound = GetComponent<AudioSource>();

        xFlag = this.transform.position.x;
        yFlag = this.transform.position.y;

        StartCoroutine(Start_frames());

        rb = GetComponent<Rigidbody>();
        distance = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        //3秒後から操作可能に
        if(StartControl == true)
        {
            Vector3 rayPosition = transform.position + new Vector3(0.0f, 0.0f, 0.0f);
            Ray ray = new Ray(rayPosition, Vector3.down);
            bool isGround = Physics.Raycast(ray, distance);

            //地に足がついていたらスペースキーでジャンプ出来る
            if (Input.GetKeyDown(KeyCode.Space) && isGround == true)
            {
                rb.AddForce(new Vector3(0, upForce, 0));
                JumpSound.Play();

                StartCoroutine(ForceCoroutine());
            }

            //左右に移動
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                this.transform.Translate(speed, 0.0f, 0.0f);
                RightHead = false;
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                this.transform.Translate(speed, 0.0f, 0.0f);
                RightHead = true;
            }

            //キャラの座標を取得
            xFlag = this.transform.position.x;
            yFlag = this.transform.position.y;
        }

        //ジャンプ動作の過程で向きが変わらないように
        if (RightHead == true)
            this.transform.rotation = Quaternion.Euler(0f, 0f, 0f);

        if (RightHead == false)
            this.transform.rotation = Quaternion.Euler(0f, 180f, 0f);

        //落下したらゲームオーバー
        if (yFlag <= -10)
            FailureFlag = true;

        Debug.Log(yFlag);

        //ゴール地点で一定以上の高さにいたらクリア、そうじゃなければ失敗
        if (xFlag >= 13.5)
        {
            if (yFlag >= 7.0)
                SuccessFlag = true;
            else
                FailureFlag = true;
        }

        //ゲーム終了で操作不能に
        if (SuccessFlag == true || FailureFlag == true)
            StartControl = false;
    }

    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartControl = true;
    }

    private IEnumerator ForceCoroutine()
    {
        yield return new WaitForSeconds(0.5f);

        rb.AddForce(new Vector3(0, -upForce, 0));
    }

    public static float getxFlag()
    {
        return xFlag;
    }

    public static float getyFlag()
    {
        return yFlag;
    }

    public static bool getSuccessFlag()
    {
        return SuccessFlag;
    }

    public static bool getFailureFlag()
    {
        return FailureFlag;
    }
}
