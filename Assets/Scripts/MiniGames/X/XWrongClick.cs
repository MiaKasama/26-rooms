using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XWrongClick : MonoBehaviour
{
    //クリック可能か
    bool CanClick = false;

    GameObject clickedGameObject;

    //間違いテキスト表示用
    public static bool WrongXXX = false;

    //正解か
    public static bool SuccessXXX = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessXXX = false;

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        WrongXXX = false;

        if (Input.GetMouseButtonDown(0) && CanClick == true)
        {
            clickedGameObject = null;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(ray, out hit))
            {
                clickedGameObject = hit.collider.gameObject;
            }

            //交差箇所のコライダーのタグを感知したら実行
            if (clickedGameObject != null)
            {
                if (clickedGameObject.tag == "XWrong")
                    WrongXXX = true;

                if (clickedGameObject.tag == "XSuccess")
                    SuccessXXX = true;
            }
        }   

        if (XClickOver.getFailureXXX() == true || SuccessXXX == true)
            CanClick = false;
    }

    //3秒後にクリック可能に
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        CanClick = true;
    }

    public static bool getWrongXXX()
    {
        return WrongXXX;
    }

    public static bool getSuccessXXX()
    {
        return SuccessXXX;
    }
}
