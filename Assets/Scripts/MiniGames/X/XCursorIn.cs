using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XCursorIn : MonoBehaviour
{
    public static bool CursorXXX = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CursorXXX = false;

        //特定の位置にカーソルを近づけた時の処理
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();

        if (Physics.Raycast(ray, out hit))
        {
            GameObject XSphere = hit.collider.gameObject;

            if (XSphere.tag == "XSuccess")
                CursorXXX = true;
        }
    }

    public static bool getCursorXXX()
    {
        return CursorXXX;
    }
}
