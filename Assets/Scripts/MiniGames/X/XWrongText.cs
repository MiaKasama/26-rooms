using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class XWrongText : MonoBehaviour
{
    private TextMeshPro Wrong;

    [SerializeField]
    [Tooltip("表示するテキスト一覧")]
    private string[] WrongString = new string[10];

    //どれを表示するか
    int TextNum;

    //経過時間
    int CurrentTime;

    // Start is called before the first frame update
    void Start()
    {
        Wrong = GetComponent<TextMeshPro>();

        Wrong.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        //適当な時間が経過したらテキスト表示を無効に
        CurrentTime++;            

        if (CurrentTime >= 500)
        {
            Wrong.text = "";
            CurrentTime = 0;
        }

        if (XWrongClick.getWrongXXX() == true)
        {
            CurrentTime = 0;

            TextNum = Random.Range(0, WrongString.Length);
            Wrong.text = WrongString[TextNum];
        }

        //正解に近づいたら特殊なテキストを表示
        if (XCursorIn.getCursorXXX() == true)
        {
            Wrong.text = "ほぇ？";
            CurrentTime = 500;
        }

        if (XWrongClick.getSuccessXXX() == true)
            Wrong.text = "XXXXXXXXXXXXXXXXXXXXXXXX";
    }
}
