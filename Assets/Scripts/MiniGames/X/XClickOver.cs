using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class XClickOver : MonoBehaviour
{
    //強制終了するか
    public static bool FailureXXX = false;
    bool FailureEnd = true;

    //経過時間の配列
    int[] CurrentTimeArray = new int[5];

    //経過時間をカウントするか
    bool[] CanCountArray = new bool[5];

    //次に経過時間の処理を行う配列の番号
    int NextCounter = 0;

    //失敗音
    private AudioSource OverSound;

    //プレートの落下速度
    float speed = 0.05f;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        FailureXXX = false;

        OverSound = GetComponent<AudioSource>();

        for (i = 0; i < CurrentTimeArray.Length; i++)
        {
            CurrentTimeArray[i] = 0;
            CanCountArray[i] = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (i = 0; i < CurrentTimeArray.Length; i++)
        {
            //trueになっている物のみをカウント
            if (CanCountArray[i] == true)
                CurrentTimeArray[i]++;

            //一定時間経過でカウントを0にし、falseに戻す
            if (CurrentTimeArray[i] >= 180)
            {
                CurrentTimeArray[i] = 0;
                CanCountArray[i] = false;
            }    
        }

        //配列が全てtrueだったら強制終了
        for (i = 0; i < CanCountArray.Length; i++)
        {
            if (CanCountArray[i] == false)
                break;

            if (i == CanCountArray.Length - 1)
                FailureXXX = true;
        }
        
        //クリックする度にtrueを入れる
        if (Input.GetMouseButtonDown(0))
        {
            CanCountArray[NextCounter] = true;

            //次に入れる配列の箇所を変更
            NextCounter++;

            if (NextCounter == CanCountArray.Length)
                NextCounter = 0;
        }

        //強制終了直前の処理
        if (FailureXXX == true && FailureEnd == true && XWrongClick.getSuccessXXX() == false)
        {
            if (this.transform.position.y > 5.7f)
                this.transform.Translate(0.0f, -speed, 0.0f);
            else
            {
                FailureEnd = false;
                OverSound.Play();

                //StartCoroutine(FailureCoroutine());
            }
        }
    }

    //5秒後に強制終了
    private IEnumerator FailureCoroutine()
    {
        yield return new WaitForSeconds(5.0f);

        SceneManager.LoadScene(1);
    }

    public static bool getFailureXXX()
    {
        return FailureXXX;
    }
}
