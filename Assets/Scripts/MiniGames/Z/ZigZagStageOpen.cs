using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZigZagStageOpen : MonoBehaviour
{
    //ランダムでどれか一つのステージ以外を非アクティブにする
    int StageCode;

    GameObject Stage1;
    GameObject Stage2;
    GameObject Stage3;
    GameObject Stage4;
    GameObject Stage5;

    // Start is called before the first frame update
    void Start()
    {
        StageCode = Random.Range(0, 5);

        Stage1 = GameObject.Find("Route1");
        Stage2 = GameObject.Find("Route2");
        Stage3 = GameObject.Find("Route3");
        Stage4 = GameObject.Find("Route4");
        Stage5 = GameObject.Find("Route5");

        if (StageCode == 0)
        {
            Stage2.SetActive(false);
            Stage3.SetActive(false);
            Stage4.SetActive(false);
            Stage5.SetActive(false);
        }
        else if (StageCode == 1)
        {
            Stage1.SetActive(false);
            Stage3.SetActive(false);
            Stage4.SetActive(false);
            Stage5.SetActive(false);
        }
        else if (StageCode == 2)
        {
            Stage1.SetActive(false);
            Stage2.SetActive(false);
            Stage4.SetActive(false);
            Stage5.SetActive(false);
        }
        else if (StageCode == 3)
        {
            Stage1.SetActive(false);
            Stage2.SetActive(false);
            Stage3.SetActive(false);
            Stage5.SetActive(false);
        }
        else if (StageCode == 4)
        {
            Stage1.SetActive(false);
            Stage2.SetActive(false);
            Stage3.SetActive(false);
            Stage4.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
