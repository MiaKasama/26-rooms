using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZigZagMove : MonoBehaviour
{
    //動くことが出来るか
    bool CanMove = false;

    //どの方向に動くか
    bool FirstMoveVectorChange = false;
    bool MoveVectorChange = true;
    public static int MoveVectorZigZag;

    //方向チェンジ時の音
    AudioSource ArrowSound;

    //ぶつかった時の音
    AudioSource AttackSound;

    //直前のムーバーの座標
    float MoverX, MoverY, MoverZ;

    //成功か
    public static bool SuccessZigZag = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessZigZag = false;

        ArrowSound = GetComponent<AudioSource>();
        AttackSound = GameObject.Find("Floor").GetComponent<AudioSource>();

        MoverX = this.transform.position.x;
        MoverY = this.transform.position.y;
        MoverZ = this.transform.position.z;

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (MoveVectorChange == true && FirstMoveVectorChange == true)
        {
            MoveVectorChange = false;
            StartCoroutine(VectorCoroutine());
        }

        //Enterを押した時、指定された方向に動く
        if (Input.GetKeyDown(KeyCode.Space) && CanMove == true)
        {
            if (MoveVectorZigZag == 0)
                this.transform.position = new Vector3(MoverX, MoverY + 1.0f, MoverZ);
            else if (MoveVectorZigZag == 1)
                this.transform.position = new Vector3(MoverX + 1.0f, MoverY, MoverZ);
            else if (MoveVectorZigZag == 2)
                this.transform.position = new Vector3(MoverX, MoverY - 1.0f, MoverZ);
            else if (MoveVectorZigZag == 3)
                this.transform.position = new Vector3(MoverX - 1.0f, MoverY, MoverZ);

            CanMove = false;

            StartCoroutine(Update_frames());
        }

        if (SuccessZigZag == true)
        {
            CanMove = false;
            FirstMoveVectorChange = false;
        }
    }

    //3秒後に操作可能に
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        CanMove = true;
        FirstMoveVectorChange = true;
    }

    //Enterは一度押したら少しの間だけ押さない
    private IEnumerator Update_frames()
    {
        yield return new WaitForSeconds(0.1f);

        CanMove = true;

        MoverX = this.transform.position.x;
        MoverY = this.transform.position.y;
        MoverZ = this.transform.position.z;
    }

    //0.5秒毎に移動方向を変える
    private IEnumerator VectorCoroutine()
    {
        yield return new WaitForSeconds(0.5f);

        MoveVectorZigZag++;

        if (MoveVectorZigZag >= 4)
            MoveVectorZigZag = 0;

        ArrowSound.Play();

        MoveVectorChange = true;
    }

    //壁の方向に進んだ場合は元に戻す
    void OnTriggerEnter(Collider other)
    {
        //ゴールのコリジョンのみそのまま進み、成功処理を加える
        if (other.gameObject.tag != "ZigZagGoal")
        {
            this.transform.position = new Vector3(MoverX, MoverY, MoverZ);

            AttackSound.Play();
        }
        else
        {
            SuccessZigZag = true;
        }
    }

    public static int getMoveVectorZigZag()
    {
        return MoveVectorZigZag;
    }

    public static bool getSuccessZigZag()
    {
        return SuccessZigZag;
    }
}
