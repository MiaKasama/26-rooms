using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ZigZagVectorText : MonoBehaviour
{
    private TextMeshPro ArrowText;

    // Start is called before the first frame update
    void Start()
    {
        ArrowText = GetComponent<TextMeshPro>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ZigZagMove.getMoveVectorZigZag() == 0)
            ArrowText.text = "��";
        else if (ZigZagMove.getMoveVectorZigZag() == 1)
            ArrowText.text = "��";
        else if (ZigZagMove.getMoveVectorZigZag() == 2)
            ArrowText.text = "��";
        else if (ZigZagMove.getMoveVectorZigZag() == 3)
            ArrowText.text = "��";

        //�S�[���ɒH�蒅�������A�N�e�B�u
        if (ZigZagMove.getSuccessZigZag() == true)
            ArrowText.text = "";
    }
}
