using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HalfAnswerOpen : MonoBehaviour
{
    //経過時間
    float CurrentTime = 0.0f;

    [SerializeField]
    [Tooltip("コード(左は0、右は1)")]
    private int Code;

    //ボタンを押す音
    private AudioSource PushSound;

    //音は一回だけ
    bool SoundOn = true;

    //正解か否か
    public static bool SuccessHalf = false;
    public static bool FailureHalf = false;

    // Start is called before the first frame update
    void Start()
    {
        SoundOn = true;
        PushSound = GetComponent<AudioSource>();

        SuccessHalf = false;
        FailureHalf = false;
    }

    // Update is called once per frame
    void Update()
    {
        //箱の前面を移動する
        if (HalfButton.getOpenHalf() == Code && CurrentTime <= 1.0f)
        {
            CurrentTime += Time.deltaTime;

            transform.position -= 0.25f * transform.up;

            if(SoundOn == true)
            {
                SoundOn = false;
                PushSound.Play();
            }
        }

        if (CurrentTime >= 1.0f)
        {
            if (Code == HalfActive.getCorrectHalf())
                SuccessHalf = true;
            else
                FailureHalf = true;
        }
    }

    public static bool getSuccessHalf()
    {
        return SuccessHalf;
    }

    public static bool getFailureHalf()
    {
        return FailureHalf;
    }
}
