using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HalfLetter : MonoBehaviour
{
    TextMeshPro Letter;

    //文字の色
    Color LetterColorR = new Color(1.0f, 0.0f, 0.0f, 1.0f); //不正解の色
    Color LetterColorB = new Color(0.0f, 0.0f, 1.0f, 1.0f); //正解の色

    [SerializeField]
    [Tooltip("コード(左は0、右は1)")]
    private int Code;

    // Start is called before the first frame update
    void Start()
    {
        Letter = GetComponent<TextMeshPro>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (HalfAnswerOpen.getSuccessHalf() == true)
            Letter.color = LetterColorB;

        if (HalfAnswerOpen.getFailureHalf() == true)
            Letter.color = LetterColorR;
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        //当たりと外れ
        if (HalfActive.getStringHalf() == 0)
        {
            if (Code == HalfActive.getCorrectHalf())
            {
                Letter.text = "当ったり\n♪♪♪";
            }
            else
            {
                Letter.text = "はずれぇ\n………";
            }
        }
        //見事と残念
        if (HalfActive.getStringHalf() == 1)
        {
            if (Code == HalfActive.getCorrectHalf())
            {
                Letter.text = "わーお！\nお見事！";
            }
            else
            {
                Letter.text = "ああぁ…\n残…念…";
            }
        }
        //正解と不正解
        if (HalfActive.getStringHalf() == 2)
        {
            if (Code == HalfActive.getCorrectHalf())
            {
                Letter.text = "いえい！\n正解！";
            }
            else
            {
                Letter.text = "あう…\n不正解…";
            }
        }
        //信頼と不信
        if (HalfActive.getStringHalf() == 3)
        {
            if (Code == HalfActive.getCorrectHalf())
            {
                Letter.text = "信じて\nくれたね！";
            }
            else
            {
                Letter.text = "騙された\nようだ…";
            }
        }
        //デレデレとツンツン
        if (HalfActive.getStringHalf() == 4)
        {
            if (Code == HalfActive.getCorrectHalf())
            {
                Letter.text = "おめでと\n//////";
            }
            else
            {
                Letter.text = "つーん\n…………";
            }
        }
    }
}
