using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HalfButton : MonoBehaviour
{
    //解答オープン用
    public static int OpenHalf = -1;

    // Start is called before the first frame update
    void Start()
    {
        OpenHalf = -1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // 左ボタンが押された場合
    public void OnLeftClick()
    {
        OpenHalf = 0;
    }

    // 右ボタンが押された場合
    public void OnRightClick()
    {
        OpenHalf = 1;
    }

    public static int getOpenHalf()
    {
        return OpenHalf;
    }
}
