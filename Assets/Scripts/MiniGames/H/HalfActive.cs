using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HalfActive : MonoBehaviour
{
    //ボタンのアクティブ用
    GameObject TwoButtons;

    //正解はどちらか
    public static int CorrectHalf = -1;

    //どの文字を表示するか
    public static int StringHalf;

    // Start is called before the first frame update
    void Start()
    {
        TwoButtons = GameObject.Find("Buttons");

        //ボタンを非表示
        TwoButtons.SetActive(false);

        StartCoroutine(Start_frames());

        //ランダムに正解と表示文字を決定
        CorrectHalf = Random.Range(0, 2);
        StringHalf = Random.Range(0, 5);
    }

    // Update is called once per frame
    void Update()
    {
        //ボタンを押したらまた非表示にする
        if (HalfButton.getOpenHalf() != -1)
            TwoButtons.SetActive(false);
    }

    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        //3秒後にアクティブ
        TwoButtons.SetActive(true);
    }

    public static int getCorrectHalf()
    {
        return CorrectHalf;
    }

    public static int getStringHalf()
    {
        return StringHalf;
    }
}
