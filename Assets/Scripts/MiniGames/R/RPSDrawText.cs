using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RPSDrawText : MonoBehaviour
{
    private TextMeshProUGUI DrawText;

    //引き分け音
    private AudioSource DrawSound;
    bool CanSound = true;

    // Start is called before the first frame update
    void Start()
    {
        DrawSound = GetComponent<AudioSource>();

        DrawText = GetComponent<TextMeshProUGUI>();
        DrawText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (RPSTheirHand.getRevengeRPS() == true)
        {
            DrawText.text = "Draw";

            if(CanSound == true)
            {
                CanSound = false;
                DrawSound.Play();
            }
        }
        else
        {
            CanSound = true;

            DrawText.text = "";
        }
    }
}
