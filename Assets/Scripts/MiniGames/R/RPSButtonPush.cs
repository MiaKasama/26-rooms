using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPSButtonPush : MonoBehaviour
{
    //どれを出すか
    public static int YourHandRPS = -1;

    //あいこの時に引き返す用
    bool TurnHand = false;

    //ボタンを押して1.5秒は引き返さない
    public static bool CanTurnRPS = true;

    //じゃんけんオブジェクト
    GameObject YourRock;
    GameObject YourPaper;
    GameObject YourScissors;

    //ボタンの表示
    GameObject RPSButton;

    [SerializeField]
    [Tooltip("止めるy座標")]
    private float YLim = -0.5f;

    [SerializeField]
    [Tooltip("オブジェクトの移動速度")]
    private float speed = 0.2f;

    //y座標の初期値
    float StartY;

    //現在のy座標
    float y;

    // Start is called before the first frame update
    void Start()
    {
        YourHandRPS = -1;

        CanTurnRPS = true;

        YourRock = GameObject.Find("YourRock");
        YourPaper = GameObject.Find("YourPaper");
        YourScissors = GameObject.Find("YourScissors");

        RPSButton = GameObject.Find("Buttons");
        RPSButton.SetActive(false);

        StartY = YourRock.transform.position.y;

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (TurnHand == false)
        {
            //出した手によって移動させるオブジェクトを変更
            if (YourHandRPS == 0)
            {
                y = YourRock.transform.position.y;

                if (y < YLim)
                {
                    YourRock.transform.position += speed * transform.up;
                }
            }

            if (YourHandRPS == 1)
            {
                y = YourPaper.transform.position.y;

                if (y < YLim)
                {
                    YourPaper.transform.position += speed * transform.up;
                }
            }

            if (YourHandRPS == 2)
            {
                y = YourScissors.transform.position.y;

                if (y < YLim)
                {
                    YourScissors.transform.position += speed * transform.up;
                }
            }
        }
        else
        {
            //オブジェクトを元に戻す
            if (YourHandRPS == 0)
            {
                y = YourRock.transform.position.y;

                if (y > StartY)
                {
                    YourRock.transform.position -= speed * transform.up;
                }
            }

            if (YourHandRPS == 1)
            {
                y = YourPaper.transform.position.y;

                if (y > StartY)
                {
                    YourPaper.transform.position -= speed * transform.up;
                }
            }

            if (YourHandRPS == 2)
            {
                y = YourScissors.transform.position.y;

                if (y > StartY)
                {
                    YourScissors.transform.position -= speed * transform.up;
                }
            }

            if (y <= StartY)
            {
                RPSButton.SetActive(true);
                YourHandRPS = -1;
            }
        }
        
        if (RPSTheirHand.getRevengeRPS() == true)
            StartCoroutine(TurnCoroutine());
    }

    //3秒後にボタン表示
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        //ボタン表示
        RPSButton.SetActive(true);
    }

    //あいこで元に戻す
    private IEnumerator TurnCoroutine()
    {
        yield return new WaitForSeconds(1.5f);

        if (RPSTheirHand.getRevengeRPS() == true)
            TurnHand = true;

        if (CanTurnRPS == false)
            TurnHand = false;
    }

    // グーが押された場合
    public void OnRockClick()
    {
        YourHandRPS = 0;
        TurnHand = false;
        RPSButton.SetActive(false);

        StartCoroutine(CanTurnCoroutine());
    }

    // パーが押された場合
    public void OnPaperClick()
    {
        YourHandRPS = 1;
        TurnHand = false;
        RPSButton.SetActive(false);

        StartCoroutine(CanTurnCoroutine());
    }

    // チョキが押された場合
    public void OnScissorsClick()
    {
        YourHandRPS = 2;
        TurnHand = false;
        RPSButton.SetActive(false);

        StartCoroutine(CanTurnCoroutine());
    }

    private IEnumerator CanTurnCoroutine()
    {
        CanTurnRPS = false;

        yield return new WaitForSeconds(1.5f);

        CanTurnRPS = true;
    }

    public static int getYourHandRPS()
    {
        return YourHandRPS;
    }

    public static bool getCanTurnRPS()
    {
        return CanTurnRPS;
    }
}
