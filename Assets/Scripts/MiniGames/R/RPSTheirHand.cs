using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPSTheirHand : MonoBehaviour
{
    //手を出すか
    bool CanHand = true;

    //どれを出すか
    int TheirHandRPS = -1;

    //あいこの時に引き返す用
    bool TurnHand = false;

    //判定用
    //bool Judgement = true;

    //じゃんけんオブジェクト
    GameObject TheirRock;
    GameObject TheirPaper;
    GameObject TheirScissors;

    [SerializeField]
    [Tooltip("止めるy座標")]
    private float YLim = 2.5f;

    [SerializeField]
    [Tooltip("オブジェクトの移動速度")]
    private float speed = 0.2f;

    //y座標の初期値
    float StartY;

    //現在のy座標
    float y;

    //勝ちか負けか
    public static bool SuccessRPS = false;
    public static bool FailureRPS = false;

    //あいこか
    public static bool RevengeRPS = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessRPS = false;
        FailureRPS = false;
        RevengeRPS = false;

        TheirRock = GameObject.Find("TheirRock");
        TheirPaper = GameObject.Find("TheirPaper");
        TheirScissors = GameObject.Find("TheirScissors");

        StartY = TheirRock.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        //ボタンを押したら、こっちも手を出す
        if (TurnHand == false)
        {
            if (RPSButtonPush.getYourHandRPS() >= 0)
            {
                if (CanHand == true)
                {
                    CanHand = false;
                    TheirHandRPS = Random.Range(0, 3);
                    //TheirHandRPS = 0;
                }

                //出した手によって移動させるオブジェクトを変更
                if (TheirHandRPS == 0)
                {
                    y = TheirRock.transform.position.y;

                    if (y > YLim)
                    {
                        TheirRock.transform.position -= speed * transform.up;
                    }
                }

                if (TheirHandRPS == 1)
                {
                    y = TheirPaper.transform.position.y;

                    if (y > YLim)
                    {
                        TheirPaper.transform.position -= speed * transform.up;
                    }
                }

                if (TheirHandRPS == 2)
                {
                    y = TheirScissors.transform.position.y;

                    if (y > YLim)
                    {
                        TheirScissors.transform.position -= speed * transform.up;
                    }
                }

                if (y <= YLim)
                {
                    //グーが0、パーが1、チョキが2
                    if (RPSButtonPush.getYourHandRPS() == 0)
                    {
                        if (TheirHandRPS == 2)
                            SuccessRPS = true;
                        else if (TheirHandRPS == 1)
                            FailureRPS = true;
                        else if (TheirHandRPS == 0)
                            RevengeRPS = true;
                    }
                    else if (RPSButtonPush.getYourHandRPS() == 1)
                    {
                        if (TheirHandRPS == 0)
                            SuccessRPS = true;
                        else if (TheirHandRPS == 2)
                            FailureRPS = true;
                        else if (TheirHandRPS == 1)
                            RevengeRPS = true;
                    }
                    else if (RPSButtonPush.getYourHandRPS() == 2)
                    {
                        if (TheirHandRPS == 1)
                            SuccessRPS = true;
                        else if (TheirHandRPS == 0)
                            FailureRPS = true;
                        else if (TheirHandRPS == 2)
                            RevengeRPS = true;
                    }
                }
            }
        }
        else
        {
            //オブジェクトを元に戻す
            if (TheirHandRPS == 0)
            {
                y = TheirRock.transform.position.y;

                if (y < StartY)
                {
                    TheirRock.transform.position += speed * transform.up;
                }
            }

            if (TheirHandRPS == 1)
            {
                y = TheirPaper.transform.position.y;

                if (y < StartY)
                {
                    TheirPaper.transform.position += speed * transform.up;
                }
            }

            if (TheirHandRPS == 2)
            {
                y = TheirScissors.transform.position.y;

                if (y < StartY)
                {
                    TheirScissors.transform.position += speed * transform.up;
                }
            }

            if (y >= StartY)
            {
                RevengeRPS = false;

                CanHand = true;
                TurnHand = false;
                TheirHandRPS = -1;
            }
        }

        if (RevengeRPS == true)
            StartCoroutine(TurnCoroutine());
    }

    //あいこで元に戻す
    private IEnumerator TurnCoroutine()
    {
        yield return new WaitForSeconds(1.5f);

        if (RevengeRPS == true)
            TurnHand = true;

        if (RPSButtonPush.getCanTurnRPS() == false)
            TurnHand = false;
    }

    public static bool getSuccessRPS()
    {
        return SuccessRPS;
    }

    public static bool getFailureRPS()
    {
        return FailureRPS;
    }

    public static bool getRevengeRPS()
    {
        return RevengeRPS;
    }
}
