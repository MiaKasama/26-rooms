using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RPSComment : MonoBehaviour
{
    private TextMeshProUGUI Comment;

    bool CanComment = false;

    //どのコメントを表示するか
    int CommentChoice;
    int BeforeChoice;

    bool CommentChange = true;

    [SerializeField]
    [Tooltip("コメントの内容")]
    private string[] CommentArray = new string[4];

    // Start is called before the first frame update
    void Start()
    {
        Comment = GetComponent<TextMeshProUGUI>();
        Comment.text = "";

        StartCoroutine(Start_frames());

        CommentChoice = Random.Range(0, CommentArray.Length);
        BeforeChoice = CommentChoice;
    }

    // Update is called once per frame
    void Update()
    {
        if (CanComment == true)
        {
            if (RPSButtonPush.getYourHandRPS() < 0)
            {
                CommentChange = true;

                Comment.text = CommentArray[CommentChoice];
            }
            else
            {
                Comment.text = "";

                if (CommentChange == true)
                {
                    while(true)
                    {
                        CommentChange = false;

                        CommentChoice = Random.Range(0, CommentArray.Length);

                        if (CommentChoice != BeforeChoice)
                            break;
                    }
                }  
            }
        }
    }

    //3秒後にコメント表示
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        CanComment = true;
    }
}
