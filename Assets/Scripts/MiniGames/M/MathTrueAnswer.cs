using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MathTrueAnswer : MonoBehaviour
{
    private TextMeshPro MathText;

    //次の問題を出題する用
    int NextQ;

    //文字の色
    Color AfterColor = new Color(0.0f, 0.0f, 0.0f, 1.0f); //デフォルト
    Color AfterColorR = new Color(1.0f, 0.0f, 0.0f, 1.0f); //間違った場合
    Color AfterColorB = new Color(0.0f, 0.0f, 1.0f, 1.0f); //合っている場合

    // Start is called before the first frame update
    void Start()
    {
        NextQ = 2;

        MathText = GetComponent<TextMeshPro>();

        MathText.text = "???";
        MathText.color = AfterColor;
    }

    // Update is called once per frame
    void Update()
    {
        //答えを表示する
        if (MathInputField.getTrueAnswerMath() >= 0)
        {
            MathText.SetText(MathInputField.getTrueAnswerMath().ToString());

            if (MathInputField.getFailureMath() == true)
                MathText.color = AfterColorR;
            else
                MathText.color = AfterColorB;
        }

        //次の問題が出題されたら元に戻す
        if (NextQ == MathInputField.getNextQMath())
        {
            NextQ++;

            MathText.text = "???";
            MathText.color = AfterColor;
        }
    }
}
