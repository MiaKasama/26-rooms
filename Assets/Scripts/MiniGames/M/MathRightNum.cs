using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MathRightNum : MonoBehaviour
{
    public static int RightMath;

    private TextMeshPro MathText;

    //次の問題を出題する用
    int NextQ;

    // Start is called before the first frame update
    void Start()
    {
        NextQ = 2;

        RightMath = 0;

        MathText = GetComponent<TextMeshPro>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (NextQ == MathInputField.getNextQMath())
        {
            NextQ++;
            StartCoroutine(NextCoroutine());
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        //2〜左数未満でランダムに右数の値を得る
        RightMath = Random.Range(2, MathLeftNum.getLeftMath());

        MathText.SetText(RightMath.ToString());
    }

    public static int getRightMath()
    {
        return RightMath;
    }

    private IEnumerator NextCoroutine()
    {
        yield return null;

        RightMath = Random.Range(2, MathLeftNum.getLeftMath());

        MathText.SetText(RightMath.ToString());
    }
}
