using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MathLeftNum : MonoBehaviour
{
    public static int LeftMath;

    private TextMeshPro MathText;

    //次の問題を出題する用
    int NextQ;

    // Start is called before the first frame update
    void Start()
    {
        NextQ = 2;

        //3〜19でランダムに数値を得る
        LeftMath = Random.Range(3, 20);

        MathText = GetComponent<TextMeshPro>();
        MathText.SetText(LeftMath.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        if(NextQ == MathInputField.getNextQMath())
        {
            NextQ++;

            LeftMath = Random.Range(3, 20);
            MathText.SetText(LeftMath.ToString());
        }
    }

    public static int getLeftMath()
    {
        return LeftMath;
    }
}
