using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MathSignChange : MonoBehaviour
{
    public static int SignCodeMath;

    private TextMeshPro MathText;

    //次の問題を出題する用
    int NextQ;

    // Start is called before the first frame update
    void Start()
    {
        NextQ = 2;

        MathText = GetComponent<TextMeshPro>();

        //0は＋、1はー、2は×
        SignCodeMath = Random.Range(0, 3);

        if (SignCodeMath == 0)
            MathText.text = "＋";
        else if (SignCodeMath == 1)
            MathText.text = "ー";
        else if (SignCodeMath == 2)
            MathText.text = "×";
    }

    // Update is called once per frame
    void Update()
    {
        if (NextQ == MathInputField.getNextQMath())
        {
            NextQ++;

            SignCodeMath = Random.Range(0, 3);

            if (SignCodeMath == 0)
                MathText.text = "＋";
            else if (SignCodeMath == 1)
                MathText.text = "ー";
            else if (SignCodeMath == 2)
                MathText.text = "×";
        }
    }

    public static int getSignCodeMath()
    {
        return SignCodeMath;
    }
}
