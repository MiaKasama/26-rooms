using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MathInputField : MonoBehaviour
{
    TMP_InputField InputMath;
    GameObject Calculating;

    //解答
    string YourAnswer;

    public static int TrueAnswerMath = -1;

    //正解数
    public static int CorrectMath;

    //次は何問目か
    public static int NextQMath;

    //成功か否か
    public static bool SuccessMath = false;
    public static bool FailureMath = false;

    //途中の正解音
    private AudioSource MathSound;

    // Start is called before the first frame update
    void Start()
    {
        SuccessMath = false;
        FailureMath = false;

        CorrectMath = 0;
        NextQMath = 1;

        TrueAnswerMath = -1;

        Calculating = GameObject.Find("CalculateAnswer");
        InputMath = Calculating.GetComponent<TMP_InputField>();

        MathSound = GetComponent<AudioSource>();

        //InputFieldオブジェクトを非表示
        Calculating.SetActive(false);

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //入力を受け付けた時の処理
    public void GetInputMath()
    {
        YourAnswer = InputMath.text;

        if (MathSignChange.getSignCodeMath() == 0)
            TrueAnswerMath = MathLeftNum.getLeftMath() + MathRightNum.getRightMath();
        else if (MathSignChange.getSignCodeMath() == 1)
            TrueAnswerMath = MathLeftNum.getLeftMath() - MathRightNum.getRightMath();
        else if (MathSignChange.getSignCodeMath() == 2)
            TrueAnswerMath = MathLeftNum.getLeftMath() * MathRightNum.getRightMath();

        if (YourAnswer == TrueAnswerMath.ToString())
        {
            CorrectMath++;
            InputMath.text = "";

            if (CorrectMath == 5)
                SuccessMath = true;
            else
                StartCoroutine(NextCoroutine());
        }
        else
            FailureMath = true;

        //InputFieldオブジェクトを非表示
        Calculating.SetActive(false);
    }

    //3秒後に表示
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        Calculating.SetActive(true);
    }

    //0.7秒後に次の問題を出す
    private IEnumerator NextCoroutine()
    {
        MathSound.Play();

        yield return new WaitForSeconds(0.7f);

        NextQMath++;
        TrueAnswerMath = -1;

        Calculating.SetActive(true);
    }

    public static int getNextQMath()
    {
        return NextQMath;
    }

    public static int getTrueAnswerMath()
    {
        return TrueAnswerMath;
    }

    public static bool getSuccessMath()
    {
        return SuccessMath;
    }

    public static bool getFailureMath()
    {
        return FailureMath;
    }
}
