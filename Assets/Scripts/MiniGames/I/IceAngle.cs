using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceAngle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(IceCharacterMove.getCanMoveIce() == true && IceCharacterMove.getStartCharaIce() == true)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
                transform.rotation = Quaternion.Euler(0f, 0f, 00f);

            if (Input.GetKeyDown(KeyCode.DownArrow))
                transform.rotation = Quaternion.Euler(0f, 0f, 180f);

            if (Input.GetKeyDown(KeyCode.RightArrow))
                transform.rotation = Quaternion.Euler(0f, 0f, 270f);

            if (Input.GetKeyDown(KeyCode.LeftArrow))
                transform.rotation = Quaternion.Euler(0f, 0f, 90f);
        }
    }
}
