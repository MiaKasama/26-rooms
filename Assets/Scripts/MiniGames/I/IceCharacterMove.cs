using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceCharacterMove : MonoBehaviour
{
    //キャラを動かし始めるかどうか
    public static bool StartCharaIce = false;
    public static bool CanMoveIce = false;

    [SerializeField]
    [Tooltip("キャラクターの移動速度")]
    float speed = 0.1f;

    bool MoveUp = false;
    bool MoveDown = false;
    bool MoveRight = false;
    bool MoveLeft = false;

    //現在の移動速度
    float NowSpeedX, NowSpeedY;

    //直前の座標
    float LatestX, LatestY, LatestZ;

    [SerializeField]
    [Tooltip("座標の左限界")]
    float XLim = -7.5f;

    [SerializeField]
    [Tooltip("座標の下限界")]
    float YLim = -5.5f;

    [SerializeField]
    [Tooltip("ゴール座標")]
    float XGoal = 7.0f;

    //止まる音
    private AudioSource StopSound;

    //ゴールに辿り着いたか
    public static bool SuccessIce = false;

    //while文用変数
    int i, j;

    // Start is called before the first frame update
    void Start()
    {
        StartCharaIce = false;
        CanMoveIce = false;

        SuccessIce = false;

        LatestX = this.transform.position.x;
        LatestY = this.transform.position.y;
        LatestZ = this.transform.position.z;

        StopSound = GameObject.Find("StopSound").GetComponent<AudioSource>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //移動速度が0なら操作可能にする
        NowSpeedX = (this.transform.position.x - LatestX) / Time.deltaTime;
        NowSpeedY = (this.transform.position.y - LatestY) / Time.deltaTime;

        LatestX = this.transform.position.x;
        LatestY = this.transform.position.y;

        if (NowSpeedX == 0 && NowSpeedY == 0)
        {
            CanMoveIce = true;

            MoveUp = false;
            MoveDown = false;
            MoveRight = false;
            MoveLeft = false;
        }
        else
            CanMoveIce = false;

        if (CanMoveIce == true && StartCharaIce == true)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
                MoveUp = true;

            if (Input.GetKeyDown(KeyCode.DownArrow))
                MoveDown = true;

            if (Input.GetKeyDown(KeyCode.RightArrow))
                MoveRight = true;

            if (Input.GetKeyDown(KeyCode.LeftArrow))
                MoveLeft = true;
        }

        if (MoveUp == true)
            this.transform.Translate(0.0f, speed, 0.0f);

        if (MoveDown == true)
            this.transform.Translate(0.0f, -speed, 0.0f);

        if (MoveRight == true)
            this.transform.Translate(speed, 0.0f, 0.0f);

        if (MoveLeft == true)
            this.transform.Translate(-speed, 0.0f, 0.0f);

        //ゴールに着いたら操作不能に
        if (XGoal <= LatestX && LatestX < (XGoal + 1))
        {
            StartCharaIce = false;
            SuccessIce = true;
        }
    }

    //3秒後にスタート
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartCharaIce = true;
    }

    //壁に当たったら動きを停止し、位置調整をする
    private void OnCollisionEnter(Collision collision)
    {
        this.transform.Translate(0.0f, 0.0f, 0.0f);

        MoveUp = false;
        MoveDown = false;
        MoveRight = false;
        MoveLeft = false;

        StopSound.Play();

        i = 0;
        j = 0;

        while(true)
        {
            if (Mathf.Abs(LatestX - (XLim + i)) < Mathf.Abs(LatestX - (XLim + i + 1)))
            {
                LatestX = XLim + i;

                break;
            }
            else
                i++;
        }

        while (true)
        {
            if (Mathf.Abs(LatestY - (YLim + j)) < Mathf.Abs(LatestY - (YLim + j + 1)))
            {
                LatestY = YLim + j;

                break;
            }
            else
                j++;
        }

        transform.position = new Vector3(LatestX, LatestY, LatestZ);

        //Debug.Log("当たった");
    }

    public static bool getStartCharaIce()
    {
        return StartCharaIce;
    }

    public static bool getCanMoveIce()
    {
        return CanMoveIce;
    }

    public static bool getSuccessIce()
    {
        return SuccessIce;
    }
}
