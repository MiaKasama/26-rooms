using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VegetableTag : MonoBehaviour
{
    [SerializeField]
    [Tooltip("GameObjectのコード")]
    private int Code;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //このオブジェクトのコードと、次獲得する野菜のコードが一致したら専用のタグを付ける
        if (Code == VegetableBasketMove.getNextCodeVegetable())
            this.tag = "VegetableTrue";
        else
            this.tag = "VegetableFalse";
    }
}
