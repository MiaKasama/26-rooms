using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VegetableFall : MonoBehaviour
{
    //野菜の落下速度
    float speed = 0.04f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(0.0f, -speed, 0.0f);

        //一定の位置まで落下したら自然消滅
        if (this.transform.position.y <= -11)
            Destroy(gameObject);
    }
}
