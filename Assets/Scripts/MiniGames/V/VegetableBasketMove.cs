using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VegetableBasketMove : MonoBehaviour
{
    //籠を動かし始めるかどうか
    bool StartBasket = false;

    [SerializeField]
    [Tooltip("籠の移動速度")]
    float speed = 0.05f;

    [SerializeField]
    [Tooltip("移動できる上の限界")]
    float UpLim = 11.0f;

    [SerializeField]
    [Tooltip("移動できる下の限界")]
    float DownLim = -9.0f;

    [SerializeField]
    [Tooltip("移動できる左の限界")]
    float LeftLim = -19.0f;

    [SerializeField]
    [Tooltip("移動できる右の限界")]
    float RightLim = 7.5f;

    //手に入れた野菜の数
    int CatchVegetable = 0;

    //クリアまでの野菜の数
    int SuccessCatchVegetable;

    //次に手に入れるべき野菜
    public static int NextCodeVegetable;

    //現在の座標
    float x, y;

    //獲得音
    private AudioSource CatchSound;

    //成功か否か
    public static bool SuccessVegetable = false;
    public static bool FailureVegetable = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessVegetable = false;
        FailureVegetable = false;

        CatchVegetable = 0;
        SuccessCatchVegetable = Random.Range(15, 31);

        NextCodeVegetable = Random.Range(0, 4);

        CatchSound = GetComponent<AudioSource>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (StartBasket == true)
        {
            //座標を取得
            x = this.transform.position.x;
            y = this.transform.position.y;

            //上に移動
            if (Input.GetKey(KeyCode.UpArrow) && y <= UpLim)
                this.transform.Translate(0.0f, speed, 0.0f);

            //下に移動
            if (Input.GetKey(KeyCode.DownArrow) && y >= DownLim)
                this.transform.Translate(0.0f, -speed, 0.0f);

            //左に移動
            if (Input.GetKey(KeyCode.LeftArrow) && x >= LeftLim)
                this.transform.Translate(-speed, 0.0f, 0.0f);

            //右に移動
            if (Input.GetKey(KeyCode.RightArrow) && x <= RightLim)
                this.transform.Translate(speed, 0.0f, 0.0f);
        }

        if (SuccessVegetable == true || FailureVegetable == true)
            StartBasket = false;
    }

    // 3秒後にスタート
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartBasket = true;
    }

    // 当たった野菜の消滅
    void OnCollisionEnter(Collision collision)
    {
        //タグで、失敗判定を行う
        if (collision.gameObject.tag == "VegetableFalse" && SuccessVegetable != true)
            FailureVegetable = true;
        else if (collision.gameObject.tag == "VegetableTrue")
        {
            CatchVegetable++;

            if (CatchVegetable == SuccessCatchVegetable)
                SuccessVegetable = true;

            if (SuccessVegetable == false)
                CatchSound.Play();

            if (SuccessVegetable == false && FailureVegetable == false)
                NextCodeVegetable = Random.Range(0, 4);
        }

        Destroy(collision.gameObject);
    }

    public static int getNextCodeVegetable()
    {
        return NextCodeVegetable;
    }

    public static bool getSuccessVegetable()
    {
        return SuccessVegetable;
    }

    public static bool getFailureVegetable()
    {
        return FailureVegetable;
    }
}
