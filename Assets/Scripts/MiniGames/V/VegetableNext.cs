using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VegetableNext : MonoBehaviour
{
    private GameObject[] Vegetables = new GameObject[4];

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //野菜の情報を取得
        Vegetables[0] = GameObject.Find("NextCucumber");
        Vegetables[1] = GameObject.Find("NextGarlic");
        Vegetables[2] = GameObject.Find("NextOnion");
        Vegetables[3] = GameObject.Find("NextZucchini");
    }

    // Update is called once per frame
    void Update()
    {
        //次に手に入れるべき野菜のみ表示する
        for (i = 0; i < Vegetables.Length; i++)
        {
            if (i == VegetableBasketMove.getNextCodeVegetable())
                Vegetables[i].SetActive(true);
            else
                Vegetables[i].SetActive(false);
        }
    }
}
