using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VegetableSpawn : MonoBehaviour
{
    //スポーンするか
    bool CanSpawn = false;
    int SpawnCode = 0;

    [SerializeField]
    [Tooltip("生成するGameObject")]
    private GameObject[] createPrefab = new GameObject[4];

    //スポーンする座標と向き
    float x, y, z;
    int angleX, angleY, angleZ;

    // Start is called before the first frame update
    void Start()
    {
        y = 12.0f;
        z = 10.0f;

        angleX = 0;
        angleY = 0;
        angleZ = 0;

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (CanSpawn == true && VegetableBasketMove.getSuccessVegetable() == false && VegetableBasketMove.getFailureVegetable() == false)
        {
            CanSpawn = false;

            //ランダムにスポーンさせるオブジェクトを変更
            SpawnCode = Random.Range(0, createPrefab.Length);
            x = Random.Range(-19.0f, 7.5f);

            //定義した位置にスポーン
            GameObject Vegetables = Instantiate(createPrefab[SpawnCode], new Vector3(x, y, z), Quaternion.Euler(angleX, angleY, angleZ));

            StartCoroutine(SpawnCoroutine());
        }
    }

    //3秒後にスポーン開始
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        CanSpawn = true;
    }

    //初回以降は1.0秒ごとにスポーン
    private IEnumerator SpawnCoroutine()
    {
        yield return new WaitForSeconds(1.0f);

        CanSpawn = true;
    }
}
