using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class YieldProgram : MonoBehaviour
{
    //Enterを押したか
    bool PressEnter = false;

    private TextMeshPro ReadyText;

    private GameObject GoPlate;

    [SerializeField]
    [Tooltip("GoPlateの移動速度")]
    float speed = 0.1f;

    //お手つき処理用
    bool BeginingJudge = false;

    //合格判定が開始されるタイミングと判定
    float SuccessStart;
    bool SuccessJudge = false;

    //フェイント処理
    int Feint;

    //Goまたはフェイントでのサウンド
    private AudioSource GoSound;
    private AudioSource FeintSound;

    //合格判定が継続される時間
    int SuccessFrame;

    //ホワイトアウト処理用
    private GameObject WhiteOutPlane;

    //ホワイトアウトサウンド
    private AudioSource WhiteSound;

    //お手付き用テキスト
    private TextMeshProUGUI BeginingText;

    //自分の敵のオブジェクトと試合終了後の座標
    private GameObject Character;
    float CharX, CharY, CharZ;
    int CharAngleX, CharAngleY, CharAngleZ;
    
    private GameObject YouText;
    float YouTextY;

    private GameObject Enemy;
    float EnemyX, EnemyY, EnemyZ;
    int EnemyAngleX, EnemyAngleY, EnemyAngleZ;

    //成功か否か
    public static bool SuccessYield = false;
    public static bool FailureYield = false;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        SuccessYield = false;
        FailureYield = false;

        WhiteOutPlane = GameObject.Find("WhiteOutPlane");
        WhiteOutPlane.SetActive(false);
        WhiteSound = GameObject.Find("WhiteSound").GetComponent<AudioSource>();

        ReadyText = GameObject.Find("ReadyText").GetComponent<TextMeshPro>();
        ReadyText.text = "Ready...";
        ReadyText.fontSize = 1.5f;

        GoSound = GameObject.Find("GoSound").GetComponent<AudioSource>();
        FeintSound = GameObject.Find("FeintSound").GetComponent<AudioSource>();

        //合格開始時間と、範囲はランダム
        SuccessStart = Random.Range(2.0f, 10.0f);
        SuccessFrame = Random.Range(15, 31);

        //1/5の確率でフェイントを入れる
        Feint = Random.Range(0, 4);

        BeginingText = GameObject.Find("BeginingText").GetComponent<TextMeshProUGUI>();
        BeginingText.text = "";

        //結果が決まった後の情報を取得しておく
        Character = GameObject.Find("Character");
        Enemy = GameObject.Find("Enemy");
        YouText = GameObject.Find("YouText");

        CharX = 2.5f;
        CharY = Character.transform.position.y;
        CharZ = Character.transform.position.z;

        CharAngleX = 0;
        CharAngleY = 0;
        CharAngleZ = Random.Range(135, 270);

        EnemyX = -2.5f;
        EnemyY = Enemy.transform.position.y;
        EnemyZ = Enemy.transform.position.z;

        EnemyAngleX = 0;
        EnemyAngleY = 180;
        EnemyAngleZ = Random.Range(135, 270);

        YouTextY = YouText.transform.position.y;

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //Enterを押せるのは一回だけ
        if (Input.GetKeyDown(KeyCode.Return) && PressEnter == true)
        {
            PressEnter = false;

            StartCoroutine(EnterCoroutine());
        }
    }

    private IEnumerator Start_frames()
    {
        //GoPlateを動かす
        yield return new WaitForSeconds(3.0f);

        GoPlate = GameObject.Find("GoPlate");

        //GoPlateが到着したらEnterを押せるようにし、暫くお手つき処理を行う
        while (true)
        {
            yield return null;

            GoPlate.transform.Translate(0.0f, -speed, 0.0f);

            if (GoPlate.transform.position.y <= 2.1f)
                break;
        }

        //Debug.Log(speed);

        PressEnter = true;
        BeginingJudge = true;

        //合格判定が開始されたらお手つき処理を停止する
        yield return new WaitForSeconds(SuccessStart);

        //否、フェイントが入る場合はさらに一秒追加
        if (Feint == 0 && PressEnter == true)
        {
            ReadyText.text = "Goと言ったら押してね♪";
            ReadyText.fontSize = 0.8f;
            FeintSound.Play();

            yield return new WaitForSeconds(1.0f);
        }

        if (PressEnter == true)
        {
            BeginingJudge = false;
            SuccessJudge = true;

            ReadyText.text = "Go!!";
            ReadyText.fontSize = 2.0f;
            GoSound.Play();
        }

        //お手つきも成功もしていなかったら以下の処理を行う
        for (i = 0; i < SuccessFrame; i++)
            yield return null;

        if (PressEnter == true)
        {
            SuccessJudge = false;
            PressEnter = false;

            StartCoroutine(EnterCoroutine());
        }
    }

    private IEnumerator EnterCoroutine()
    {
        //0.5秒間ホワイトアウトさせる
        GoPlate.SetActive(false);
        WhiteOutPlane.SetActive(true);
        WhiteSound.Play();

        yield return new WaitForSeconds(0.5f);

        WhiteOutPlane.SetActive(false);

        //状況によって処理を変更
        //お手つき用処理
        if (BeginingJudge == true)
        {
            //お手つき時はその場にとどまる
            CharX = Character.transform.position.x;
            EnemyX = Enemy.transform.position.x;
            EnemyAngleZ = 0;

            Character.transform.position = new Vector3(CharX, CharY, CharZ);
            Enemy.transform.position = new Vector3(EnemyX, EnemyY, EnemyZ);
            YouText.transform.position = new Vector3(CharX, YouTextY, CharZ);

            yield return new WaitForSeconds(0.5f);

            FailureYield = true;
            BeginingText.text = "お手つき...";

            Character.transform.eulerAngles = new Vector3(CharAngleX, CharAngleY, CharAngleZ);
            Enemy.transform.eulerAngles = new Vector3(EnemyAngleX, EnemyAngleY, EnemyAngleZ);
        }
        //合格処理
        else if (SuccessJudge == true)
        {
            CharAngleZ = 0;

            Character.transform.position = new Vector3(CharX, CharY, CharZ);
            Enemy.transform.position = new Vector3(EnemyX, EnemyY, EnemyZ);
            YouText.transform.position = new Vector3(CharX, YouTextY, CharZ);

            yield return new WaitForSeconds(0.5f);

            SuccessYield = true;
            
            Character.transform.eulerAngles = new Vector3(CharAngleX, CharAngleY, CharAngleZ);
            Enemy.transform.eulerAngles = new Vector3(EnemyAngleX, EnemyAngleY, EnemyAngleZ);
        }
        //出遅れ処理
        else
        {
            EnemyAngleZ = 0;

            Character.transform.position = new Vector3(CharX, CharY, CharZ);
            Enemy.transform.position = new Vector3(EnemyX, EnemyY, EnemyZ);
            YouText.transform.position = new Vector3(CharX, YouTextY, CharZ);

            yield return new WaitForSeconds(0.5f);

            FailureYield = true;
            
            Character.transform.eulerAngles = new Vector3(CharAngleX, CharAngleY, CharAngleZ);
            Enemy.transform.eulerAngles = new Vector3(EnemyAngleX, EnemyAngleY, EnemyAngleZ);
        }
    }

    public static bool getSuccessYield()
    {
        return SuccessYield;
    }

    public static bool getFailureYield()
    {
        return FailureYield;
    }
}
