using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeAngle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.rotation = Quaternion.Euler(KnifeThrow.getAngleXKnife(), KnifeThrow.getAngleYKnife(), 0.0f);
    }
}
