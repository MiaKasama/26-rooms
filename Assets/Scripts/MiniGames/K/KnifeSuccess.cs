using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeSuccess : MonoBehaviour
{
    //成功か
    public static bool SuccessKnife = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessKnife = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //ナイフが当たったら成功
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Knife"))
        {
            // 触れたobjの親を的にする
            other.transform.SetParent(transform);

            SuccessKnife = true;
        }

        //if (SuccessKnife == true)
            //Debug.Log("Success");
    }

    public static bool getSuccessKnife()
    {
        return SuccessKnife;
    }
}
