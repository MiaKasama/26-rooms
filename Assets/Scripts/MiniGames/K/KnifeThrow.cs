using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeThrow : MonoBehaviour
{
    //-1.5 ~ 3.5, -2.5 ~ 2.5
    //-4 ~ 6, -5 ~ 5
    //5

    //スポーン時点での座標
    bool FirstPos = false;
    float FirstX, FirstY;

    //最終的に到達するだろう座標
    float FinalX, FinalY;

    //現在のz座標
    float z;

    //移動速度
    float TransX, TransY, TransZ;

    [SerializeField]
    [Tooltip("移動速度調整（大きい方が遅くなる）")]
    float Adjust = 50.0f;

    //投げた時の角度
    public static float AngleXKnife;
    public static float AngleYKnife;

    //的に当たらなかった場合の処理
    public static bool ThroughKnife = false;

    //投げる音
    private AudioSource ThrowSound;
    bool SoundOn = true;

    // Start is called before the first frame update
    void Start()
    {
        ThrowSound = GetComponent<AudioSource>();

        AngleXKnife = 0.0f;
        AngleYKnife = 0.0f;

        ThroughKnife = false;
    }

    // Update is called once per frame
    void Update()
    {
        //一度だけ音を鳴らす
        if(SoundOn == true)
        {
            SoundOn = false;
            ThrowSound.Play();
        }

        if(FirstPos == false)
        {
            FirstPos = true;

            FirstX = this.transform.position.x;
            FirstY = this.transform.position.y;

            FinalX = 2 * FirstX;
            FinalY = 2 * (FirstY - 1) + 1;

            TransX = (FinalX - FirstX) / Adjust;
            TransY = (FinalY - FirstY) / Adjust;
            TransZ = 5.0f/ Adjust;

            //角度は正接の逆関数
            AngleXKnife = Mathf.Atan(-TransY / TransZ) * Mathf.Rad2Deg;
            AngleYKnife = Mathf.Atan(TransX / TransZ) * Mathf.Rad2Deg;
        }

        this.transform.Translate(TransX, TransY, TransZ);

        z = this.transform.position.z;

        if (z > 3)
        {
            ThroughKnife = true;

            StartCoroutine(DestroyCoroutine());
        }        

        /*
        if(z < 0)
            this.transform.Translate(TransX, TransY, TransZ);

        z = this.transform.position.z;
        */
    }

    private IEnumerator DestroyCoroutine()
    {
        yield return new WaitForSeconds(0.1f);

        ThroughKnife = false;
        Destroy(gameObject);
    }

    //オブジェクトに当たったら動きを止める
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.name);
        TransX = 0;
        TransY = 0;
        TransZ = 0;
    }

    public static bool getThroughKnife()
    {
        return ThroughKnife;
    }

    public static float getAngleXKnife()
    {
        return AngleXKnife;
    }

    public static float getAngleYKnife()
    {
        return AngleYKnife;
    }
}
