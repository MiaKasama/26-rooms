using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeFailure : MonoBehaviour
{
    //失敗か
    public static bool FailureKnife = false;

    // Start is called before the first frame update
    void Start()
    {
        FailureKnife = false;
    }

    // Update is called once per frame
    void Update()
    {
        //成功ゾーンにあるのなら失敗は取り消し
        if (KnifeSuccess.getSuccessKnife() == true)
            FailureKnife = false;

        //当たらなかった場合も失敗
        if (KnifeThrow.getThroughKnife() == true)
        {
            FailureKnife = true;
            Debug.Log("Failure");
        }
    }

    //ナイフが当たったら失敗
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Knife"))
        {
            // 触れたobjの親を的にする
            other.transform.SetParent(transform);

            FailureKnife = true;
        }

        //成功ゾーンにあるのなら失敗は取り消し
        if (KnifeSuccess.getSuccessKnife() == true)
            FailureKnife = false;

        //if (FailureKnife == true)
            //Debug.Log("Failure");
    }

    public static bool getFailureKnife()
    {
        return FailureKnife;
    }
}
