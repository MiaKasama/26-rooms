using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeSpawn : MonoBehaviour
{
    [SerializeField]
    [Tooltip("生成するGameObject")]
    private GameObject createPrefab;

    //照準を動かし始めるかどうか
    bool StartAim = false;

    //操作中か
    bool Moving = false;

    //どの方向に動かすかの乱数
    int AutoVector;

    [SerializeField]
    [Tooltip("照準の移動速度")]
    float speed = 0.02f;

    [SerializeField]
    [Tooltip("無操作時の移動速度")]
    float speedAuto = 0.005f;

    [SerializeField]
    [Tooltip("移動できる上下の限界")]
    float sideUp = 3.0f;

    [SerializeField]
    [Tooltip("移動できる左右の限界")]
    float sideRight = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if(StartAim == true)
        {
            //台の座標を取得
            Vector3 pos = this.transform.position;

            Moving = false;

            // 下に移動
            if ((1 - sideUp) <= pos.y)
            {
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    this.transform.Translate(0.0f, -speed, 0.0f);

                    Moving = true;
                }  
            }

            // 上に移動
            if (pos.y <= (1 + sideUp))
            {
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    this.transform.Translate(0.0f, speed, 0.0f);

                    Moving = true;
                }
            }

            // 左に移動
            if (-sideRight <= pos.x)
            {
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    this.transform.Translate(-speed, 0.0f, 0.0f);

                    Moving = true;
                }   
            }

            // 右に移動
            if (pos.x <= sideRight)
            {
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    this.transform.Translate(speed, 0.0f, 0.0f);

                    Moving = true;
                }
            }

            //値によって方向を決める
            if(Moving == false)
            {
                AutoVector = Random.Range(0, 8);

                if(AutoVector == 0)
                    this.transform.Translate(0.0f, speedAuto, 0.0f);
                else if(AutoVector == 1)
                    this.transform.Translate(speedAuto, speedAuto, 0.0f);
                else if (AutoVector == 2)
                    this.transform.Translate(speedAuto, 0.0f, 0.0f);
                else if (AutoVector == 3)
                    this.transform.Translate(speedAuto, -speedAuto, 0.0f);
                else if (AutoVector == 4)
                    this.transform.Translate(0.0f, -speedAuto, 0.0f);
                else if (AutoVector == 5)
                    this.transform.Translate(-speedAuto, -speedAuto, 0.0f);
                else if (AutoVector == 6)
                    this.transform.Translate(-speedAuto, 0.0f, 0.0f);
                else if (AutoVector == 7)
                    this.transform.Translate(-speedAuto, speedAuto, 0.0f);
            }


            if (Input.GetKeyDown(KeyCode.Return))
            {
                pos = this.transform.position;

                GameObject Knife = Instantiate(createPrefab, new Vector3(pos.x, pos.y, pos.z), Quaternion.Euler(0.0f, 0.0f, 0.0f));

                this.gameObject.SetActive(false);
            }    
        }
    }

    //3秒後にスタート
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartAim = true;
    }
}
