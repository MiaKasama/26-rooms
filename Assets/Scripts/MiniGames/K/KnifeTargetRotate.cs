using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeTargetRotate : MonoBehaviour
{
    //��]���邩
    bool CanRotate = true;

    //��]���x
    float RotateSpeed;

    //�m���ŉ�]���x��ύX
    int n;

    //����
    float Brake = 0.05f;

    bool StartBrake = true;

    // Start is called before the first frame update
    void Start()
    {
        n = Random.Range(0, 10);

        if (n == 0)
            RotateSpeed = 0.0f;
        else if (n == 1 || n == 2)
            RotateSpeed = 2.0f;
        else if (n == 3 || n == 4 || n == 5)
            RotateSpeed = 4.0f;
        else if (n == 6 || n == 7 || n == 8)
            RotateSpeed = -4.0f;
        else
            RotateSpeed = 8.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (CanRotate == true)
            this.transform.Rotate(0.0f, RotateSpeed, 0.0f);

        //�����E���s�����ɏ��X�ɉ�]��x������
        if(KnifeSuccess.getSuccessKnife() == true || KnifeFailure.getFailureKnife() == true && StartBrake == true)
        {
            StartBrake = false;
            StartCoroutine(AccelCoroutine());
        }
    }

    private IEnumerator AccelCoroutine()
    {
        yield return new WaitForSeconds(0.1f);

        if (RotateSpeed > 0)
            RotateSpeed -= Brake;

        if (RotateSpeed < 0)
            RotateSpeed += Brake;

        if (0 < RotateSpeed && RotateSpeed <= Brake)
            RotateSpeed = 0.0f;

        if (0 > RotateSpeed && RotateSpeed >= -Brake)
            RotateSpeed = 0.0f;

        //Debug.Log(RotateSpeed);

        StartBrake = true;
    }
}
