using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AppleHowMany : MonoBehaviour
{
    private TextMeshPro HowManyApple;

    [SerializeField]
    [Tooltip("正しい答え")]
    private bool TrueAnswer = true;

    // Start is called before the first frame update
    void Start()
    {
        HowManyApple = GetComponent<TextMeshPro>();
        HowManyApple.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (AppleInputFieldManager.getOpenApple() == true)
            StartCoroutine(Answer());
    }

    private IEnumerator Answer()
    {
        yield return new WaitForSeconds(1.0f);

        if(TrueAnswer == true)
            HowManyApple.text = AppleSpawn.getStringAppleN();
        else
            HowManyApple.text = AppleInputFieldManager.getAnswerApple();
    }
}
