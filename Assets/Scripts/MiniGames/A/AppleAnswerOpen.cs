using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppleAnswerOpen : MonoBehaviour
{
    //経過時間
    float CurrentTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //箱の前面を移動する
        if(AppleInputFieldManager.getOpenApple() == true && CurrentTime <= 1.0f)
        {
            CurrentTime += Time.deltaTime;

            transform.position -= 0.1f * transform.up;
        }
    }
}
