using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppleSound : MonoBehaviour
{
    //衝突音
    private AudioSource Sound;

    bool SoundOn = true;

    //フェイクで追加で音を入れる用
    int FakeSound;
    float FakeTime;

    // Start is called before the first frame update
    void Start()
    {
        Sound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //一定地点まで落下したら一度だけ音を鳴らす
        if (this.transform.position.y <= 0 && SoundOn == true)
        {
            SoundOn = false;
            Sound.Play();

            FakeSound = Random.Range(0, 2);
            FakeTime = Random.Range(0.1f, 0.3f);

            //1/2の確率で二度音を鳴らす
            if(FakeSound == 0)
                StartCoroutine(FakeCoroutine());
        }
    }

    private IEnumerator FakeCoroutine()
    {
        yield return new WaitForSeconds(FakeTime);

        Sound.Play();
    }
}
