using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AppleInputFieldManager : MonoBehaviour
{
    TMP_InputField InputApple;
    GameObject HowManyApple;

    public static string AnswerApple;

    public static bool OpenApple = false;

    //アクティブにするか
    bool CanActive = true;

    //正解か否か
    public static bool SuccessApple = false;
    public static bool FailureApple = false;

    // Start is called before the first frame update
    void Start()
    {
        OpenApple = false;

        SuccessApple = false;
        FailureApple = false;

        HowManyApple = GameObject.Find("HowManyApple");
        InputApple = HowManyApple.GetComponent<TMP_InputField>();

        //InputFieldオブジェクトを非表示
        HowManyApple.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //全リンゴスポーン後に表示
        if (AppleSpawn.getInputSwitchApple() == true && CanActive == true)
        {
            HowManyApple.SetActive(true);
            CanActive = false;
        }
    }

    public void GetInputApple()
    {
        OpenApple = true;

        AnswerApple = InputApple.text;

        if (AnswerApple == AppleSpawn.getStringAppleN())
            StartCoroutine(Success());
        else
            StartCoroutine(Failure());

        //InputFieldオブジェクトを非表示
        HowManyApple.SetActive(false);
    }

    private IEnumerator Success()
    {
        yield return new WaitForSeconds(1.0f);

        SuccessApple = true;
    }

    private IEnumerator Failure()
    {
        yield return new WaitForSeconds(1.0f);

        FailureApple = true;
    }

    public static string getAnswerApple()
    {
        return AnswerApple;
    }

    public static bool getOpenApple()
    {
        return OpenApple;
    }

    public static bool getSuccessApple()
    {
        return SuccessApple;
    }

    public static bool getFailureApple()
    {
        return FailureApple;
    }
}
