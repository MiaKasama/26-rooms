using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppleSpawn : MonoBehaviour
{
    [SerializeField]
    [Tooltip("生成するGameObject")]
    private GameObject createPrefab;

    //スポーンする座標と向き
    float x, y, z;
    int angleX, angleY, angleZ;

    //スポーンスパン
    float span = 0.3f;

    //経過時間
    float CurrentTime = 0.0f;

    //入力可能か
    public static bool InputSwitchApple = false;

    //スポーン個数
    int n;

    //個数を文字列に変換する
    public static string StringAppleN;

    //for文用変数
    int i = 0;

    //これがtrueになったらゲーム開始
    bool GameStart = false;

    // Start is called before the first frame update
    void Start()
    {
        InputSwitchApple = false;

        //5〜15個の中から個数を決定
        n = Random.Range(5, 16);

        StringAppleN = n.ToString();

        x = 0.0f;
        y = 5.0f;
        z = -5.0f;

        angleX = 0;
        angleY = 0;
        angleZ = 0;

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if(GameStart == true)
        {
            CurrentTime += Time.deltaTime;

            if (CurrentTime > span && i < n)
            {
                //ランダムに向きを決定
                angleX = Random.Range(0, 360);
                angleY = Random.Range(0, 360);
                angleZ = Random.Range(0, 360);

                //定義した位置にスポーン
                GameObject Apples = Instantiate(createPrefab, new Vector3(x, y, z), Quaternion.Euler(angleX, angleY, angleZ));

                CurrentTime = 0;
                i++;
            }

            if(i == n)
                StartCoroutine(InputStart());
            InputSwitchApple = false;
        }
    }

    //3秒後に開始
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        GameStart = true;
    }

    //全スポーン2秒後に入力可能に
    private IEnumerator InputStart()
    {
        yield return new WaitForSeconds(2.0f);

        InputSwitchApple = true;
        i++;
    }

    public static bool getInputSwitchApple()
    {
        return InputSwitchApple;
    }

    public static string getStringAppleN()
    {
        return StringAppleN;
    }
}
