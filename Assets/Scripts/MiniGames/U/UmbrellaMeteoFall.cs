using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UmbrellaMeteoFall : MonoBehaviour
{
    [SerializeField]
    [Tooltip("隕石の移動速度")]
    float speed = 0.01f;

    //横方向の隕石の移動速度
    float speedX;

    //落下音
    private AudioSource FallSound;
    bool SoundOn = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (SoundOn == true)
        {
            SoundOn = false;

            FallSound = GetComponent<AudioSource>();
            FallSound.Play();
        }

        speedX = speed * UmbrellaMeteoSpawn.getAngleRadUmbrella();

        this.transform.Translate(speedX, -speed, 0.0f);
    }
}
