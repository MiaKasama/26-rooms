using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UmbrellaMeteoSpawn : MonoBehaviour
{
    //スポーンするか
    bool CanSpawn = false;
    int SpawnCode = 0;

    bool FirstSpawn = false;

    [SerializeField]
    [Tooltip("生成するGameObject")]
    private GameObject createPrefab;

    //スポーンする座標と向き
    float x, y, z;
    int angleX, angleY, angleZ;

    //隕石の角度の調整
    int Angle;
    public static float AngleRadUmbrella;

    //スポーンの端の座標(着地点基準)
    float LeftX0, RightX0;

    //スポーンの端の座標
    float LeftX, RightX;

    //スポーンスパン
    float span = 0.5f;

    //スポーンチャンス
    bool SpawnChance = false;

    //経過時間
    float CurrentTime = 0.0f;

    //コルーチンスパン
    float CoroutineSpan = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        Angle = Random.Range(-16, 17);
        AngleRadUmbrella = Angle * Mathf.Deg2Rad;

        y = 5.0f;
        z = -5.0f;

        angleX = 0;
        angleY = 0;
        angleZ = Angle;

        //隕石の角度から、スポーンの範囲を設定する
        if (Angle <= 0)
        {
            LeftX0 = -4.5f;
            RightX0 = 4.5f + Angle / 16;
        }

        if (Angle >= 0)
        {
            LeftX0 = -4.5f + Angle / 16;
            RightX0 = 4.5f;
        }

        //tanを用いて正式なスポーン範囲を指定
        LeftX = LeftX0 - y * Mathf.Tan(AngleRadUmbrella);
        RightX = RightX0 - y * Mathf.Tan(AngleRadUmbrella);

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (CanSpawn == true && UmbrellaJudgement.getSuccessUmbrella() == false && UmbrellaJudgement.getFailureUmbrella() == false)
        {
            CanSpawn = false;

            x = Random.Range(LeftX, RightX);

            //定義した位置にスポーン
            GameObject Meteorite = Instantiate(createPrefab, new Vector3(x, y, z), Quaternion.Euler(angleX, angleY, angleZ));
        }

        if (FirstSpawn == true)
        {
            FirstSpawn = false;
            SpawnChance = true;
        }

        if (SpawnChance == true)
        {
            SpawnChance = false;

            StartCoroutine(SpawnCoroutine());
        }
    }

    //3秒後にスポーン開始
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        FirstSpawn = true;
        CanSpawn = true;
    }

    //一定時間毎に1/2の確率でスポーンさせる
    private IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(CoroutineSpan);

            CurrentTime += CoroutineSpan;

            if (CurrentTime >= span)
            {
                SpawnChance = true;
                CurrentTime = 0;

                SpawnCode = Random.Range(0, 2);

                if (SpawnCode == 0)
                    CanSpawn = true;

                break;
            }
        }
    }

    public static float getAngleRadUmbrella()
    {
        return AngleRadUmbrella;
    }
}
