using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UmbrellaJudgement : MonoBehaviour
{
    //成功か否か
    public static bool SuccessUmbrella = false;
    public static bool FailureUmbrella = false;

    //爆発
    private AudioSource MeteorSound;
    private GameObject Effect;

    float SuccessTime;

    // Start is called before the first frame update
    void Start()
    {
        SuccessUmbrella = false;
        FailureUmbrella = false;

        MeteorSound = GetComponent<AudioSource>();
        Effect = GameObject.Find("FailureExplosion");

        Effect.SetActive(false);

        SuccessTime = Random.Range(23.0f, 33.0f);

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //ここに隕石が当たったら失敗
    void OnCollisionEnter(Collision collision)
    {
        if (SuccessUmbrella == false && FailureUmbrella == false)
        {
            if (collision.gameObject.tag == "UmbrellaMeteorite")
            {
                FailureUmbrella = true;

                MeteorSound.Play();

                StartCoroutine(EffectCoroutine());
            }
        }
    }

    private IEnumerator EffectCoroutine()
    {
        Effect.SetActive(true);

        yield return new WaitForSeconds(1.0f);

        Effect.SetActive(false);
    }

    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(SuccessTime);

        if(FailureUmbrella == false)
            SuccessUmbrella = true;
    }

    public static bool getSuccessUmbrella()
    {
        return SuccessUmbrella;
    }

    public static bool getFailureUmbrella()
    {
        return FailureUmbrella;
    }
}
