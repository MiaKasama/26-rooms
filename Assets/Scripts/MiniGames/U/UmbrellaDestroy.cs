using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UmbrellaDestroy : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (UmbrellaJudgement.getFailureUmbrella())
            StartCoroutine(DestroyCoroutine());
    }

    private IEnumerator DestroyCoroutine()
    {
        yield return new WaitForSeconds(0.5f);

        Destroy(gameObject);
    }
}
