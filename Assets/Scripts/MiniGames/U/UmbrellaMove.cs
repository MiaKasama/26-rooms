using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UmbrellaMove : MonoBehaviour
{
    //傘を動かし始めるかどうか
    bool StartParasol = false;

    [SerializeField]
    [Tooltip("傘の移動速度")]
    float speed = 0.1f;

    [SerializeField]
    [Tooltip("移動できる左右の限界")]
    float side = 5.0f;

    //ガード音
    private AudioSource GuardSound;

    // Start is called before the first frame update
    void Start()
    {
        GuardSound = GetComponent<AudioSource>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //3秒後にスタート
        if (StartParasol == true && UmbrellaJudgement.getSuccessUmbrella() == false && UmbrellaJudgement.getFailureUmbrella() == false)
        {
            //傘の座標を取得
            Vector3 pos = this.transform.position;

            // 左に移動
            if (-side <= pos.x)
            {
                if (Input.GetKey(KeyCode.LeftArrow))
                    this.transform.Translate(-speed, 0.0f, 0.0f);
            }

            // 右に移動
            if (pos.x <= side)
            {
                if (Input.GetKey(KeyCode.RightArrow))
                    this.transform.Translate(speed, 0.0f, 0.0f);
            }
        }
    }

    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartParasol = true;
    }

    // 当たった隕石を消滅
    void OnCollisionEnter(Collision collision)
    {
        GuardSound.Play();

        if (collision.gameObject.tag == "UmbrellaMeteorite")
            Destroy(collision.gameObject);
    }
}
