using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NothingTextPos : MonoBehaviour
{
    [SerializeField]
    [Tooltip("表示開始までの時間")]
    float IndicateTextTime = 33.0f;

    bool TextStart = false;

    [SerializeField]
    [Tooltip("表示するのはXか")]
    bool boolX = true;

    Transform CameraTrans;

    private TextMeshProUGUI NothingProUI;

    //現在座標
    float x, y;

    // Start is called before the first frame update
    void Start()
    {
        CameraTrans = GameObject.Find("Main Camera").transform;

        NothingProUI = GetComponent<TextMeshProUGUI>();
        NothingProUI.text = "";

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (TextStart == true)
        {
            if (boolX == true)
            {
                x = Mathf.Round(CameraTrans.position.x);

                NothingProUI.SetText(x.ToString());
            }
            else
            {
                y = Mathf.Round(CameraTrans.position.y);

                NothingProUI.SetText(y.ToString());
            }
        }
    }

    //スタートから指定した時間経過でテキスト表示
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(IndicateTextTime);

        TextStart = true;
    }
}
