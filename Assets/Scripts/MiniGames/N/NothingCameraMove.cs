using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NothingCameraMove : MonoBehaviour
{
    //発見時のテキストオブジェクトのアクティブ用
    GameObject FindTextUI;

    //カメラを動かし始めるかどうか
    bool StartCamera = false;

    [SerializeField]
    [Tooltip("カメラの移動速度")]
    float speed = 0.1f;

    [SerializeField]
    [Tooltip("移動できる上下の限界")]
    float UpLim = 44.5f;

    [SerializeField]
    [Tooltip("移動できる左右の限界")]
    float RightLim = 39.8f;

    //現在の座標
    float x, y, z;

    //何かあったか
    public static bool SuccessNothing = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessNothing = false;

        FindTextUI = GameObject.Find("Thing");
        FindTextUI.SetActive(false);

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //3秒後にスタート
        if (StartCamera == true)
        {
            //上に移動
            if (Input.GetKey(KeyCode.UpArrow))
                this.transform.Translate(0.0f, speed, 0.0f);

            //下に移動
            if (Input.GetKey(KeyCode.DownArrow))
                this.transform.Translate(0.0f, -speed, 0.0f);

            //左に移動
            if (Input.GetKey(KeyCode.LeftArrow))
                this.transform.Translate(-speed, 0.0f, 0.0f);

            //右に移動
            if (Input.GetKey(KeyCode.RightArrow))
                this.transform.Translate(speed, 0.0f, 0.0f);

            //台の座標を取得
            x = this.transform.position.x;
            y = this.transform.position.y;
            z = this.transform.position.z;

            //上下左右で端を繋げる
            if (x >= RightLim)
                this.transform.position = new Vector3(-RightLim + 1.0f, y, z);

            if (x <= -RightLim)
                this.transform.position = new Vector3(RightLim - 1.0f, y, z);

            if (y >= UpLim)
                this.transform.position = new Vector3(x, -UpLim + 1.0f, z);

            if (y <= -UpLim)
                this.transform.position = new Vector3(x, UpLim - 1.0f, z);

            //指定の範囲内に入ったらクリア
            if (27.5 <= x && x <= 28.5 && -14.5 <= y && y <= -13.5)
            {
                StartCamera = false;

                this.transform.position = new Vector3(28.0f, -14.0f, z);
                SuccessNothing = true;

                FindTextUI.SetActive(true);
            }
        }
    }

    // 3秒後にスタート
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartCamera = true;
    }

    public static bool getSuccessNothing()
    {
        return SuccessNothing;
    }
}
