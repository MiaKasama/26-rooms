using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NothingText : MonoBehaviour
{
    [SerializeField]
    [Tooltip("表示するテキストの内容")]
    string NothingStr = "操作方法";

    [SerializeField]
    [Tooltip("表示開始までの時間")]
    float IndicateTextTime = 23.0f;

    private TextMeshProUGUI NothingProUI;

    // Start is called before the first frame update
    void Start()
    {
        NothingProUI = GetComponent<TextMeshProUGUI>();
        NothingProUI.text = "";

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //スタートから指定した時間経過でテキスト表示
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(IndicateTextTime);

        NothingProUI.text = NothingStr;
    }
}
