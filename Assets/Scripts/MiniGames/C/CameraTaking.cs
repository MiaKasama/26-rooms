using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTaking : MonoBehaviour
{
    [SerializeField]
    [Tooltip("指定範囲")]
    int xRange = 3;

    [SerializeField]
    [Tooltip("強制失敗範囲")]
    int FailureRange = 15;

    //撮影可能か
    bool TakeStart = false;

    //撮影音
    private AudioSource CameraSound;

    //撮影したか
    public static bool TookCamera = false;

    //成功か否か
    public static bool SuccessCamera = false;
    public static bool FailureCamera = false;

    //シーンにある車の数
    int count;

    // Start is called before the first frame update
    void Start()
    {
        SuccessCamera = false;
        FailureCamera = false;

        TookCamera = false;

        CameraSound = GetComponent<AudioSource>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        count = GameObject.FindGameObjectsWithTag("CameraCar").Length;

        if (TakeStart == true)
        {
            //Enterを押したら音を出す
            if (Input.GetKeyDown(KeyCode.Return))
            {
                TakeStart = false;
                TookCamera = true;

                CameraSound.Play();

                StartCoroutine(TookCoroutine());
            }
        }

        if(count != 0)
        {
            if(CameraCarMove.getxCamera() < -FailureRange || FailureRange < CameraCarMove.getxCamera())
                FailureCamera = true;
        }
    }

    //3秒後から撮影可能に
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        TakeStart = true;
    }

    private IEnumerator TookCoroutine()
    {
        yield return new WaitForSeconds(1.0f);

        //車が存在し、かつ車の座標が一定範囲内にいれば撮影成功
        if (-xRange <= CameraCarMove.getxCamera() && CameraCarMove.getxCamera() <= xRange && count != 0)
            SuccessCamera = true;
        else
            FailureCamera = true;
    }

    public static bool getTookCamera()
    {
        return TookCamera;
    }

    public static bool getSuccessCamera()
    {
        return SuccessCamera;
    }

    public static bool getFailureCamera()
    {
        return FailureCamera;
    }
}
