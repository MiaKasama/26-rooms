using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCarSpawn : MonoBehaviour
{
    [SerializeField]
    [Tooltip("生成するGameObject1")]
    private GameObject createPrefab1;

    [SerializeField]
    [Tooltip("生成するGameObject2")]
    private GameObject createPrefab2;

    [SerializeField]
    [Tooltip("生成するGameObject3")]
    private GameObject createPrefab3;

    [SerializeField]
    [Tooltip("生成するGameObject4")]
    private GameObject createPrefab4;

    //どのオブジェクトを生成するかを決定する変数
    int n;

    //スポーンする座標と向き
    [SerializeField]
    [Tooltip("スポーンするX座標")]
    private int x;

    int angleY;

    //左右どっちでスポーンさせるか（右がtrue）
    public static bool RightOrLeftCamera;
    int RandomSpawn;

    //スポーンするまでの時間
    float SpawnTime;

    //スポーン開始
    bool SpawnStart = false;

    // Start is called before the first frame update
    void Start()
    {
        //0〜9の中からランダム
        n = Random.Range(0, 10);

        //0ならtrue、1ならfalse
        RandomSpawn = Random.Range(0, 2);

        if (RandomSpawn == 0)
            RightOrLeftCamera = true;
        else
            RightOrLeftCamera = false;

        if(RightOrLeftCamera == true)
            angleY = -90;
        else
        {
            x = -1 * x;
            angleY = 90;
        }

        //開始から4〜8秒の間で一度スポーン
        SpawnTime = Random.Range(4, 8);

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //スポーン前に撮影した場合はスポーン自体を中止する
        if(SpawnStart == true && CameraTaking.getTookCamera() == false)
        {
            GameObject Car;

            SpawnStart = false;

            //定義した位置に、定義したプレハブをスポーン
            if (0 <= n && n < 4)
                Car = Instantiate(createPrefab1, new Vector3(x, 0.5f, -4), Quaternion.Euler(0, angleY, 0));
            else if (4 <= n && n < 8)
                Car = Instantiate(createPrefab2, new Vector3(x, 0.5f, -4), Quaternion.Euler(0, angleY, 0));
            else if (n == 8)
                Car = Instantiate(createPrefab3, new Vector3(x, 0.5f, -4), Quaternion.Euler(0, angleY, 0));
            else
                Car = Instantiate(createPrefab4, new Vector3(x, 0.5f, -4), Quaternion.Euler(0, angleY, 0));
        }
    }

    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(SpawnTime);

        SpawnStart = true;
    }

    public static bool getRightOrLeftCamera()
    {
        return RightOrLeftCamera;
    }
}
