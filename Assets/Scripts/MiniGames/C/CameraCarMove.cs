using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCarMove : MonoBehaviour
{
    [SerializeField]
    [Tooltip("車の移動速度")]
    float speed = 0.1f;

    //現座標
    public static float xCamera;

    //Enter処理は一回だけ
    bool EnterStop = true;

    // Start is called before the first frame update
    void Start()
    {
        //speed = 0.01f;
    }

    // Update is called once per frame
    void Update()
    {
        xCamera = this.transform.position.x;
        //Debug.Log(xCamera);
        
        //Enterを押したら車を停止し、x座標を取得する
        if (Input.GetKeyDown(KeyCode.Return) && EnterStop == true)
        {
            EnterStop = false;

            speed = 0.0f;

            //Debug.Log(xCamera);
        }

        this.transform.Translate(0.0f, 0.0f, speed);

        //クリア失敗後は座標を一定の値で留める
        if (CameraTaking.FailureCamera == true)
        {
            speed = 0;
            this.transform.position = new Vector3(10.0f, 0.5f, -4.0f);
        }
    }

    public static float getxCamera()
    {
        return xCamera;
    }
}
