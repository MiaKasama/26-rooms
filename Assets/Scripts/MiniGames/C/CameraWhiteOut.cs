using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWhiteOut : MonoBehaviour
{
    [SerializeField]
    [Tooltip("最終的に除去するか")]
    bool DestroyPlane = true;

    //撮影可能か
    bool TakeStart = false;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //Enterを押したら白面を移動・破壊
        if (TakeStart == true)
        {
            if (Input.GetKeyDown(KeyCode.Return))
                StartCoroutine(TookCoroutine());
        }
    }

    //3秒後から撮影可能に
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        TakeStart = true;
    }

    private IEnumerator TookCoroutine()
    {
        if(DestroyPlane == true)
        {
            this.transform.position = new Vector3(0.0f, 1.0f, -9.6f);

            yield return new WaitForSeconds(0.5f);

            Destroy(gameObject);
        }
        else
        {
            yield return new WaitForSeconds(0.5f);

            this.transform.position = new Vector3(0.0f, 0.0f, -9.5f);
        }
    }
}
