using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TypingInput : MonoBehaviour
{
    TMP_InputField InputTyping;
    GameObject TypingWindow;

    GameObject WrongText;
    GameObject ClearText;

    string AnswerTyping;

    //正解か否か
    public static bool SuccessTyping = false;
    public static bool FailureTyping = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessTyping = false;
        FailureTyping = false;

        TypingWindow = GameObject.Find("TypingWindow");
        InputTyping = TypingWindow.GetComponent<TMP_InputField>();

        WrongText = GameObject.Find("WrongText");
        ClearText = GameObject.Find("ClearText");

        //各オブジェクトを非表示
        TypingWindow.SetActive(false);
        WrongText.SetActive(false);
        ClearText.SetActive(false);

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //カウント0で失敗
        if (TypingCountDown.getSecondTyping() == 0)
        {
            FailureTyping = true;
            TypingWindow.SetActive(false);
        }
    }

    public void GetInputTyping()
    {
        AnswerTyping = InputTyping.text;

        if (AnswerTyping == TypingMissionText.getMissionTyping())
        {
            SuccessTyping = true;
            TypingWindow.SetActive(false);
            
            ClearText.SetActive(true);
            WrongText.SetActive(false);
        }
        else if(FailureTyping == false)
            StartCoroutine(WrongCoroutine());
    }

    //3秒後にInputFieldオブジェクトを表示
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        TypingWindow.SetActive(true);
    }

    //間違えた場合に一時的にテキストを表示
    private IEnumerator WrongCoroutine()
    {
        WrongText.SetActive(true);

        yield return new WaitForSeconds(1.5f);

        WrongText.SetActive(false);
    }

    public static bool getSuccessTyping()
    {
        return SuccessTyping;
    }

    public static bool getFailureTyping()
    {
        return FailureTyping;
    }
}
