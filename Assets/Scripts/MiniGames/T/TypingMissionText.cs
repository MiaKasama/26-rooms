using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TypingMissionText : MonoBehaviour
{
    private TextMeshPro TypingText;
    public static string MissionTyping;

    //ランダムテキスト表示用
    int TypingQ;

    int n = 10;

    // Start is called before the first frame update
    void Start()
    {
        TypingText = GetComponent<TextMeshPro>();
        TypingQ = Random.Range(0, n);

        //テキストを表示
        if (TypingQ == 0)
            MissionTyping = "カエルぴょこピョコ三ピョコぴょこピョコンとぴょぉん";
        else if (TypingQ == 1)
            MissionTyping = "日月火水木金土水金地火木土天海太陽月も加えちゃえい";
        else if (TypingQ == 2)
            MissionTyping = "あいうえおかきくけこたちつてとなにぬねにょはひへほ";
        else if (TypingQ == 3)
            MissionTyping = "たこ焼き食べたいたい焼き食べたいお好み焼き飲みたい";
        else if (TypingQ == 4)
            MissionTyping = "鹿島臨海鉄道大洗鹿島線長者ヶ浜潮騒はまなす公園前駅";
        else if (TypingQ == 5)
            MissionTyping = "私はあなたが好き好き愛してるずっとずっと見ているよ";
        else if (TypingQ == 6)
            MissionTyping = "もし明日地球が滅ぶのならば人は皆何をするのだろうか";
        else if (TypingQ == 7)
            MissionTyping = "君が思うブラック企業を減らす方法を提案してほしいな";
        else if (TypingQ == 8)
            MissionTyping = "全ての人間が悲しい思いをしない世界を作るのが夢だよ";
        else if (TypingQ == 9)
            MissionTyping = "あああああああああああああああああああああああああ";

        TypingText.text = MissionTyping;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static string getMissionTyping()
    {
        return MissionTyping;
    }
}
