using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TypingCountDown : MonoBehaviour
{
    //カウントダウンを始めるかどうか
    bool StartCount = false;
    bool CountCheck = true;

    //カウント表示
    TextMeshPro CountText;

    //残り何秒か
    public static int SecondTyping;

    //最初の残り時間
    int StartingTime;

    //カウント音
    private AudioSource CountSound;

    // Start is called before the first frame update
    void Start()
    {
        StartingTime = Random.Range(0, 8);

        if (StartingTime == 0)
            SecondTyping = 20;
        else if (StartingTime == 1 || StartingTime == 2 || StartingTime == 3)
            SecondTyping = 30;
        else if (StartingTime == 4 || StartingTime == 5 || StartingTime == 6)
            SecondTyping = 50;
        else
            SecondTyping = 99;

        CountText = GetComponent<TextMeshPro>();
        CountText.SetText(SecondTyping.ToString());

        CountSound = GetComponent<AudioSource>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //0秒になるまで以下の関数を呼び起こす
        if (StartCount == true && CountCheck == true && SecondTyping > 0 && TypingInput.getSuccessTyping() == false)
        {
            CountCheck = false;

            StartCoroutine(CountDownSecond());
        }
    }

    //3秒後にスタート
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartCount = true;
    }

    //1秒毎にカウントダウン
    private IEnumerator CountDownSecond()
    {
        CountSound.Play();

        yield return new WaitForSeconds(1.0f);

        SecondTyping--;
        CountText.SetText(SecondTyping.ToString());

        CountCheck = true;
    }

    public static int getSecondTyping()
    {
        return SecondTyping;
    }
}
