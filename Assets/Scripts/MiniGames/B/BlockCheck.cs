using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCheck : MonoBehaviour
{
    //全部壊せたらtrue
    public static bool SuccessBlock = false;

    // Start is called before the first frame update
    void Start()
    {
        SuccessBlock = false;
    }

    // Update is called once per frame
    void Update()
    {
        //残りブロック数をタグを用いて取得
        int count = GameObject.FindGameObjectsWithTag("Block").Length;

        if (count == 0)
            SuccessBlock = true;
    }

    public static bool getSuccessBlock()
    {
        return SuccessBlock;
    }
}
