using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockBallSpeed : MonoBehaviour
{
    //ボールを動かし始めるかどうか
    bool StartBall = false;

    //移動速度
    float speed = 15.0f;

    //右方向の移動速度
    float speedRight;

    //上方向の移動速度
    float speedUp;

    //移動の向き
    int angle;
    float angleRadian;

    //現在のy座標
    float y;

    //衝突音
    private AudioSource BallSound;

    // Start is called before the first frame update
    void Start()
    {
        BallSound = GetComponent<AudioSource>();

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //3秒後にスタート
        if (StartBall == true)
        {
            StartBall = false;

            //15°〜75°の範囲で向きを決定
            angle = Random.Range(15, 76);
            angleRadian = angle * Mathf.PI / 180;

            speedRight = -speed * Mathf.Cos(angleRadian);
            speedUp = -speed * Mathf.Sin(angleRadian);

            //var force = (transform.up + transform.right) * speed;
            var force = new Vector3(speedRight, speedUp, 0.0f);
            GetComponent<Rigidbody>().AddForce(force, ForceMode.VelocityChange);
        }
        
        y = this.transform.position.y;

        //ボールが落下したら元の位置に戻し、やり直しをする
        //速度情報は一旦リセット
        if(y <= -7.5)
        {
            transform.position = new Vector3(4.0f, 0.0f, 5.0f);
            StartBall = true;

            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        if (BlockCheck.getSuccessBlock() == true)
            GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartBall = true;
    }

    void OnCollisionEnter(Collision collision)
    {
        BallSound.Play();
    }
}
