using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockStandMove : MonoBehaviour
{
    //台を動かし始めるかどうか
    bool StartStand = false;

    [SerializeField]
    [Tooltip("台の移動速度")]
    float speed = 0.1f;

    [SerializeField]
    [Tooltip("移動できる左右の限界")]
    float side = 6.0f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //3秒後にスタート
        if(StartStand == true && BlockCheck.getSuccessBlock() == false)
        {
            //台の座標を取得
            Transform StandTrans = GameObject.Find("Stand").transform;
            Vector3 pos = StandTrans.position;

            // 左に移動
            if (-side <= pos.x)
            {
                if (Input.GetKey(KeyCode.LeftArrow))
                    this.transform.Translate(-speed, 0.0f, 0.0f);
            }

            // 右に移動
            if (pos.x <= side)
            {
                if (Input.GetKey(KeyCode.RightArrow))
                    this.transform.Translate(speed, 0.0f, 0.0f);
            }
        }
    }

    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        StartStand = true;
    }
}
