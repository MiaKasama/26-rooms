using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AlwaysGiveUpText : MonoBehaviour
{
    private TextMeshProUGUI AlwaysEnterText;

    bool AlwaysStart = false;

    // Start is called before the first frame update
    void Start()
    {
        AlwaysEnterText = GetComponent<TextMeshProUGUI>();
        AlwaysEnterText.text = "";

        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if(AlwaysStart == true)
            AlwaysEnterText.text = "Enter: 今のところは止めておこう…";

        if(MiniToMain.getSuccessTextBool() == true)
            AlwaysEnterText.text = "";
    }

    //3秒後に表示
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(3.0f);

        AlwaysStart = true;
    }
}
