using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngotSpawn : MonoBehaviour
{
    [SerializeField]
    [Tooltip("生成するGameObject")]
    private GameObject createPrefab;

    [SerializeField]
    [Tooltip("生成するGameObjectの色")]
    private Material[] ColorSet = new Material[26];

    int ColorNo = 0;

    //スポーンする座標と向き
    float x, y, z;
    int angleX, angleY, angleZ;

    //スポーン・SEスパン
    float span = 0.1f;
    float spanSE = 1.0f;

    //経過時間
    float CurrentTime = 0.0f;
    float CurrentTimeSE = 0.0f;

    //入手したコインの枚数(=出現したインゴットの数)
    int IngotPiece = 0;

    int FinalIngotPiece = 5;

    //ジャラジャラ音
    private AudioSource MoneyMoneySE;

    //for文用変数
    //int i = 0;

    [SerializeField]
    [Tooltip("どのくらい賞金をスポーンするか")]
    private int iMax = 260;

    //コイン消滅までの時間
    float EraserTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        MoneyMoneySE = GetComponent<AudioSource>();

        x = 0.0f;
        y = 10.0f;
        z = 5.0f;

        angleX = 0;
        angleY = 0;
        angleZ = 0;

        EraserTime = span * iMax;
    }

    // Update is called once per frame
    void Update()
    {
        IngotPiece = GoalDoor.getCatchCoins();

        if(IngotPiece == FinalIngotPiece)
        {
            CurrentTime += Time.deltaTime;
            CurrentTimeSE += Time.deltaTime;

            if (CurrentTime > span)
            {
                //指定した範囲内でランダムに位置と向きを決定
                x = Random.Range(-5.0f, 5.0f);
                z = Random.Range(4.0f, 6.0f);

                angleX = Random.Range(0, 360);
                angleY = Random.Range(0, 360);
                angleZ = Random.Range(0, 360);

                //色をランダムに設定
                ColorNo = Random.Range(0, 26);

                //定義した位置にスポーン
                GameObject CoinRainbow = Instantiate(createPrefab, new Vector3(x, y, z), Quaternion.Euler(angleX, angleY, angleZ));

                //マテリアルを設定
                CoinRainbow.GetComponent<MeshRenderer>().material = ColorSet[ColorNo];

                //マテリアルを破壊
                Destroy(CoinRainbow, EraserTime);

                CurrentTime = 0;
            }

            if (CurrentTimeSE > spanSE)
            {
                //ジャラジャラ音を出す
                MoneyMoneySE.Play();

                CurrentTimeSE = 0;
            }

            /*
            for (i = 0; i < iMax; i++)
            {
                //指定した範囲内でランダムに位置と向きを決定
                x = Random.Range(-1.0f, 1.0f);
                z = Random.Range(4.0f, 6.0f);

                angleX = Random.Range(0, 360);
                angleY = Random.Range(0, 360);
                angleZ = Random.Range(0, 360);

                //色をランダムに設定
                ColorNo = Random.Range(0, 26);

                //定義した位置にスポーン
                GameObject CoinRainbow = Instantiate(createPrefab, new Vector3(x, y, z), Quaternion.Euler(angleX, angleY, angleZ));

                //マテリアルを設定
                CoinRainbow.GetComponent<MeshRenderer>().material = ColorSet[ColorNo];
            }
            */
        }
    }
}
