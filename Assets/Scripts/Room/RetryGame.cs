using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RetryGame : MonoBehaviour
{
    //初期化タイム
    public static bool InitializeStartTime = false;

    //待つかどうか
    bool Waiting = true;

    //テキストの点滅表示をするか
    bool RetryGameBool = false;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(InitializeTime_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //ゲームオーバーまたはクリアでリトライ可能に
        //ゲームオーバーは1秒後、クリアは4秒後で
        if (GameOverInformation.getGameOverSet() == true && Waiting == true)
        {
            //コルーチンの実行
            StartCoroutine(OverText_frames());

            Waiting = false;
        }

        if (NowRoom.getClearCheck() == true && Waiting == true)
        {
            //コルーチンの実行
            StartCoroutine(ClearText_frames());

            Waiting = false;
        }

        //Enterを押してリトライ
        if (RetryGameBool == true)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                // MainStageを再読み込み
                //コルーチンの実行
                StartCoroutine(RetryMainStage());
            }
        }
    }

    private IEnumerator InitializeTime_frames()
    {
        yield return new WaitForSeconds(0.1f);

        InitializeStartTime = false;
    }

    private IEnumerator OverText_frames()
    {
        yield return new WaitForSeconds(1.0f);

        RetryGameBool = true;
    }

    private IEnumerator ClearText_frames()
    {
        yield return new WaitForSeconds(4.0f);

        RetryGameBool = true;
    }

    private IEnumerator RetryMainStage()
    {
        InitializeStartTime = true;

        yield return null;

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public static bool getInitializeStartTime()
    {
        return InitializeStartTime;
    }
}
