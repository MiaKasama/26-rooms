using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalDoor : MonoBehaviour
{
    /*
    [SerializeField]
    [Tooltip("扉を動かす角度")]
    private int DoorTilt;

    int turn = 0;
    */

    //獲得したコインの枚数
    public static int CatchCoins = 0;

    //処理したコイン
    public static bool CatchA = false;
    public static bool CatchB = false;
    public static bool CatchC = false;
    public static bool CatchD = false;
    public static bool CatchE = false;
    public static bool CatchF = false;
    public static bool CatchG = false;
    public static bool CatchH = false;
    public static bool CatchI = false;
    public static bool CatchJ = false;
    public static bool CatchK = false;
    public static bool CatchL = false;
    public static bool CatchM = false;
    public static bool CatchN = false;
    public static bool CatchO = false;
    public static bool CatchP = false;
    public static bool CatchQ = false;
    public static bool CatchR = false;
    public static bool CatchS = false;
    public static bool CatchT = false;
    public static bool CatchU = false;
    public static bool CatchV = false;
    public static bool CatchW = false;
    public static bool CatchX = false;
    public static bool CatchY = false;
    public static bool CatchZ = false;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        //StartCoroutine(OpenDoor());

        //

        //リトライ時、獲得コイン数をリセット
        if (RetryGame.getInitializeStartTime() == true)
        {
            CatchCoins = 0;
            
            CatchA = false;
            CatchB = false;
            CatchC = false;
            CatchD = false;
            CatchE = false;
            CatchF = false;
            CatchG = false;
            CatchH = false;
            CatchI = false;
            CatchJ = false;
            CatchK = false;
            CatchL = false;
            CatchM = false;
            CatchN = false;
            CatchO = false;
            CatchP = false;
            CatchQ = false;
            CatchR = false;
            CatchS = false;
            CatchT = false;
            CatchU = false;
            CatchV = false;
            CatchW = false;
            CatchX = false;
            CatchY = false;
            CatchZ = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (CoinAcquierA.getCatchCoinA() == 1 && CatchA == false)
        {
            CatchCoins++;
            CatchA = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierB.getCatchCoinB() == 2 && CatchB == false)
        {
            CatchCoins++;
            CatchB = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierC.getCatchCoinC() == 3 && CatchC == false)
        {
            CatchCoins++;
            CatchC = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierD.getCatchCoinD() == 4 && CatchD == false)
        {
            CatchCoins++;
            CatchD = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierE.getCatchCoinE() == 5 && CatchE == false)
        {
            CatchCoins++;
            CatchE = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierF.getCatchCoinF() == 6 && CatchF == false)
        {
            CatchCoins++;
            CatchF = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierG.getCatchCoinG() == 7 && CatchG == false)
        {
            CatchCoins++;
            CatchG = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierH.getCatchCoinH() == 8 && CatchH == false)
        {
            CatchCoins++;
            CatchH = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierI.getCatchCoinI() == 9 && CatchI == false)
        {
            CatchCoins++;
            CatchI = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierJ.getCatchCoinJ() == 10 && CatchJ == false)
        {
            CatchCoins++;
            CatchJ = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierK.getCatchCoinK() == 11 && CatchK == false)
        {
            CatchCoins++;
            CatchK = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierL.getCatchCoinL() == 12 && CatchL == false)
        {
            CatchCoins++;
            CatchL = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierM.getCatchCoinM() == 13 && CatchM == false)
        {
            CatchCoins++;
            CatchM = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierN.getCatchCoinN() == 14 && CatchN == false)
        {
            CatchCoins++;
            CatchN = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierO.getCatchCoinO() == 15 && CatchO == false)
        {
            CatchCoins++;
            CatchO = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierP.getCatchCoinP() == 16 && CatchP == false)
        {
            CatchCoins++;
            CatchP = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierQ.getCatchCoinQ() == 17 && CatchQ == false)
        {
            CatchCoins++;
            CatchQ = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierR.getCatchCoinR() == 18 && CatchR == false)
        {
            CatchCoins++;
            CatchR = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierS.getCatchCoinS() == 19 && CatchS == false)
        {
            CatchCoins++;
            CatchS = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierT.getCatchCoinT() == 20 && CatchT == false)
        {
            CatchCoins++;
            CatchT = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierU.getCatchCoinU() == 21 && CatchU == false)
        {
            CatchCoins++;
            CatchU = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierV.getCatchCoinV() == 22 && CatchV == false)
        {
            CatchCoins++;
            CatchV = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierW.getCatchCoinW() == 23 && CatchW == false)
        {
            CatchCoins++;
            CatchW = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierX.getCatchCoinX() == 24 && CatchX == false)
        {
            CatchCoins++;
            CatchX = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierY.getCatchCoinY() == 25 && CatchY == false)
        {
            CatchCoins++;
            CatchY = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        if (CoinAcquierZ.getCatchCoinZ() == 26 && CatchZ == false)
        {
            CatchCoins++;
            CatchZ = true;
            Debug.Log("CatchCoins =" + CatchCoins);
        }

        //コイン5枚集めたら扉を開ける
        /*
        if(CatchCoins == 5)
        {
            CatchCoins = 0;
            StartCoroutine(OpenDoor());
        }
        */
    }

    /*
    private IEnumerator OpenDoor()
    {
        yield return new WaitForSeconds(3.0f);

        if(DoorTilt >= 0)
        {
            for (turn = 0; turn < DoorTilt; turn++)
            {
                transform.Rotate(0, 1, 0);
                yield return new WaitForSeconds(0.01f);
            }
        }
        else
        {
            for (turn = 0; turn > DoorTilt; turn--)
            {
                transform.Rotate(0, -1, 0);
                yield return new WaitForSeconds(0.01f);
            }
        }
    }
    */

    public static int getCatchCoins()
    {
        return CatchCoins;
    }
}
