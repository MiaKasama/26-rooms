using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverSE : MonoBehaviour
{
    //ゲームオーバー音
    private AudioSource OverSE;

    //ゲームオーバー音は一度だけ
    bool SEOnce = true;

    // Start is called before the first frame update
    void Start()
    {
        OverSE = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameOverInformation.getGameOverSet() && SEOnce == true)
        {
            SEOnce = false;
            StartCoroutine(GameOverCoroutine());
        }
    }

    private IEnumerator GameOverCoroutine()
    {
        yield return new WaitForSeconds(1.5f);

        OverSE.Play();
    }
}
