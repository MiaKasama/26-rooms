using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NowRoom : MonoBehaviour
{
    // プレイヤーの座標取得
    float x, z;

    //各部屋に入ったかどうか

    public static bool inRoomA = false;
    public static bool inRoomB = false;
    public static bool inRoomC = false;
    public static bool inRoomD = false;
    public static bool inRoomE = false;
    public static bool inRoomF = false;
    public static bool inRoomG = false;
    public static bool inRoomH = false;
    public static bool inRoomI = false;
    public static bool inRoomJ = false;
    public static bool inRoomK = false;
    public static bool inRoomL = false;
    public static bool inRoomM = false;
    public static bool inRoomN = false;
    public static bool inRoomO = false;
    public static bool inRoomP = false;
    public static bool inRoomQ = false;
    public static bool inRoomR = false;
    public static bool inRoomS = false;
    public static bool inRoomT = false;
    public static bool inRoomU = false;
    public static bool inRoomV = false;
    public static bool inRoomW = false;
    public static bool inRoomX = false;
    public static bool inRoomY = false;
    public static bool inRoomZ = false;

    //クリア部屋に立ち入った時点でクリア
    public static bool ClearCheck = false;

    void Awake()
    {
        //ClearCheck = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        ClearCheck = false;
    }

    // Update is called once per frame
    void Update()
    {
        x = GameObject.Find("PlayerArmature").transform.position.x;
        z = GameObject.Find("PlayerArmature").transform.position.z;

        inRoom();
    }

    void inRoom()
    {
        // 部屋の順番

        // Q R C K N L V
        // J M E 1 O Y D
        // G H A 0 I F Z
        // B W S U T X P

        // -35 < x && x < -25
        if (-35 < x && x < -25 && -20 < z && z < -10)
            inRoomB = true;
        else
            inRoomB = false;

        if (-35 < x && x < -25 && -10 < z && z < 0)
            inRoomG = true;
        else
            inRoomG = false;

        if (-35 < x && x < -25 && 0 < z && z < 10)
            inRoomJ = true;
        else
            inRoomJ = false;

        if (-35 < x && x < -25 && 10 < z && z < 20)
            inRoomQ = true;
        else
            inRoomQ = false;

        // -25 < x && x < -15
        if (-25 < x && x < -15 && -20 < z && z < -10)
            inRoomW = true;
        else
            inRoomW = false;

        if (-25 < x && x < -15 && -10 < z && z < 0)
            inRoomH = true;
        else
            inRoomH = false;

        if (-25 < x && x < -15 && 0 < z && z < 10)
            inRoomM = true;
        else
            inRoomM = false;

        if (-25 < x && x < -15 && 10 < z && z < 20)
            inRoomR = true;
        else
            inRoomR = false;

        // -15 < x && x < -5
        if (-15 < x && x < -5 && -20 < z && z < -10)
            inRoomS = true;
        else
            inRoomS = false;

        if (-15 < x && x < -5 && -10 < z && z < 0)
            inRoomA = true;
        else
            inRoomA = false;

        if (-15 < x && x < -5 && 0 < z && z < 10)
            inRoomE = true;
        else
            inRoomE = false;

        if (-15 < x && x < -5 && 10 < z && z < 20)
            inRoomC = true;
        else
            inRoomC = false;

        // -5 < x && x < 5
        if (-5 < x && x < 5 && -20 < z && z < -10)
            inRoomU = true;
        else
            inRoomU = false;

        if (-5 < x && x < 5 && 10 < z && z < 20)
            inRoomK = true;
        else
            inRoomK = false;

        // 5 < x && x < 15
        if (5 < x && x < 15 && -20 < z && z < -10)
            inRoomT = true;
        else
            inRoomT = false;

        if (5 < x && x < 15 && -10 < z && z < 0)
            inRoomI = true;
        else
            inRoomI = false;

        if (5 < x && x < 15 && 0 < z && z < 10)
            inRoomO = true;
        else
            inRoomO = false;

        if (5 < x && x < 15 && 10 < z && z < 20)
            inRoomN = true;
        else
            inRoomN = false;

        // 15 < x && x < 25
        if (15 < x && x < 25 && -20 < z && z < -10)
            inRoomX = true;
        else
            inRoomX = false;

        if (15 < x && x < 25 && -10 < z && z < 0)
            inRoomF = true;
        else
            inRoomF = false;

        if (15 < x && x < 25 && 0 < z && z < 10)
            inRoomY = true;
        else
            inRoomY = false;

        if (15 < x && x < 25 && 10 < z && z < 20)
            inRoomL = true;
        else
            inRoomL = false;

        // 25 < x && x < 35
        if (25 < x && x < 35 && -20 < z && z < -10)
            inRoomP = true;
        else
            inRoomP = false;

        if (25 < x && x < 35 && -10 < z && z < 0)
            inRoomZ = true;
        else
            inRoomZ = false;

        if (25 < x && x < 35 && 0 < z && z < 10)
            inRoomD = true;
        else
            inRoomD = false;

        if (25 < x && x < 35 && 10 < z && z < 20)
            inRoomV = true;
        else
            inRoomV = false;

        // クリア部屋
        if (-4 < x && x < 4 && 1 < z && z < 9)
            ClearCheck = true;
        else
            ClearCheck = false;
    }

    // 入っている部屋の情報を返す
    public static bool getinRoomA()
    {
        return inRoomA;
    }

    public static bool getinRoomB()
    {
        return inRoomB;
    }

    public static bool getinRoomC()
    {
        return inRoomC;
    }

    public static bool getinRoomD()
    {
        return inRoomD;
    }

    public static bool getinRoomE()
    {
        return inRoomE;
    }

    public static bool getinRoomF()
    {
        return inRoomF;
    }

    public static bool getinRoomG()
    {
        return inRoomG;
    }

    public static bool getinRoomH()
    {
        return inRoomH;
    }

    public static bool getinRoomI()
    {
        return inRoomI;
    }

    public static bool getinRoomJ()
    {
        return inRoomJ;
    }

    public static bool getinRoomK()
    {
        return inRoomK;
    }

    public static bool getinRoomL()
    {
        return inRoomL;
    }

    public static bool getinRoomM()
    {
        return inRoomM;
    }

    public static bool getinRoomN()
    {
        return inRoomN;
    }

    public static bool getinRoomO()
    {
        return inRoomO;
    }

    public static bool getinRoomP()
    {
        return inRoomP;
    }

    public static bool getinRoomQ()
    {
        return inRoomQ;
    }

    public static bool getinRoomR()
    {
        return inRoomR;
    }

    public static bool getinRoomS()
    {
        return inRoomS;
    }

    public static bool getinRoomT()
    {
        return inRoomT;
    }

    public static bool getinRoomU()
    {
        return inRoomU;
    }

    public static bool getinRoomV()
    {
        return inRoomV;
    }

    public static bool getinRoomW()
    {
        return inRoomW;
    }

    public static bool getinRoomX()
    {
        return inRoomX;
    }

    public static bool getinRoomY()
    {
        return inRoomY;
    }

    public static bool getinRoomZ()
    {
        return inRoomZ;
    }

    public static bool getClearCheck()
    {
        return ClearCheck;
    }
}
