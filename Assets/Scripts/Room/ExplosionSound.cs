using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSound : MonoBehaviour
{
    //爆発音
    private AudioSource ExplosionSE;

    //爆発音は一度だけ
    bool SEOnce = true;

    // Start is called before the first frame update
    void Start()
    {
        ExplosionSE = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(GameOverInformation.getGameOverSet() == true && SEOnce == true)
        {
            SEOnce = false;
            ExplosionSE.Play();
        }
    }
}
