using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverInformation : MonoBehaviour
{
    //ゲームオーバーにするか
    public static bool GameOverSet = false;

    //間違ったコインを取得したか
    bool WrongCoinA = false;
    bool WrongCoinB = false;
    bool WrongCoinC = false;
    bool WrongCoinD = false;
    bool WrongCoinE = false;
    bool WrongCoinF = false;
    bool WrongCoinG = false;
    bool WrongCoinH = false;
    bool WrongCoinI = false;
    bool WrongCoinJ = false;
    bool WrongCoinK = false;
    bool WrongCoinL = false;
    bool WrongCoinM = false;
    bool WrongCoinN = false;
    bool WrongCoinO = false;
    bool WrongCoinP = false;
    bool WrongCoinQ = false;
    bool WrongCoinR = false;
    bool WrongCoinS = false;
    bool WrongCoinT = false;
    bool WrongCoinU = false;
    bool WrongCoinV = false;
    bool WrongCoinW = false;
    bool WrongCoinX = false;
    bool WrongCoinY = false;
    bool WrongCoinZ = false;

    void Awake()
    {
        //if (RetryGame.getInitializeStartTime() == true)
            //GameOverSet = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (RetryGame.getInitializeStartTime() == true)
            GameOverSet = false;
    }

    // Update is called once per frame
    void Update()
    {
        WrongCoinA = CoinAcquierA.getGameOverA();
        WrongCoinB = CoinAcquierB.getGameOverB();
        WrongCoinC = CoinAcquierC.getGameOverC();
        WrongCoinD = CoinAcquierD.getGameOverD();
        WrongCoinE = CoinAcquierE.getGameOverE();
        WrongCoinF = CoinAcquierF.getGameOverF();
        WrongCoinG = CoinAcquierG.getGameOverG();
        WrongCoinH = CoinAcquierH.getGameOverH();
        WrongCoinI = CoinAcquierI.getGameOverI();
        WrongCoinJ = CoinAcquierJ.getGameOverJ();
        WrongCoinK = CoinAcquierK.getGameOverK();
        WrongCoinL = CoinAcquierL.getGameOverL();
        WrongCoinM = CoinAcquierM.getGameOverM();
        WrongCoinN = CoinAcquierN.getGameOverN();
        WrongCoinO = CoinAcquierO.getGameOverO();
        WrongCoinP = CoinAcquierP.getGameOverP();
        WrongCoinQ = CoinAcquierQ.getGameOverQ();
        WrongCoinR = CoinAcquierR.getGameOverR();
        WrongCoinS = CoinAcquierS.getGameOverS();
        WrongCoinT = CoinAcquierT.getGameOverT();
        WrongCoinU = CoinAcquierU.getGameOverU();
        WrongCoinV = CoinAcquierV.getGameOverV();
        WrongCoinW = CoinAcquierW.getGameOverW();
        WrongCoinX = CoinAcquierX.getGameOverX();
        WrongCoinY = CoinAcquierY.getGameOverY();
        WrongCoinZ = CoinAcquierZ.getGameOverZ();

        if(WrongCoinA == true || WrongCoinB == true || WrongCoinC == true || WrongCoinD == true || WrongCoinE == true || WrongCoinF == true || WrongCoinG == true || 
            WrongCoinH == true || WrongCoinI == true || WrongCoinJ == true || WrongCoinK == true || WrongCoinL == true || WrongCoinM == true || WrongCoinN == true ||
            WrongCoinO == true || WrongCoinP == true || WrongCoinQ == true || WrongCoinR == true || WrongCoinS == true || WrongCoinT == true || WrongCoinU == true ||
            WrongCoinV == true || WrongCoinW == true || WrongCoinX == true || WrongCoinY == true || WrongCoinZ == true)
        {
            GameOverSet = true;
            //Debug.Log("Game Over...");
            //if (RetryGame.getInitializeStartTime() == true)
                //GameOverSet = false;
        }
    }

    public static bool getGameOverSet()
    {
        return GameOverSet;
    }
}
