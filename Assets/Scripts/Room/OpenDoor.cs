using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    [SerializeField]
    [Tooltip("扉を動かす角度")]
    private int DoorTilt;

    //手に入れたコイン
    int CatchText;

    //開けゴマ
    bool HirakeGoma = true;

    int turn = 0;

    //ドアの開く音
    private AudioSource DoorSE;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(OpenDoors());
        DoorSE = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        CatchText = GoalDoor.getCatchCoins();

        //コイン5枚集めたら扉を開ける
        if (CatchText == 5 && HirakeGoma == true)
        {
            StartCoroutine(OpenDoors());
            HirakeGoma = false;

            DoorSE.Play();
        }
    }

    //ぐるっと扉を空ける
    private IEnumerator OpenDoors()
    {
        yield return new WaitForSeconds(2.0f);

        if (DoorTilt >= 0)
        {
            for (turn = 0; turn < DoorTilt; turn++)
            {
                transform.Rotate(0, 1, 0);
                yield return new WaitForSeconds(0.01f);
            }
        }
        else
        {
            for (turn = 0; turn > DoorTilt; turn--)
            {
                transform.Rotate(0, -1, 0);
                yield return new WaitForSeconds(0.01f);
            }
        }
    }
}
