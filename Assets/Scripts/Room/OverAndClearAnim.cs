using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverAndClearAnim : MonoBehaviour
{
    private Animator anim;

    [SerializeField]
    [Tooltip("死亡エフェクト")]
    GameObject Effect;

    bool ExplosionBool = true;

    // プレイヤーの座標取得
    float x, y, z;

    // Start is called before the first frame update
    void Start()
    {
        //変数animに、Animatorコンポーネントを設定する
        anim = gameObject.GetComponent<Animator>();

        Effect.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //ゲームオーバーになったら死亡動作をonにする
        if (GameOverInformation.getGameOverSet() == true)
        {
            if (ExplosionBool == true)
            {
                //プレイヤーの位置で爆発させる
                x = GameObject.Find("PlayerArmature").transform.position.x;
                y = GameObject.Find("PlayerArmature").transform.position.y + 1;
                z = GameObject.Find("PlayerArmature").transform.position.z;

                Effect.transform.position = new Vector3(x, y, z);

                Effect.SetActive(true);
                ExplosionBool = false;
            }

            //爆発は一回だけ
            StartCoroutine(EffectSentence());

            anim.SetBool("DeathBool", true);
        }

        //クリアになったらダンス動作をonにする
        if (NowRoom.getClearCheck() == true)
            anim.SetBool("DanceBool", true);
    }

    IEnumerator EffectSentence()
    {
        yield return new WaitForSeconds(1.0f);

        Effect.SetActive(false);
    }
}
