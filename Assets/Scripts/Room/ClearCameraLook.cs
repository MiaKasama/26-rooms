using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearCameraLook : MonoBehaviour
{
    Vector3 LookAtCamera = new Vector3(0.0f, 0.0f, 0.0f);

    // 自身のTransform
    [SerializeField]
    [Tooltip("プレイヤーキャラを選択")]
    private Transform SelfCharacter;

    // ターゲットのTransform
    [SerializeField]
    [Tooltip("カメラを選択")]
    private Transform TargetCamera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //クリアになったらカメラの方向を向く
        if (NowRoom.getClearCheck() == true)
        {
            LookAtCamera.x = TargetCamera.position.x;
            LookAtCamera.y = 0;
            LookAtCamera.z = TargetCamera.position.z;

            SelfCharacter.LookAt(LookAtCamera);
        }
    }
}
