using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameClearSE : MonoBehaviour
{
    //ファンファーレ
    private AudioSource ClearSE;

    //ファンファーレは一度だけ
    bool SEOnce = true;

    // Start is called before the first frame update
    void Start()
    {
        ClearSE = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(NowRoom.getClearCheck() && SEOnce == true)
        {
            SEOnce = false;
            ClearSE.Play();
        }
    }
}
