using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierM : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 13;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverM = false;

    //コイン取得情報
    public static int CatchCoinM = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomM();
        CoinGet = SimpleCollectibleScript.getAquireCoinM();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinM Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverM = true;
            else
                CatchCoinM = 13;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverM = false;
            CatchCoinM = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinM()
    {
        return CatchCoinM;
    }

    public static bool getGameOverM()
    {
        return GameOverM;
    }
}
