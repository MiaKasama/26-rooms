using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierE : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 5;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverE = false;

    //コイン取得情報
    public static int CatchCoinE = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomE();
        CoinGet = SimpleCollectibleScript.getAquireCoinE();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinE Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverE = true;
            else
                CatchCoinE = 5;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverE = false;
            CatchCoinE = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinE()
    {
        return CatchCoinE;
    }

    public static bool getGameOverE()
    {
        return GameOverE;
    }
}
