using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierW : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 23;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverW = false;

    //コイン取得情報
    public static int CatchCoinW = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomW();
        CoinGet = SimpleCollectibleScript.getAquireCoinW();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinW Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverW = true;
            else
                CatchCoinW = 23;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverW = false;
            CatchCoinW = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinW()
    {
        return CatchCoinW;
    }

    public static bool getGameOverW()
    {
        return GameOverW;
    }
}
