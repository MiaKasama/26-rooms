using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierQ : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 17;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverQ = false;

    //コイン取得情報
    public static int CatchCoinQ = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomQ();
        CoinGet = SimpleCollectibleScript.getAquireCoinQ();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinQ Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverQ = true;
            else
                CatchCoinQ = 17;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverQ = false;
            CatchCoinQ = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinQ()
    {
        return CatchCoinQ;
    }

    public static bool getGameOverQ()
    {
        return GameOverQ;
    }
}
