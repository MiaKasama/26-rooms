using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierF : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 6;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverF = false;

    //コイン取得情報
    public static int CatchCoinF = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomF();
        CoinGet = SimpleCollectibleScript.getAquireCoinF();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinF Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverF = true;
            else
                CatchCoinF = 6;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverF = false;
            CatchCoinF = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinF()
    {
        return CatchCoinF;
    }

    public static bool getGameOverF()
    {
        return GameOverF;
    }
}
