using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierK : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 11;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverK = false;

    //コイン取得情報
    public static int CatchCoinK = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomK();
        CoinGet = SimpleCollectibleScript.getAquireCoinK();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinK Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverK = true;
            else
                CatchCoinK = 11;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverK = false;
            CatchCoinK = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinK()
    {
        return CatchCoinK;
    }

    public static bool getGameOverK()
    {
        return GameOverK;
    }
}
