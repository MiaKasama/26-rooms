using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierC : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 3;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverC = false;

    //コイン取得情報
    public static int CatchCoinC = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomC();
        CoinGet = SimpleCollectibleScript.getAquireCoinC();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinC Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverC = true;
            else
                CatchCoinC = 3;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverC = false;
            CatchCoinC = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinC()
    {
        return CatchCoinC;
    }

    public static bool getGameOverC()
    {
        return GameOverC;
    }
}
