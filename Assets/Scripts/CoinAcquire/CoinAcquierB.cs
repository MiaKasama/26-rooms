using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierB : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 2;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverB = false;

    //コイン取得情報
    public static int CatchCoinB = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomB();
        CoinGet = SimpleCollectibleScript.getAquireCoinB();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinB Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverB = true;
            else
                CatchCoinB = 2;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverB = false;
            CatchCoinB = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinB()
    {
        return CatchCoinB;
    }

    public static bool getGameOverB()
    {
        return GameOverB;
    }
}
