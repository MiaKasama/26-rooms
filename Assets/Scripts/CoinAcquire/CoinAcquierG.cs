using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierG : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 7;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverG = false;

    //コイン取得情報
    public static int CatchCoinG = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomG();
        CoinGet = SimpleCollectibleScript.getAquireCoinG();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinG Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverG = true;
            else
                CatchCoinG = 7;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverG = false;
            CatchCoinG = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinG()
    {
        return CatchCoinG;
    }

    public static bool getGameOverG()
    {
        return GameOverG;
    }
}
