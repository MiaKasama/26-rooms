using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierP : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 16;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverP = false;

    //コイン取得情報
    public static int CatchCoinP = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomP();
        CoinGet = SimpleCollectibleScript.getAquireCoinP();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinP Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverP = true;
            else
                CatchCoinP = 16;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverP = false;
            CatchCoinP = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinP()
    {
        return CatchCoinP;
    }

    public static bool getGameOverP()
    {
        return GameOverP;
    }
}
