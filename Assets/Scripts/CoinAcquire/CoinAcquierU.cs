using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierU : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 21;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverU = false;

    //コイン取得情報
    public static int CatchCoinU = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomU();
        CoinGet = SimpleCollectibleScript.getAquireCoinU();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinU Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverU = true;
            else
                CatchCoinU = 21;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverU = false;
            CatchCoinU = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinU()
    {
        return CatchCoinU;
    }

    public static bool getGameOverU()
    {
        return GameOverU;
    }
}
