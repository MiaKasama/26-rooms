using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierI : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 9;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverI = false;

    //コイン取得情報
    public static int CatchCoinI = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomI();
        CoinGet = SimpleCollectibleScript.getAquireCoinI();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinI Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverI = true;
            else
                CatchCoinI = 9;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverI = false;
            CatchCoinI = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinI()
    {
        return CatchCoinI;
    }

    public static bool getGameOverI()
    {
        return GameOverI;
    }
}
