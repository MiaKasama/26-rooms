using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierN : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 14;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverN = false;

    //コイン取得情報
    public static int CatchCoinN = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomN();
        CoinGet = SimpleCollectibleScript.getAquireCoinN();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinN Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverN = true;
            else
                CatchCoinN = 14;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverN = false;
            CatchCoinN = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinN()
    {
        return CatchCoinN;
    }

    public static bool getGameOverN()
    {
        return GameOverN;
    }
}
