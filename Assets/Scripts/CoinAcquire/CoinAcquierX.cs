using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierX : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 24;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverX = false;

    //コイン取得情報
    public static int CatchCoinX = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomX();
        CoinGet = SimpleCollectibleScript.getAquireCoinX();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinX Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverX = true;
            else
                CatchCoinX = 24;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverX = false;
            CatchCoinX = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinX()
    {
        return CatchCoinX;
    }

    public static bool getGameOverX()
    {
        return GameOverX;
    }
}
