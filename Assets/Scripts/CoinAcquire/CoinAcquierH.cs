using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierH : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 8;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverH = false;

    //コイン取得情報
    public static int CatchCoinH = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomH();
        CoinGet = SimpleCollectibleScript.getAquireCoinH();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinH Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverH = true;
            else
                CatchCoinH = 8;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverH = false;
            CatchCoinH = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinH()
    {
        return CatchCoinH;
    }

    public static bool getGameOverH()
    {
        return GameOverH;
    }
}
