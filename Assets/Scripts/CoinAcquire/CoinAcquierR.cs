using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierR : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 18;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverR = false;

    //コイン取得情報
    public static int CatchCoinR = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomR();
        CoinGet = SimpleCollectibleScript.getAquireCoinR();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinR Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverR = true;
            else
                CatchCoinR = 18;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverR = false;
            CatchCoinR = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinR()
    {
        return CatchCoinR;
    }

    public static bool getGameOverR()
    {
        return GameOverR;
    }
}
