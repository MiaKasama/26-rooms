using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierS : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 19;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverS = false;

    //コイン取得情報
    public static int CatchCoinS = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomS();
        CoinGet = SimpleCollectibleScript.getAquireCoinS();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinS Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverS = true;
            else
                CatchCoinS = 19;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverS = false;
            CatchCoinS = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinS()
    {
        return CatchCoinS;
    }

    public static bool getGameOverS()
    {
        return GameOverS;
    }
}
