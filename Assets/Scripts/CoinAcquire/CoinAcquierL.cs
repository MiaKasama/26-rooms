using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierL : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 12;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverL = false;

    //コイン取得情報
    public static int CatchCoinL = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomL();
        CoinGet = SimpleCollectibleScript.getAquireCoinL();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinL Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverL = true;
            else
                CatchCoinL = 12;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverL = false;
            CatchCoinL = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinL()
    {
        return CatchCoinL;
    }

    public static bool getGameOverL()
    {
        return GameOverL;
    }
}
