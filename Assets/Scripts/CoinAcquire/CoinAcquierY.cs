using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierY : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 25;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverY = false;

    //コイン取得情報
    public static int CatchCoinY = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomY();
        CoinGet = SimpleCollectibleScript.getAquireCoinY();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinY Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverY = true;
            else
                CatchCoinY = 25;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverY = false;
            CatchCoinY = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinY()
    {
        return CatchCoinY;
    }

    public static bool getGameOverY()
    {
        return GameOverY;
    }
}
