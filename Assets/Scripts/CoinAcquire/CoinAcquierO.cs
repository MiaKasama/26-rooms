using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierO : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 15;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverO = false;

    //コイン取得情報
    public static int CatchCoinO = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomO();
        CoinGet = SimpleCollectibleScript.getAquireCoinO();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinO Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverO = true;
            else
                CatchCoinO = 15;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverO = false;
            CatchCoinO = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinO()
    {
        return CatchCoinO;
    }

    public static bool getGameOverO()
    {
        return GameOverO;
    }
}
