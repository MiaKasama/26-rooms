using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierZ : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 26;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverZ = false;

    //コイン取得情報
    public static int CatchCoinZ = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomZ();
        CoinGet = SimpleCollectibleScript.getAquireCoinZ();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinZ Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverZ = true;
            else
                CatchCoinZ = 26;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverZ = false;
            CatchCoinZ = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinZ()
    {
        return CatchCoinZ;
    }

    public static bool getGameOverZ()
    {
        return GameOverZ;
    }
}
