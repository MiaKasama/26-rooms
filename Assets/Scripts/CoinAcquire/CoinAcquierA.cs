using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierA : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 1;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverA = false;

    //コイン取得情報
    public static int CatchCoinA = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomA();
        CoinGet = SimpleCollectibleScript.getAquireCoinA();
        
        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinA Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverA = true;
            else
                CatchCoinA = 1;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverA = false;
            CatchCoinA = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinA()
    {
        return CatchCoinA;
    }

    public static bool getGameOverA()
    {
        return GameOverA;
    }
}
