using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierT : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 20;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverT = false;

    //コイン取得情報
    public static int CatchCoinT = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomT();
        CoinGet = SimpleCollectibleScript.getAquireCoinT();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinT Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverT = true;
            else
                CatchCoinT = 20;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverT = false;
            CatchCoinT = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinT()
    {
        return CatchCoinT;
    }

    public static bool getGameOverT()
    {
        return GameOverT;
    }
}
