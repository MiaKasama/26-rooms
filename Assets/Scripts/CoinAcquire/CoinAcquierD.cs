using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierD : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 4;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverD = false;

    //コイン取得情報
    public static int CatchCoinD = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomD();
        CoinGet = SimpleCollectibleScript.getAquireCoinD();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinD Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverD = true;
            else
                CatchCoinD = 4;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverD = false;
            CatchCoinD = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinD()
    {
        return CatchCoinD;
    }

    public static bool getGameOverD()
    {
        return GameOverD;
    }
}
