using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAcquierV : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    bool GetZone = false;
    bool CoinGet = false;
    bool Got = false;

    //コインにはそれぞれ内部パラメータに番号がある
    int GetNumber = 22;

    //指定以外のコインを拾ったらゲームオーバー
    bool CoinOver = true;
    public static bool GameOverV = false;

    //コイン取得情報
    public static int CatchCoinV = 0;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        GetZone = NowRoom.getinRoomV();
        CoinGet = SimpleCollectibleScript.getAquireCoinV();

        if(GetZone == true && CoinGet == true && Got == false)
        {
            Debug.Log("CoinV Get!");

            Got = true;

            for(i = 0; i < IndicateArray.Length; i++)
            {
                if (GetNumber == IndicateArray[i])
                    CoinOver = false;
            }

            if(CoinOver == true)
                GameOverV = true;
            else
                CatchCoinV = 22;
        }

        if (RetryGame.getInitializeStartTime() == true)
        {
            GameOverV = false;
            CatchCoinV = 0;
        }
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();
    }

    public static int getCatchCoinV()
    {
        return CatchCoinV;
    }

    public static bool getGameOverV()
    {
        return GameOverV;
    }
}
