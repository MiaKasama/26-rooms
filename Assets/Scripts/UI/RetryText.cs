using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RetryText : MonoBehaviour
{
    //待つかどうか
    bool Waiting = true;

    //テキストの点滅表示をするか
    bool RetryTextBool = false;

    //表示するテキストの内容
    private TextMeshProUGUI RetryFlashText;

    //点滅スパン
    float span = 1.0f;

    //経過時間
    float CurrentTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        RetryFlashText = GetComponent<TextMeshProUGUI>();
        RetryFlashText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        //ゲームオーバーまたはクリアで表示
        //表示はゲームオーバーは2秒後、クリアは5秒後
        if (GameOverInformation.getGameOverSet() == true && Waiting == true)
        {
            //コルーチンの実行
            StartCoroutine(OverText_frames());

            Waiting = false;
        }

        if (NowRoom.getClearCheck() == true && Waiting == true)
        {
            //コルーチンの実行
            StartCoroutine(ClearText_frames());

            Waiting = false;
        }

        if(RetryTextBool == true)
        {
            CurrentTime += Time.deltaTime;

            if (CurrentTime > span)
                RetryFlashText.text = "";

            if (CurrentTime > 2 * span)
            {
                RetryFlashText.text = "Enterキーを押すとリトライします。";
                CurrentTime = 0.0f;
            }
        }
    }

    private IEnumerator OverText_frames()
    {
        yield return new WaitForSeconds(1.0f);

        RetryTextBool = true;
    }

    private IEnumerator ClearText_frames()
    {
        yield return new WaitForSeconds(4.0f);

        RetryTextBool = true;
    }
}
