using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NarrationIcon : MonoBehaviour
{
    //表示するアイコン
    private Image NarrationImage;

    //透明度を無くすかどうか
    bool TransParency = false;

    [SerializeField]
    [Tooltip("表示する際の透明度")]
    private float AlphaValue = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        NarrationImage = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        TransParency = NarrationText.getToIcon();

        if (TransParency == true)
            NarrationImage.color = new Color(1, 1, 1, 0);
        else
            NarrationImage.color = new Color(1, 1, 1, AlphaValue);
    }
}
