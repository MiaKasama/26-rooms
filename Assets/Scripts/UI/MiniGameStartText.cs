using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MiniGameStartText : MonoBehaviour
{
    //表示するテキストの内容
    private TextMeshProUGUI EnterText;

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        EnterText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if(OnTriggerMiniGame.getEnterText() == true)
            EnterText.text = " Enter: ミニゲームスタート";
        else
            EnterText.text = "";
    }
}
