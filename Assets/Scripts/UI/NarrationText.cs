using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NarrationText : MonoBehaviour
{
    //表示するテキストの内容
    private TextMeshProUGUI Narration;

    string NarrationStr;

    //最初に表示するテキスト関連
    int StartText = 0;

    //一度に何度も同じテキストを表示しないように
    bool StartTextBool = false;
    bool OverTextBool = true;
    bool CatchTextBool = true;
    bool ClearTextBool = true;

    //手に入れたコイン
    public static int CatchText;
    public static int NextCatchText;

    [SerializeField]
    [Tooltip("文字送りのスピード")]
    private float TextWaitSecond = 0.1f;

    [SerializeField]
    [Tooltip("テキストが全て表示されてから消えるまでの時間")]
    private float TextEraseSecond = 3.0f;

    [SerializeField]
    [Tooltip("一文字目が非表示になるバグの対処")]
    float DelaySecond = 0.05f;

    //効果音
    private AudioSource TextSE;

    //別スクリプトでアイコン表示させる用
    public static bool ToIcon = false;

    //for文用変数
    int i = 0;

    //初回表示用boolean
    public static bool FirstTextBoolean = true;

    // Start is called before the first frame update
    void Start()
    {
        Narration = GetComponent<TextMeshProUGUI>();
        TextSE = GetComponent<AudioSource>();

        //初回・ゲームオーバー・ゲームクリア後以外は表示しない
        if (FirstTextBoolean == true || GameOverInformation.getGameOverSet() == true || NowRoom.getClearCheck() == true)
        {
            FirstTextBoolean = false;

            CatchText = 0;
            NextCatchText = 1;

            NarrationStr = "そこの数字をヒントに正解のコインを五枚全て集めてください。";
            StartCoroutine(StartDisplaySentence()); //文字送り開始
        }
        else 
        {
            Narration.text = "";
            ToIcon = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        CatchText = GoalDoor.getCatchCoins();

        //ゲームオーバー時のテキスト
        if (GameOverInformation.getGameOverSet() == true && OverTextBool == true)
        {
            OverTextBool = false;
            StartCoroutine(OverDisplaySentence());
        }
        else
        {
            if(CatchText >= 1 && NextCatchText >= 1 && CatchText == NextCatchText && CatchTextBool == true)
            {
                CatchTextBool = false;
                NextCatchText++;
                StartCoroutine(CatchDisplaySentence());
            }

            else if (StartText == 1 && StartTextBool == true)
            {
                StartTextBool = false;
                NarrationStr = "なお、正しくないコインを入手した時点で失格となりますのでご注意を。";
                StartCoroutine(StartDisplaySentence());
            }
            else if (StartText == 2 && StartTextBool == true)
            {
                StartTextBool = false;
                NarrationStr = "それなりに頑張ってくださいね。ではっ。";
                StartCoroutine(StartDisplaySentence());
            }
        }

        //クリア時のテキスト
        if(NowRoom.getClearCheck() == true && ClearTextBool == true)
        {
            ClearTextBool = false;
            StartCoroutine(OverDisplaySentence());
        }

        /*
        if (Narration.text == "")
            ToIcon = true;
        else
            ToIcon = false;
        */
    }

    //開始時のテキスト処理
    IEnumerator StartDisplaySentence()
    {
        foreach(char x in NarrationStr.ToCharArray())
        {
            if (OverTextBool == true && CatchTextBool == true && ClearTextBool == true)
            {
                //一文字ずつ追加
                Narration.text += x;
                ToIcon = false;

                //テキスト音
                TextSE.Play();

                yield return new WaitForSeconds(TextWaitSecond);
            }
            else
                break;
        }

        for(i = 0;i < 100 * TextEraseSecond;i++)
        {
            if (OverTextBool == true && CatchTextBool == true && ClearTextBool == true)
                yield return null;
            else
                break;
        }

        //現在の文章を白紙にする
        if (OverTextBool == true && CatchTextBool == true && ClearTextBool == true)
        {
            StartText++;
            StartTextBool = true;
        }

        Narration.text = "";
        ToIcon = true;
    }

    //コインゲット時のテキスト処理
    IEnumerator CatchDisplaySentence()
    {
        yield return new WaitForSeconds(DelaySecond);

        if (CatchText == 1)
            NarrationStr = "コイン一枚目です。まさか偶然じゃありませんね？";
        else if (CatchText == 2)
            NarrationStr = "コイン二枚目です。実は他の正解が分かってないなんて言いませんよね？";
        else if (CatchText == 3)
            NarrationStr = "コイン三枚目です。半分は超えました。まぁ、さすがにそれなりにやるみたいですね。";
        else if (CatchText == 4)
            NarrationStr = "コイン四枚目です。ちなみに最後の正解はあなたが思っている数字とは関係ない物ですよ？（大嘘）";
        else if (CatchText == 5)
            NarrationStr = "最後のコインが手に入りました。ゴールへの扉が開いたみたいです。本当ですからね？疑うなら確認してみてください。";

        foreach (char x in NarrationStr.ToCharArray())
        {
            if (OverTextBool == true && CatchText != NextCatchText && ClearTextBool == true)
            {
                //一文字ずつ追加
                Narration.text += x;
                ToIcon = false;

                //テキスト音
                TextSE.Play();

                yield return new WaitForSeconds(TextWaitSecond);
            }
            else
                break;
        }

        for (i = 0; i < 100 * TextEraseSecond; i++)
        {
            if (OverTextBool == true && CatchText != NextCatchText && ClearTextBool == true)
                yield return null;
            else
                break;
        }

        //現在の文章を白紙にする
        if (OverTextBool == true && ClearTextBool == true)
            CatchTextBool = true;

        Narration.text = "";
        ToIcon = true;
    }

    //ゲームオーバー時のテキスト処理
    IEnumerator OverDisplaySentence()
    {
        yield return new WaitForSeconds(DelaySecond);

        if(NowRoom.getClearCheck() == true)
            NarrationStr = "おめでとうございます。ゲームクリアです。その部屋のコインは全てあなたの物ですよ。全部オモチャのコインですけどね。";
        else
            NarrationStr = "間違ったコインを入手したのでミッション失敗です。";

        foreach (char x in NarrationStr.ToCharArray())
        {
            //一文字ずつ追加
            Narration.text += x;
            ToIcon = false;

            //テキスト音
            TextSE.Play();

            yield return new WaitForSeconds(TextWaitSecond);
        }
    }

    public static bool getToIcon()
    {
        return ToIcon;
    }
}
