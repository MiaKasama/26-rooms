using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class KeyIndicate1 : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    //表示する数値
    TextMeshPro KI1;

    //表示する数値はどれか
    int n = 0;

    //文字の色
    Color BeforeColor = new Color(1.0f, 1.0f, 1.0f, 1.0f); //コイン獲得前
    Color AfterColor = new Color(0.1f, 0.1f, 0.1f, 1.0f); //コイン獲得後

    // Start is called before the first frame update
    void Start()
    {
        KI1 = GetComponent<TextMeshPro>();

        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //コインを獲得したら文字をグレーにする
        if (IndicateArray[n] == CoinAcquierA.getCatchCoinA())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierB.getCatchCoinB())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierC.getCatchCoinC())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierD.getCatchCoinD())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierE.getCatchCoinE())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierF.getCatchCoinF())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierG.getCatchCoinG())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierH.getCatchCoinH())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierI.getCatchCoinI())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierJ.getCatchCoinJ())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierK.getCatchCoinK())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierL.getCatchCoinL())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierM.getCatchCoinM())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierN.getCatchCoinN())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierO.getCatchCoinO())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierP.getCatchCoinP())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierQ.getCatchCoinQ())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierR.getCatchCoinR())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierS.getCatchCoinS())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierT.getCatchCoinT())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierU.getCatchCoinU())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierV.getCatchCoinV())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierW.getCatchCoinW())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierX.getCatchCoinX())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierY.getCatchCoinY())
            KI1.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierZ.getCatchCoinZ())
            KI1.color = AfterColor;
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();

        //KI1 = GetComponent<TextMeshPro>();
        KI1.SetText(IndicateArray[n].ToString());
        KI1.color = BeforeColor;
    }
}
