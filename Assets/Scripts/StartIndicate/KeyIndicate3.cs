using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class KeyIndicate3 : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    //表示する数値
    TextMeshPro KI3;

    //表示する数値はどれか
    int n = 2;

    //文字の色
    Color BeforeColor = new Color(1.0f, 1.0f, 1.0f, 1.0f); //コイン獲得前
    Color AfterColor = new Color(0.1f, 0.1f, 0.1f, 1.0f); //コイン獲得後

    // Start is called before the first frame update
    void Start()
    {
        KI3 = GetComponent<TextMeshPro>();

        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //コインを獲得したら文字をグレーにする
        if (IndicateArray[n] == CoinAcquierA.getCatchCoinA())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierB.getCatchCoinB())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierC.getCatchCoinC())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierD.getCatchCoinD())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierE.getCatchCoinE())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierF.getCatchCoinF())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierG.getCatchCoinG())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierH.getCatchCoinH())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierI.getCatchCoinI())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierJ.getCatchCoinJ())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierK.getCatchCoinK())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierL.getCatchCoinL())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierM.getCatchCoinM())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierN.getCatchCoinN())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierO.getCatchCoinO())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierP.getCatchCoinP())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierQ.getCatchCoinQ())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierR.getCatchCoinR())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierS.getCatchCoinS())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierT.getCatchCoinT())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierU.getCatchCoinU())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierV.getCatchCoinV())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierW.getCatchCoinW())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierX.getCatchCoinX())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierY.getCatchCoinY())
            KI3.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierZ.getCatchCoinZ())
            KI3.color = AfterColor;
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();

        //KI3 = GetComponent<TextMeshPro>();
        KI3.SetText(IndicateArray[n].ToString());
        KI3.color = BeforeColor;
    }
}
