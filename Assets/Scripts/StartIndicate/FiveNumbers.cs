using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiveNumbers : MonoBehaviour
{
    //アクセス可能な配列
    //扉開放に必要な番号
    public static int[] KeyNumberArray = new int[5];

    //for文用変数
    int i, j;

    //ミニゲームからメインに帰ってきた時に、鍵番号が変わらないように
    bool KeepKeyNumberArray = false;

    public static bool OverCheckKeyNumber = false;

    void Awake()
    {
        //ゲームオーバーまたはクリアの時は変える
        if (OverCheckKeyNumber == false)
            KeepKeyNumberArray = MiniToMain.getKeepKNA();
        else
            KeepKeyNumberArray = false;

        OverCheckKeyNumber = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        //値を一時的に保管する変数
        int temp;

        if (KeepKeyNumberArray == false)
        {
            i = 0;

            //1〜26でランダムに数値を得る
            while (i < KeyNumberArray.Length)
            {
                KeyNumberArray[i] = Random.Range(1, 27);

                if (i != 0)
                {
                    //数値がダブったらやり直し
                    for (j = 0; j < i; j++)
                    {
                        if (KeyNumberArray[i] == KeyNumberArray[j])
                            i--;
                    }
                }

                //Debug.Log(KeyNumberArray[i]);
                i++;
            }

            //バブルソートを行う
            for (i = 0; i < KeyNumberArray.Length; i++)
            {
                for (j = 1; j < KeyNumberArray.Length - i; j++)
                {
                    if (KeyNumberArray[j] < KeyNumberArray[j - 1])
                    {
                        temp = KeyNumberArray[j];
                        KeyNumberArray[j] = KeyNumberArray[j - 1];
                        KeyNumberArray[j - 1] = temp;
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //ゲームオーバーまたはクリアの時は変える
        if (GameOverInformation.getGameOverSet() == true || NowRoom.getClearCheck() == true)
            OverCheckKeyNumber = true;
    }

    public static int[] getKeyNumberArray()
    {
        return KeyNumberArray;
    }
}
