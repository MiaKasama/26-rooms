using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class KeyIndicate5 : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    //表示する数値
    TextMeshPro KI5;

    //表示する数値はどれか
    int n = 4;

    //文字の色
    Color BeforeColor = new Color(1.0f, 1.0f, 1.0f, 1.0f); //コイン獲得前
    Color AfterColor = new Color(0.1f, 0.1f, 0.1f, 1.0f); //コイン獲得後

    // Start is called before the first frame update
    void Start()
    {
        KI5 = GetComponent<TextMeshPro>();

        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //コインを獲得したら文字をグレーにする
        if (IndicateArray[n] == CoinAcquierA.getCatchCoinA())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierB.getCatchCoinB())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierC.getCatchCoinC())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierD.getCatchCoinD())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierE.getCatchCoinE())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierF.getCatchCoinF())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierG.getCatchCoinG())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierH.getCatchCoinH())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierI.getCatchCoinI())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierJ.getCatchCoinJ())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierK.getCatchCoinK())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierL.getCatchCoinL())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierM.getCatchCoinM())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierN.getCatchCoinN())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierO.getCatchCoinO())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierP.getCatchCoinP())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierQ.getCatchCoinQ())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierR.getCatchCoinR())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierS.getCatchCoinS())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierT.getCatchCoinT())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierU.getCatchCoinU())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierV.getCatchCoinV())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierW.getCatchCoinW())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierX.getCatchCoinX())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierY.getCatchCoinY())
            KI5.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierZ.getCatchCoinZ())
            KI5.color = AfterColor;
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();

        //KI5 = GetComponent<TextMeshPro>();
        KI5.SetText(IndicateArray[n].ToString());
        KI5.color = BeforeColor;
    }
}
