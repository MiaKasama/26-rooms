using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class KeyIndicate4 : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    //表示する数値
    TextMeshPro KI4;

    //表示する数値はどれか
    int n = 3;

    //文字の色
    Color BeforeColor = new Color(1.0f, 1.0f, 1.0f, 1.0f); //コイン獲得前
    Color AfterColor = new Color(0.1f, 0.1f, 0.1f, 1.0f); //コイン獲得後

    // Start is called before the first frame update
    void Start()
    {
        KI4 = GetComponent<TextMeshPro>();

        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //コインを獲得したら文字をグレーにする
        if (IndicateArray[n] == CoinAcquierA.getCatchCoinA())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierB.getCatchCoinB())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierC.getCatchCoinC())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierD.getCatchCoinD())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierE.getCatchCoinE())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierF.getCatchCoinF())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierG.getCatchCoinG())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierH.getCatchCoinH())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierI.getCatchCoinI())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierJ.getCatchCoinJ())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierK.getCatchCoinK())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierL.getCatchCoinL())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierM.getCatchCoinM())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierN.getCatchCoinN())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierO.getCatchCoinO())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierP.getCatchCoinP())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierQ.getCatchCoinQ())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierR.getCatchCoinR())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierS.getCatchCoinS())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierT.getCatchCoinT())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierU.getCatchCoinU())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierV.getCatchCoinV())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierW.getCatchCoinW())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierX.getCatchCoinX())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierY.getCatchCoinY())
            KI4.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierZ.getCatchCoinZ())
            KI4.color = AfterColor;
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();

        //KI4 = GetComponent<TextMeshPro>();
        KI4.SetText(IndicateArray[n].ToString());
        KI4.color = BeforeColor;
    }
}
