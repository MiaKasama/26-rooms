using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class KeyIndicate2 : MonoBehaviour
{
    int[] IndicateArray = new int[5];

    //表示する数値
    TextMeshPro KI2;

    //表示する数値はどれか
    int n = 1;

    //文字の色
    Color BeforeColor = new Color(1.0f, 1.0f, 1.0f, 1.0f); //コイン獲得前
    Color AfterColor = new Color(0.1f, 0.1f, 0.1f, 1.0f); //コイン獲得後

    // Start is called before the first frame update
    void Start()
    {
        KI2 = GetComponent<TextMeshPro>();

        //コルーチンの実行
        StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        //コインを獲得したら文字をグレーにする
        if (IndicateArray[n] == CoinAcquierA.getCatchCoinA())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierB.getCatchCoinB())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierC.getCatchCoinC())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierD.getCatchCoinD())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierE.getCatchCoinE())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierF.getCatchCoinF())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierG.getCatchCoinG())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierH.getCatchCoinH())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierI.getCatchCoinI())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierJ.getCatchCoinJ())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierK.getCatchCoinK())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierL.getCatchCoinL())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierM.getCatchCoinM())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierN.getCatchCoinN())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierO.getCatchCoinO())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierP.getCatchCoinP())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierQ.getCatchCoinQ())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierR.getCatchCoinR())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierS.getCatchCoinS())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierT.getCatchCoinT())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierU.getCatchCoinU())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierV.getCatchCoinV())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierW.getCatchCoinW())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierX.getCatchCoinX())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierY.getCatchCoinY())
            KI2.color = AfterColor;

        if (IndicateArray[n] == CoinAcquierZ.getCatchCoinZ())
            KI2.color = AfterColor;
    }

    private IEnumerator Start_frames()
    {
        yield return null;

        IndicateArray = FiveNumbers.getKeyNumberArray();

        //KI2 = GetComponent<TextMeshPro>();
        KI2.SetText(IndicateArray[n].ToString());
        KI2.color = BeforeColor;
    }
}
