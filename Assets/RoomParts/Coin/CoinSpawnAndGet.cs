using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawnAndGet : MonoBehaviour
{
    [SerializeField]
    [Tooltip("生成するGameObject")]
    private GameObject createPrefab;

    [SerializeField]
    [Tooltip("生成する場所")]
    private Transform CoinPosition;

    [SerializeField]
    [Tooltip("部屋のコードを入力してください")]
    private int RoomCode;

    //コイン出現条件が満たせばtrue
    bool CoinSpawn = false;

    //スポーンするポジション
    float x, z;

    //一度スポーンしたら、獲得かリスタートまでスポーンを続ける
    public static int[] KeepCoinArray = new int[26];

    public static bool FirstCoinArray = true;

    //手に入れたコインは出現しないように
    public static int[] AcquieredCoinArray = new int[26];

    //FirstCoinArrayをfalseにする為の処理用変数
    public static int FirstCoinCount = 0;

    public static bool RetryCoinArray = false;

    //その部屋のステージをクリアしたか
    int[] StageClearArray = new int[26];

    //for文用変数
    int i;

    // Start is called before the first frame update
    void Start()
    {
        for (i = 0; i < StageClearArray.Length; i++)
            StageClearArray[i] = 0;

        //ゲーム開始時に配列に値を入れる
        //RoomCodeのデフォルト値と異なる値を入れておく
        if (FirstCoinArray == true)
        {
            FirstCoinCount++;

            if (FirstCoinCount == KeepCoinArray.Length)
                FirstCoinArray = false;

            KeepCoinArray[RoomCode - 1] = -1;
        }

        Acquiered();

        StageClearArray[RoomCode - 1] = MiniToMain.getStageClearArray()[RoomCode - 1];

        //ミニゲームクリアで指定ルームのコインをスポーンする
        if (StageClearArray[RoomCode - 1] == RoomCode && (RoomCode == OnTriggerMiniGame.getNowRoomCode() || KeepCoinArray[RoomCode - 1] == RoomCode) && RetryCoinArray == false)
        {
            if (AcquieredCoinArray[RoomCode - 1] != RoomCode)
            {
                //Debug.Log("CoinSpawn");
                CoinSpawn = true;
                KeepCoinArray[RoomCode - 1] = RoomCode;
            }
        }

        //リトライ時にコインはスポーンしない
        if (RetryCoinArray == true)
        {
            FirstCoinCount++;

            if (FirstCoinCount == KeepCoinArray.Length)
                RetryCoinArray = false;
        }

        //仮処理として0.1秒後にCoinSpawnをtrueにする
        //StartCoroutine(Start_frames());
    }

    // Update is called once per frame
    void Update()
    {
        if (CoinSpawn == true)
        {
            // GameObjectを部屋の中心に生成
            x = CoinPosition.position.x;
            z = CoinPosition.position.z;

            Instantiate(createPrefab, new Vector3(x, 1.0f, z), Quaternion.identity);
            CoinSpawn = false;
        }

        if (GameOverInformation.getGameOverSet() == true || NowRoom.getClearCheck() == true)
        {
            AcquieredCoinArray[RoomCode - 1] = 0;
            KeepCoinArray[RoomCode - 1] = -1;

            RetryCoinArray = true;

            FirstCoinCount = 0;
        }
    }

    void Acquiered()
    {
        //一度手に入れたコインの情報を取得し、別のミニゲーム終了時に復活しないようにする
        if (CoinAcquierA.getCatchCoinA() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierB.getCatchCoinB() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierC.getCatchCoinC() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierD.getCatchCoinD() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierE.getCatchCoinE() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierF.getCatchCoinF() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierG.getCatchCoinG() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierH.getCatchCoinH() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierI.getCatchCoinI() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierJ.getCatchCoinJ() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierK.getCatchCoinK() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierL.getCatchCoinL() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierM.getCatchCoinM() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierN.getCatchCoinN() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierO.getCatchCoinO() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierP.getCatchCoinP() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierQ.getCatchCoinQ() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierR.getCatchCoinR() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierS.getCatchCoinS() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierT.getCatchCoinT() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierU.getCatchCoinU() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierV.getCatchCoinV() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierW.getCatchCoinW() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierX.getCatchCoinX() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierY.getCatchCoinY() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;

        if (CoinAcquierZ.getCatchCoinZ() == RoomCode)
            AcquieredCoinArray[RoomCode - 1] = RoomCode;
    }

    /*
    private IEnumerator Start_frames()
    {
        yield return new WaitForSeconds(0.1f);

        CoinSpawn = true;
    }
    */
}
